from xmlrpc.server import SimpleXMLRPCServer
from xmlrpc.server import SimpleXMLRPCRequestHandler

# Restrict to a particular path.
class RequestHandler(SimpleXMLRPCRequestHandler):
    rpc_paths = ('/RPC2',)

# Create server
server = SimpleXMLRPCServer(("localhost", 9000),
                            requestHandler=RequestHandler)
server.register_introspection_functions()


class TodoRobot:
    def __init__(self):
        self.todos = {}

    def add(self, title):
        self.todos[title] = 1
        return len(self.todos.keys())
    
    def list(self):
        return list(self.todos.keys())
    
server.register_instance(TodoRobot())

# Run the server's main loop
server.serve_forever()
