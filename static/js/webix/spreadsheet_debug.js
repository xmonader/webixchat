/*
@license
Webix SpreadSheet v.5.1.0
This software is covered by Webix Commercial License.
Usage without proper license is prohibited.
(c) XB Software Ltd.
*/
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/codebase/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	__webpack_require__(12);
	
	__webpack_require__(13);
	
	var _toolbar = __webpack_require__(30);
	
	var bar = _interopRequireWildcard(_toolbar);
	
	var _table = __webpack_require__(36);
	
	var tbl = _interopRequireWildcard(_table);
	
	var _hotkeys = __webpack_require__(42);
	
	var key = _interopRequireWildcard(_hotkeys);
	
	var _editor = __webpack_require__(43);
	
	var editor = _interopRequireWildcard(_editor);
	
	var _sheets = __webpack_require__(48);
	
	var sheets = _interopRequireWildcard(_sheets);
	
	var _index = __webpack_require__(50);
	
	var dialogs = _interopRequireWildcard(_index);
	
	var _context = __webpack_require__(64);
	
	var context = _interopRequireWildcard(_context);
	
	var _index2 = __webpack_require__(65);
	
	var _index3 = __webpack_require__(90);
	
	var _menu = __webpack_require__(104);
	
	var menu = _interopRequireWildcard(_menu);
	
	var _conditional = __webpack_require__(105);
	
	__webpack_require__(106);
	
	function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj["default"] = obj; return newObj; } }
	
	//modules
	//global definitions
	var component = {
		name: "spreadsheet",
		_base_index: { count: 1 },
		defaults: {
			spans: true,
			rowCount: 50,
			math: true,
			columnCount: 20,
			resizeCell: true,
			sheetTabWidth: 90,
			conditionStyle: (0, _conditional.getDefaultStyles)()
		},
		$init: function $init() {
			this.$index = this._base_index.count++;
			this.$view.className += " webix_ssheet";
	
			this.$ready.unshift(this._sub_init);
			this.$ready.push(this._set_handlers);
		},
		_sub_init: function _sub_init() {
			var obj = this.config;
			var rows = [];
	
			//toolbars
			if (!obj.readonly && obj.menu) rows.push(menu.init(this));
			if (!obj.readonly && obj.toolbar !== false) rows.push(bar.init(this));
			if (obj.subbar) rows.push(obj.subbar);
			if (obj.liveEditor) rows.push(editor.init(this));
	
			//data part
			rows.push(tbl.init(this, {
				editable: !obj.readonly,
				spans: true,
				clipboard: obj.clipboard,
				liveEditor: obj.liveEditor
			}));
	
			if (obj.bottombar) {
				if (obj.bottombar === true) rows.push(sheets.init(this));else rows.push(obj.bottombar);
			}
			if (!obj.readonly) {
				context.init(this);
				dialogs.init(this);
			}
	
			this.rows_setter(rows);
		},
		_set_handlers: function _set_handlers() {
			var _this = this;
	
			//prevent double init
			if (this._table) return;
			this._table = this.$$("cells");
	
			//init all sub modules
			(0, _index2.operations)(this);
	
			//set sizes for the table
			tbl.reset(this, this.config.columnCount, this.config.rowCount);
			key.init(this); //keyboard shortcuts
	
			this._table.attachEvent("onAfterAreaAdd", function () {
				return _this.callEvent("onAfterSelect", [_this.getSelectedId(true)]);
			});
	
			this.callEvent("onComponentInit", []);
		},
		$onLoad: function $onLoad(obj, driver) {
			//when loading data by data:, we can get $onLoad before _set_handlers call
			this._set_handlers();
	
			if (obj.excel) obj = this._parseExcel(obj, driver);else if (!obj.data && typeof obj == "string" && (driver.cell || driver.row)) obj = this._parseCsv(obj, driver);
	
			sheets.load(this, obj);
		}
	};
	
	//styling
	
	(0, _index3.api)(component);
	webix.protoUI(component, webix.AtomDataLoader, webix.IdSpace, webix.ui.layout);

/***/ }),
/* 1 */,
/* 2 */,
/* 3 */,
/* 4 */,
/* 5 */,
/* 6 */,
/* 7 */,
/* 8 */,
/* 9 */,
/* 10 */,
/* 11 */,
/* 12 */
/***/ (function(module, exports) {

	"use strict";
	
	webix.i18n.spreadsheet = {
		labels: {
			// formats
			common: "Common",
			currency: "Currency",
			number: "Number",
			percent: "Percent",
			// titles
			"undo-redo": "Undo/Redo",
			"font": "Font",
			"text": "Text",
			"cell": "Cell",
			"align": "Align",
			"format": "Number",
			"column": "Column",
			"borders": "Borders",
			"px": "px",
			"apply": "Apply",
			// popups and dialogs
			"cancel": "Cancel",
			"save": "Save",
			// multisheets
			"sheet": "Sheet",
			// conditions
			"conditional-format": "Conditional Format",
			"condition": "Condition",
			"conditional-value": "Value",
			"conditional-style": "Style",
			// ranges
			"range": "Range",
			"range-title": "Named ranges",
			"range-name": "Name",
			"range-cells": "Range",
			// images
			"image-or": "or",
			"image-title": "Add image",
			"image-upload": "Select file for upload",
			"image-url": "URL (e.g. http://*)",
			// sparklines
			"sparkline-title": "Add sparkline",
			"sparkline-type": "Type",
			"sparkline-range": "Range",
			"sparkline-color": "Color",
			"sparkline-positive": "Positive",
			"sparkline-negative": "Negative",
			// custom format
			"format-title": "Set format",
			"format-pattern": "Format pattern",
			//dropdown
			"dropdown-title": "Add dropdown",
			"dropdown-range": "Range",
			// confirm
			"ok": "OK",
			// import/export
			"import-title": "Import",
			"import-not-support": "Sorry, your browser does not support import",
			"export-title": "Export",
			"export-name": "Name of xslx file",
			"export-all-sheets": "Export all sheets",
			// add link
			"link-title": "Add Link",
			"link-name": "Text",
			"link-url": "URL",
			//images
			"image": "Image",
			"graph": "Graph",
			//conditional format labels
			"display": "Display",
			"value": "Value",
			// confirm messages
			"range-remove-confirm": "Are you sure you want to remove the range permanently?",
			"sheet-remove-confirm": "Are you sure you want to remove the sheet permanently?",
			"merge-cell-confirm": "Only the left top value will remain after merging. Continue?",
			"error-range": "The range is incorrect!",
			//print
			"print": "Print",
			"print-title": "Before you print..",
			"print-settings": "General settings",
			"print-paper": "Paper size",
			"print-layout": "Layout",
			"current-sheet": "Current sheet",
			"all-sheets": "All sheets",
			"selection": "Selected cells",
			"borderless": "Hide gridlines",
			"sheet-names": "Show sheet names",
			"skip-rows": "Skip empty rows",
			"margin": "Hide margins",
			"page-letter": "Letter",
			"page-a4": "A4 (210x297mm)",
			"page-a3": "A3 (297x420mm)",
			"page-width": "Page width",
			"page-actual": "Actual Size",
			"page-portrait": "Portrait",
			"page-landscape": "Landscape"
		},
		tooltips: {
			"color": "Font color",
			"background": "Background color",
			"font-family": "Font family",
			"font-size": "Font size",
			"text-align": "Horizontal align",
			"vertical-align": "Vertical align",
			"borders": "Borders",
			"borders-no": "Clear borders",
			"borders-left": "Left border",
			"borders-top": "Top border",
			"borders-right": "Right border",
			"borders-bottom": "Bottom border",
			"borders-all": "All borders",
			"borders-outer": "Outer borders",
			"borders-top-bottom": "Top and bottom borders",
			"borders-color": "Border color",
			"align-left": "Left align",
			"align-center": "Center align",
			"align-right": "Right align",
			"align-top": "Top align",
			"align-middle": "Middle align",
			"align-bottom": "Bottom align",
			"span": "Merge",
			"wrap": "Text wrap",
			"undo": "Undo",
			"redo": "Redo",
			"format": "Number format",
			"font-weight": "Bold",
			"font-style": "Italic",
			"text-decoration": "Underline",
			"hide-gridlines": "Hide/show gridlines",
			"hide-headers": "Hide/show headers",
			"freeze-columns": "Freeze/unfreeze columns",
			"add-range": "Set name for the selected range",
			"conditional": "Conditional formatting",
			"add-sheet": "Add Sheet",
			"lock-cell": "Lock/unlock cell",
			"clear-styles": "Clear styles",
			"add-link": "Add link",
			"row": "Rows",
			"column": "Columns",
			"sheet": "Sheet"
		},
		menus: {
			"remove-sheet": "Remove sheet",
			"rename-sheet": "Rename sheet",
			"file": "File",
			"new": "New",
			"new-sheet": "New sheet",
			"excel-import": "Import from Excel",
			"excel-export": "Export to Excel",
			"sheet": "Sheets",
			"copy-sheet": "Copy to new sheet",
			"edit": "Edit",
			"undo": "Undo",
			"redo": "Redo",
			"columns": "Columns",
			"insert-column": "Insert column",
			"delete-column": "Delete column",
			"show-column": "Show column",
			"hide-column": "Hide column",
			"rows": "Rows",
			"insert-row": "Insert row",
			"delete-row": "Delete row",
			"show-row": "Show row",
			"hide-row": "Hide row",
			"insert": "Insert",
			"conditional-format": "Conditional format",
			"clear-styles": "Clear styles",
			"add-image": "Image",
			"add-sparkline": "Graph",
			"data": "Data",
			"add-link": "Add link",
			"add-range": "Named ranges",
			"sort": "Sort",
			"sort-asc": "Sort A to Z",
			"sort-desc": "Sort Z to A",
			"create-filter": "Create filter",
			"view": "View",
			"freeze-columns": "Freeze/unfreeze columns",
			"freeze-rows": "Freeze/unfreeze rows",
			"hide-gridlines": "Hide/show gridlines",
			"hide-headers": "Hide/show headers",
			"add-dropdown": "Add dropdown",
			"custom-format": "Custom format",
			"lock-cell": "Lock/unlock cell",
			"print": "Print"
		}
	};

/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	__webpack_require__(14);
	
	__webpack_require__(15);
	
	__webpack_require__(16);
	
	__webpack_require__(17);
	
	__webpack_require__(18);
	
	__webpack_require__(19);
	
	__webpack_require__(20);
	
	__webpack_require__(21);
	
	__webpack_require__(22);
	
	__webpack_require__(24);
	
	__webpack_require__(25);
	
	__webpack_require__(26);
	
	__webpack_require__(27);
	
	__webpack_require__(28);
	
	__webpack_require__(29);

/***/ }),
/* 14 */
/***/ (function(module, exports) {

	"use strict";
	
	webix.protoUI({
		name: "ssheet-align",
		$cssName: "richselect",
		$init: function $init(config) {
			config.options = {
				view: "datasuggest",
				body: {
					view: "ssheet-icons",
					tooltip: {
						template: "#tooltip#"
					},
					xCount: 3, yCount: 1
				},
				data: config.data
			};
		}
	}, webix.ui.richselect);

/***/ }),
/* 15 */
/***/ (function(module, exports) {

	"use strict";
	
	webix.protoUI({
		name: "ssheet-borders-suggest",
		defaults: {
			width: 300
		},
		$init: function $init(config) {
			config.body = {
				margin: 6,
				cols: [{ view: "ssheet-icons", scroll: false, select: true,
					xCount: 4,
					yCount: 2,
					tooltip: {
						template: function template(obj) {
							return webix.i18n.spreadsheet.tooltips["borders-" + obj.id];
						}
					},
					on: {
						onAfterSelect: function onAfterSelect() {
							var suggest = this.getParentView().getParentView();
							suggest.updateMasterValue(true);
						}
					},
					template: function template(obj) {
						var css = "webix_ssheet_button_icon webix_ssheet_icon_borders_" + obj.value;
						return "<span class='" + css + "'></span>";
					},
					data: config.data
				}, { view: "ssheet-separator" }, {
					rows: [{ view: "ssheet-color", css: config.css, name: config.name, width: 62,
						tooltip: webix.i18n.spreadsheet.tooltips["borders-color"],
						title: "<span class='webix_icon fa-pencil'></span>",
						on: {
							onChange: function onChange() {
								var suggest = this.getParentView().getParentView().getParentView();
								suggest.updateMasterValue(false);
							}
						}
					}, {}]
				}]
			};
		},
		updateMasterValue: function updateMasterValue(hide) {
			var value = this.getValue();
			var master = webix.$$(this.config.master);
			master.setValue(value);
			if (hide) this.hide();
		},
		setValue: function setValue(value) {
			if (value[0]) this.getList().select(value[0]);
			if (value[1]) this.getColorView().setValue(value[1]);
		},
		getValue: function getValue() {
			return [this.getList().getSelectedId(), this.getColorView().getValue() || ""];
		},
		getList: function getList() {
			return this.getBody().getChildViews()[0];
		},
		getColorView: function getColorView() {
			return this.getBody().getChildViews()[2].getChildViews()[0];
		}
	}, webix.ui.suggest);
	
	webix.protoUI({
		name: "ssheet-borders",
		$cssName: "richselect",
		defaults: {
			text: "<span class='webix_ssheet_button_icon webix_ssheet_icon_borders'>"
		},
		$init: function $init(config) {
			config.options = {
				view: "ssheet-borders-suggest",
				fitMaster: false,
				data: config.data
			};
	
			this.$ready.push(webix.bind(function () {
				this.getPopup().config.master = this.config.id;
			}, this));
		},
		setValue: function setValue(value) {
			if (webix.isArray(value)) {
				if (!this.config.value || value[0] != this.config.value[0] || value[1] != this.config.value[1]) {
					this.getPopup().setValue(value);
				}
				this.config.value = value;
				if (value[0]) this.callEvent("onChange");
			}
			return value;
		},
		getValue: function getValue() {
			return this.getPopup().getValue().join(",");
		},
		getList: function getList() {
			return this.getPopup().getBody().getChildViews()[0];
		},
		getColorView: function getColorView() {
			return this.getPopup().getBody().getChildViews()[1].getChildViews()[0];
		}
	}, webix.ui.richselect);

/***/ }),
/* 16 */
/***/ (function(module, exports) {

	"use strict";
	
	// toolbar: color selector
	webix.protoUI({
		$cssName: "colorboard",
		name: "ssheet-colorboard",
		defaults: {
			css: "webix_ssheet_colorboard",
			palette: [["#000000", "#434343", "#666666", "#999999", "#b7b7b7", "#cccccc", "#d9d9d9", "#efefef", "#f3f3f3", "#ffffff"], ["#980000", "#ff0000", "#ff9900", "#ffff00", "#00ff00", "#00ffff", "#4a86e8", "#0000ff", "#9900ff", "#ff00ff"], ["#e6b8af", "#f4cccc", "#fce5cd", "#fff2cc", "#d9ead3", "#d0e0e3", "#c9daf8", "#cfe2f3", "#d9d2e9", "#ead1dc"], ["#dd7e6b", "#ea9999", "#f9cb9c", "#ffe599", "#b6d7a8", "#a2c4c9", "#a4c2f4", "#9fc5e8", "#b4a7d6", "#d5a6bd"], ["#cc4125", "#e06666", "#f6b26b", "#ffd966", "#93c47d", "#76a5af", "#6d9eeb", "#6fa8dc", "#8e7cc3", "#c27ba0"], ["#a61c00", "#cc0000", "#e69138", "#f1c232", "#6aa84f", "#45818e", "#3c78d8", "#3d85c6", "#674ea7", "#a64d79"], ["#85200c", "#990000", "#b45f06", "#bf9000", "#38761d", "#134f5c", "#1155cc", "#0b5394", "#351c75", "#741b47"], ["#5b0f00", "#660000", "#783f04", "#7f6000", "#274e13", "#0c343d", "#1c4587", "#073763", "#20124d", "#4c1130"]]
		}
	}, webix.ui.colorboard);
	
	webix.protoUI({
		$cssName: "richselect",
		name: "ssheet-color",
		defaults: {
			css: "webix_ssheet_color",
			icon: "angle-down",
			suggest: {
				height: 202,
				borderless: true,
				body: {
					view: "ssheet-colorboard",
					on: {
						onSelect: function onSelect(value) {
							this.getParentView().setMasterValue({ value: value });
						}
					}
				}
			}
		},
		$init: function $init() {
			this.$view.className += " webix_ssheet_color";
		},
		$renderInput: function $renderInput(config, divStart, id) {
			var color = this.renderColor.call(this);
			divStart = divStart.replace(/([^>]>)(.*)(<\/div)/, function (match, p1, p2, p3) {
				return p1 + config.title + color + p3;
			});
	
			return webix.ui.colorpicker.prototype.$renderInput.call(this, config, divStart, id);
		},
		$setValue: function $setValue(value) {
			var popup = webix.$$(this.config.popup.toString());
			var colorboard = popup.getBody();
			if (value) {
				colorboard.setValue(value);
				this.config.value = value;
				this.getColorNode().style.backgroundColor = value;
			}
		},
		renderColor: function renderColor() {
			return "<div class='webix_ssheet_cp_color' style='background-color:" + this.config.value + ";'> </div>";
		},
		getColorNode: function getColorNode() {
			return this.$view.firstChild.firstChild.childNodes[1];
		},
		$renderIcon: function $renderIcon() {
			return webix.ui.text.prototype.$renderIcon.apply(this, arguments);
		}
	}, webix.ui.colorpicker);

/***/ }),
/* 17 */
/***/ (function(module, exports) {

	"use strict";
	
	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };
	
	webix.protoUI({
		$cssName: "window",
		name: "ssheet-dialog",
		$init: function $init(config) {
			this.$view.className += " webix_ssheet_dialog";
			this.config.buttons = config.buttons;
		},
		getHeaderConfig: function getHeaderConfig(label) {
			return {
				paddingX: 10,
				paddingY: 5,
				height: 60,
				cols: [{ view: "label", label: label }, { view: "button", type: "htmlbutton", css: "close", label: "<span class='webix_ssheet_button_icon webix_ssheet_hide_icon'></span>", width: 40, click: function click() {
						var parentView = this.getTopParentView();
						parentView.callEvent("onHideClick", []);
					} }]
			};
		},
		getBodyConfig: function getBodyConfig(content) {
			content.borderless = true;
			var elements = this.getFormElements(content);
			var config = {
				view: "form",
				css: "webix_ssheet_form",
				paddingY: 0,
				elements: elements
			};
			return config;
		},
		getFormElements: function getFormElements(content) {
			var elements;
			if (webix.isArray(content)) elements = content;else {
				elements = [];
				elements.push(content);
			}
			if (this.config.buttons) {
				elements.push({ height: 1 });
				elements.push({
					margin: 10,
					cols: [{}, { view: "button", type: "cancel", label: webix.i18n.spreadsheet.labels.cancel, autowidth: true, click: function click() {
							var parentView = this.getTopParentView();
							parentView.callEvent("onCancelClick", []);
						} }, { view: "button", label: webix.i18n.spreadsheet.labels.save, autowidth: true, click: function click() {
							var parentView = this.getTopParentView();
							parentView.callEvent("onSaveClick", []);
						} }]
				});
			}
			elements.push({ height: 10, borderless: true });
			return elements;
		},
		body_setter: function body_setter(value) {
			if ((typeof value === "undefined" ? "undefined" : _typeof(value)) == "object") {
				value.paddingY = value.paddingY || 0;
				if (value.view == "form" && value.elements) {
					value.elements = this.getFormElements(value.elements);
					if (!value.css) value.css = "webix_ssheet_form";
				} else value = this.getBodyConfig(value);
			}
	
			return webix.ui.window.prototype.body_setter.call(this, value);
		},
		head_setter: function head_setter(value) {
			if (value) {
				value = this.getHeaderConfig(value);
			}
			return webix.ui.window.prototype.head_setter.call(this, value);
		},
		defaults: {
			padding: 0,
			move: true,
			head: true,
			buttons: true,
			//we need to retain focus in table area, so disabling the autofocusing feature
			autofocus: false,
			width: 350
		}
	}, webix.ui.window, webix.IdSpace);
	
	webix.protoUI({
		$cssName: "datatable",
		name: "ssheet-dialog-table",
		$init: function $init(config) {
			if (!config.headerRowHeight) config.headerRowHeight = 34;
			this.$view.className += " webix_ssheet_table webix_ssheet_dialog_table";
		}
	}, webix.ui.datatable);

/***/ }),
/* 18 */
/***/ (function(module, exports) {

	"use strict";
	
	webix.protoUI({
		name: "ssheet-form-popup",
		defaults: {
			padding: 0,
			borderless: true
		},
		$init: function $init() {
			this.$view.className += " webix_ssheet_form";
		}
	}, webix.ui.suggest);
	
	webix.protoUI({
		name: "ssheet-form-suggest",
		defaults: {
			padding: 0,
			borderless: true
		},
		$init: function $init() {
			this.$view.className += " webix_ssheet_suggest";
		}
	}, webix.ui.suggest);
	
	webix.protoUI({
		$cssName: "colorpicker",
		name: "ssheet-colorpicker",
		$init: function $init() {
			this.$view.className += " webix_ssheet_colorpicker";
		},
		defaults: {
			icon: "angle-down",
			suggest: {
				height: 202,
				borderless: true,
				body: {
					view: "ssheet-colorboard",
					on: {
						onSelect: function onSelect(value) {
							this.getParentView().setMasterValue({ value: value });
						}
					}
				}
			}
		}
	}, webix.ui.colorpicker);

/***/ }),
/* 19 */
/***/ (function(module, exports) {

	"use strict";
	
	webix.protoUI({
		name: "formlate",
		setValue: function setValue(a) {
			return this.setHTML(a);
		},
		getValue: function getValue() {
			return "";
		}
	}, webix.ui.template);

/***/ }),
/* 20 */
/***/ (function(module, exports) {

	"use strict";
	
	webix.protoUI({
		name: "ssheet-icons",
		$cssName: "dataview",
		$init: function $init() {
			this.$view.className += " webix_ssheet_dataview";
		},
		defaults: {
			borderless: true,
			template: "<span class='webix_ssheet_button_icon #css#' ></span>",
			type: {
				width: 36,
				height: 36
			}
		}
	}, webix.ui.dataview);

/***/ }),
/* 21 */
/***/ (function(module, exports) {

	"use strict";
	
	webix.protoUI({
		name: "ssheet-suggest",
		defaults: {
			padding: 0,
			css: "webix_ssheet_suggest"
		}
	}, webix.ui.contextmenu);

/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	__webpack_require__(23);
	
	webix.protoUI({
		name: "live-editor",
		$cssName: "text webix_ssheet_formula",
		defaults: {
			keyPressTimeout: 25
		},
		$init: function $init(config) {
			var _this = this;
	
			config.suggest = {
				view: "suggest-formula",
				data: config.suggestData
			};
			this.attachEvent("onKeyPress", function (code) {
				if (code === 13) {
					//ignore Enter key if it was pressed to select some value from suggest
					if (new Date() - (_this._last_value_set || 0) > 300) {
						_this.updateCellValue();
						//we have 'enter' hot key to activate editor
						webix.delay(function () {
							this.getTopParentView()._table.moveSelection("down");
						}, _this);
					}
				}
			});
	
			this.attachEvent("onAfterRender", webix.once(function () {
				var node = this.getInputNode();
				var ev = webix.env.isIE8 ? "propertychange" : "input";
	
				webix.event(node, ev, webix.bind(this.paintValue, this));
				webix.event(node, "keydown", webix.bind(this.endEdit, this));
			}));
		},
		endEdit: function endEdit(e) {
			var code = e.which || e.keyCode;
			var node = this.getInputNode();
			//if it is a formula - force user to finish it by click outside or 'enter'
			if (code == 9 || code > 36 && code < 41 && node.value.charAt(0) !== "=") {
				var dir = code == 40 ? "down" : code === 39 ? "right" : code == 37 ? "left" : "up";
				if (code === 9) dir = "right";
	
				this.updateCellValue();
				this.getTopParentView()._table.moveSelection(dir);
			}
		},
		paintValue: function paintValue() {
			var master = this.getTopParentView();
			var cell = this.config.activeCell;
			var node = master._table.getItemNode(cell);
	
			if (cell && node) node.innerHTML = this.getValue();
		},
		updateCellValue: function updateCellValue() {
			var newv = this.getValue();
			var master = this.getTopParentView();
			var cell = this.config.activeCell;
	
			if (!cell) this.setValue("");else {
				if (newv != master.getCellValue(cell.row, cell.column)) {
					this.config.value = newv;
					master.setCellValue(cell.row, cell.column, newv);
					master.refresh();
				}
			}
		},
		$setValueHere: function $setValueHere(value) {
			this.setValueHere(value);
		},
		setValueHere: function setValueHere(value) {
			this._last_value_set = new Date();
	
			var node = this.getInputNode();
			if (node.value && node.value.charAt(0) === "=") {
				var formula = node.value;
				var cursor = node.selectionStart;
				var str1 = formula.substring(0, cursor);
				var str2 = formula.substring(cursor);
				str1 = str1.replace(/([a-zA-Z\(]+)$/, value);
	
				node.value = str1 + "(" + str2;
				node.setSelectionRange(str1.length + 1, str1.length + 1);
			}
		},
		expectRange: function expectRange() {
			var text = this.getValue().trim();
			var last = text.substr(text.length - 1, 1);
			return last.match(/[\(\,]/);
		},
		expectOperator: function expectOperator() {
			var text = this.getValue().trim();
			var last = text.substr(text.length - 1, 1);
			return text.substr(0, 1) == "=" && last.match(/[\+\&\-\/\*\%\=\(:\,]/);
		},
		setRange: function setRange(range) {
			var node = this.getInputNode();
			var cursor = node.selectionStart;
			var formula = this.getValue();
	
			var str1 = formula.substring(0, cursor) + range;
			var str2 = formula.substring(cursor);
	
			node.value = str1 + str2;
			node.setSelectionRange(str1.length + 1, str1.length + 1);
			this.focus();
		}
	}, webix.ui.text);

/***/ }),
/* 23 */
/***/ (function(module, exports) {

	"use strict";
	
	webix.protoUI({
		name: "suggest-formula",
		defaults: {
			fitMaster: false,
			width: 200,
			filter: function filter(item, value) {
				var trg = webix.$$(this.config.master).getInputNode();
				var cursor = trg.selectionStart;
				var val = trg.value;
	
				if (val.charAt(0) !== "=") return;
	
				var str1 = val.substring(0, cursor).match(/([a-zA-Z]+)$/);
				var str2 = val.charAt(cursor).search(/[^A-Za-z0-9]/);
	
				if (str1 && (cursor === val.length || str2 === 0)) {
					value = str1[0];
				}
				return item.value.toString().toLowerCase().indexOf(value.toLowerCase()) === 0;
			}
		},
		$init: function $init() {
			var _this = this;
	
			this.attachEvent("onBeforeShow", function (node) {
				if (node.tagName) {
					//only for formulas
					if (!node.value || node.value.charAt(0) !== "=") return false;
	
					var sizes = webix.html.offset(node);
					var symbolLength = 9;
	
					var y = sizes.y + sizes.height;
					var x = sizes.x + node.selectionStart * symbolLength;
	
					webix.ui.popup.prototype.show.apply(_this, [{ x: x, y: y }]);
					return false;
				}
			});
		},
		setMasterValue: function setMasterValue(data, refresh) {
			var text = data.id ? this.getItemText(data.id) : data.text || data.value;
			var master = webix.$$(this.config.master);
			master.setValueHere(text);
	
			if (!refresh) this.hide(true);
	
			this.callEvent("onValueSuggest", [data, text]);
		},
	
		$enterKey: function $enterKey(popup) {
			if (popup.isVisible()) return webix.ui.suggest.prototype.$enterKey.apply(this, arguments);
		}
	}, webix.ui.suggest);

/***/ }),
/* 24 */
/***/ (function(module, exports) {

	"use strict";
	
	webix.protoUI({
		name: "ssheet-separator",
		defaults: {
			css: "webix_ssheet_toolbar_spacer",
			template: " ",
			width: 1,
			borderless: true
		}
	}, webix.ui.view);

/***/ }),
/* 25 */
/***/ (function(module, exports) {

	"use strict";
	
	webix.protoUI({
		name: "sheets",
		defaults: {
			layout: "x",
			borderless: true,
			css: "ssheet_list_sheets",
			scrollX: false,
			select: true,
			scroll: false
		},
		getVisibleCount: function getVisibleCount() {
			return Math.floor(this.$width / this.type.width);
		}
	}, webix.EditAbility, webix.ui.list);

/***/ }),
/* 26 */
/***/ (function(module, exports) {

	"use strict";
	
	// toolbar: toogle button
	webix.protoUI({
		$cssName: "toggle",
		name: "ssheet-toggle",
		toggle: function toggle() {
			var value = this.getValue() == this.config.onValue ? true : false;
			this.setValue(!value);
		},
		$setValue: function $setValue(value) {
			if (value == this.config.offValue) value = false;else if (value == this.config.onValue) value = true;
			webix.ui.toggle.prototype.$setValue.call(this, value);
		},
		getValue: function getValue() {
			var config = this.config;
			var value = config.value;
			return !value || value == config.offValue ? config.offValue || false : config.onValue || true;
		},
		defaults: {
			template: function template(obj, common) {
				var css = obj.value === true || obj.value == obj.onValue ? " webix_pressed" : "";
				var inp = common.$renderInput(obj, common);
				return "<div class='webix_el_box" + css + "' style='width:" + obj.awidth + "px; height:" + obj.aheight + "px'>" + inp + "</div>";
			}
		}
	}, webix.ui.toggle);

/***/ }),
/* 27 */
/***/ (function(module, exports) {

	"use strict";
	
	webix.protoUI({
		name: "ssheet-bar-title",
		defaults: {
			borderless: true
		},
		$init: function $init() {
			this.$view.className += " webix_ssheet_subbar_title";
		}
	}, webix.ui.template);

/***/ }),
/* 28 */
/***/ (function(module, exports) {

	"use strict";
	
	webix.protoUI({
		name: "ssheet-button",
		$cssName: "button",
		defaults: {
			type: "htmlbutton",
			width: 40
		},
		$init: function $init(config) {
			this.$view.className += " webix_ssheet_button";
			if (config.label || config.icon) {
				var icon = (config.icon || config.name).replace("-", "_"),
				    text = config.label || "";
				if (config.arrow) text += "<span class='webix_ssheet_button_icon webix_ssheet_icon_arrow fa fa-angle-down'></span>";
				config.label = "<span class='webix_ssheet_button_icon webix_ssheet_icon_" + icon + "'></span> " + text;
				config.tooltip = webix.i18n.spreadsheet.tooltips[config.name] || "";
			}
			if (config.options && !config.popup) config.popup = getPopupConfig(config.options);
		}
	}, webix.ui.button);
	
	webix.protoUI({
		name: "ssheet-button-icon-top",
		$cssName: "button",
		defaults: {
			type: "htmlbutton",
			width: 70,
			height: 70
		},
		$init: function $init(config) {
			this.$view.className += " ssheet_button_icon_top";
			if (config.label) {
				var icon = (config.icon || config.name || "").replace("-", "_"),
				    text = config.label;
				config.label = "<span class='webix_ssheet_button_icon webix_ssheet_icon_" + icon + "'></span><br/>";
				config.label += "<span class='ssheet_button_icon_top_text'>" + text + "</span>";
				if (config.arrow) config.label += "<br/><span class='ssheet_button_icon_top_arrow fa fa-angle-down'></span>";
				config.tooltip = webix.i18n.spreadsheet.tooltips[config.name] || "";
			}
			if (config.options && !config.popup) config.popup = getPopupConfig(config.options);
		}
	}, webix.ui.button);
	
	function getPopupConfig(options) {
		return {
			view: "ssheet-suggest",
			css: "webix_ssheet_suggest",
			autowidth: true,
			template: function template(obj) {
				var text = "";
				if (obj.icon) text += "<span class='webix_ssheet_button_icon webix_ssheet_icon_" + obj.icon + "'></span> ";
				text += obj.value || webix.i18n.spreadsheet.menus[obj.id] || obj.id;
				return text;
			},
			data: options,
			on: {
				onItemClick: function onItemClick(id) {
					var button = this.config.master;
					if (button) {
						var view = webix.$$(button).getTopParentView();
						var params = [this.getItem(id)];
						if (webix.$$(button).config.area) {
							var area = view.$$("cells").getSelectArea();
							if (area) params = params.concat([area.start, area.end]);
						}
						view.callEvent("onCommand", params);
					}
				}
			}
		};
	}

/***/ }),
/* 29 */
/***/ (function(module, exports) {

	"use strict";
	
	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };
	
	webix.protoUI({
		name: "multicheckbox",
		defaults: {
			padding: 0,
			type: "clean",
			borderless: true,
			elementsConfig: {
				labelWidth: 10
			}
		},
		$init: function $init(config) {
			config.rows = [{ height: 0.000001 }];
			this.$ready.push(function () {
				this._initOnChange();
				this.setValue(config.value);
			});
		},
		_initOnChange: function _initOnChange() {
			this.attachEvent("onChange", function (newv) {
				this.blockEvent();
				if (this.$eventSource.config.name === "$all") {
					for (var i in this.elements) {
						this.elements[i].setValue(newv);
					}this.getChildViews()[0].getBody().getChildViews()[0].setValue(1);
				} else {
					var count = this.getValue().length;
					if (count === this._count) this.elements.$all.setValue(1);
					if (count < this._count) this.elements.$all.setValue(0);
					if (count === 0) this.elements[this._findNext(this.$eventSource.config.name)].setValue(1);
				}
				this.unblockEvent();
			});
		},
	
		_findNext: function _findNext(name) {
			for (var i in this.elements) {
				if (i !== name && i !== "$all") return i;
			}
		},
		setValue: function setValue(value) {
			var keys = [];
			for (var i in value) {
				keys.push(i);
			}value = value || {};
			this.elements = {};
			this._count = keys.length;
	
			if ((typeof value === "undefined" ? "undefined" : _typeof(value)) !== "object" || !keys.length) {
				if (this.getChildViews().length > 1) webix.ui([{ height: 0.000001 }], this);
			} else {
				var elements = [],
				    base = [],
				    maxHeight = 400;
				for (var _i in value) {
					base.push({ view: "checkbox", labelRight: _i, name: _i });
				}var baseHeight = base.length * webix.skin.$active.inputHeight;
	
				elements.push({
					view: "scrollview", body: { rows: base },
					scroll: baseHeight > maxHeight,
					height: baseHeight > maxHeight ? maxHeight : baseHeight
				});
	
				elements.push({ template: "<div class='ss_sep_line'></div>", height: 10 });
				elements.push({ view: "checkbox", labelRight: webix.i18n.spreadsheet.labels["export-all-sheets"], name: "$all" });
	
				webix.ui(elements, this);
				for (var _i2 in value) {
					if (value[_i2]) this.elements[_i2].setValue(value[_i2]);
				}
			}
		},
		getValue: function getValue() {
			var value = this.getValues(),
			    output = [];
			for (var i in value) {
				if (value[i] && i !== "$all") output.push(i);
			}return output;
		}
	}, webix.ui.form);

/***/ }),
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports.init = init;
	
	var _actions = __webpack_require__(31);
	
	var _helpers = __webpack_require__(32);
	
	var _all_elements = __webpack_require__(35);
	
	var elm = _interopRequireWildcard(_all_elements);
	
	function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj["default"] = obj; return newObj; } }
	
	var buttons = {
		"undo-redo": ["undo", "redo"],
		"font": ["font-family", "font-size", "font-weight", "font-style", "text-decoration", "color", "background", "borders"],
		"align": ["text-align", "vertical-align", "wrap", "span"],
		"format": ["format"]
	};
	
	function init(view) {
		view.attachEvent("onComponentInit", function () {
			return ready(view);
		});
		var elements = [];
	
		if (view.config.toolbar) {
			var toolbarElements = view.config.toolbar;
			if (toolbarElements == "full") {
				toolbarElements = elm.getAllElements();
				if (webix.isUndefined(view.config.bottombar)) view.config.bottombar = true;
			}
			elements = _helpers.ui.toolbarElements(view, toolbarElements);
		} else elements = _helpers.ui.getButtons(view, view.config.buttons || buttons);
	
		var bar = {
			view: "toolbar",
			css: "webix_ssheet_toolbar webix_layout_toolbar",
			id: "bar",
			padding: 0,
			elements: elements,
			on: {
				onChange: function onChange() {
					var source = this.$eventSource;
					var value = source.getValue();
					var name = source.config.name;
					view.callEvent("onStyleSet", [name, value]);
				},
				onItemClick: function onItemClick(id) {
					var viewId = view.innerId(id);
					if (_actions.actions[viewId]) _actions.actions[viewId].call(this, view);else view.callEvent("onCommand", [{ id: viewId }]);
				}
			}
		};
	
		view.callEvent("onViewInit", ["toolbar", bar]);
		return bar;
	}
	
	function ready(view) {
		view.attachEvent("onAfterSelect", function (selected) {
			return setValues(view, selected);
		});
	}
	
	function setValues(view, selected) {
		var styles = {};
		var cell = selected[0];
		if (cell) {
			var obj = view.getStyle(cell.row, cell.column);
			if (obj) styles = obj.props;
		}
	
		webix.extend(styles, _helpers.defaultStyles);
		view.$$("bar").setValues(styles);
	}

/***/ }),
/* 31 */
/***/ (function(module, exports) {

	"use strict";
	
	exports.__esModule = true;
	var actions = exports.actions = {
		span: function span(view) {
			var range = view.$$("cells").getSelectArea();
			if (range) {
				if (isMerged(view, range)) {
					view.splitCell();
				} else {
					var values = [];
					for (var r = range.start.row; r <= range.end.row; r++) {
						for (var c = range.start.column; c <= range.end.column; c++) {
							var value = view.getCellValue(r, c);
							if (value) values.push(value);
							if (values.length > 1) break;
						}
						if (values.length > 1) break;
					}
					if (values.length > 1) {
						view.confirm({
							text: webix.i18n.spreadsheet.labels["merge-cell-confirm"],
							callback: function callback(res) {
								if (res) view.combineCells();
							}
						});
					} else view.combineCells();
				}
			}
		},
		undo: function undo(view) {
			view.undo();
		},
		redo: function redo(view) {
			view.redo();
		},
		"hide-gridlines": function hideGridlines(view) {
			view.hideGridlines("toggle");
		},
		"hide-headers": function hideHeaders(view) {
			view.hideHeaders("toggle");
		},
		"freeze-columns": function freezeColumns(view) {
			var table = view.$$("cells");
			var select = table.getSelectedId();
			view.freezeColumns(select && select.column != "rowId" ? select.column : 0);
		},
		"freeze-rows": function freezeRows(view) {
			var table = view.$$("cells");
			var select = table.getSelectedId();
			view.freezeRows(select ? select.row : 0);
		},
		"clear-data": function clearData(view) {
			view.clearRange();
		}
	};
	
	function isMerged(view, range) {
		var i,
		    j,
		    c0 = range.start,
		    c1 = range.end;
	
		for (i = c0.row * 1; i <= c1.row * 1; i++) {
			for (j = c0.column * 1; j <= c1.column * 1; j++) {
				if (view.$$("cells").getSpan(i, j)) return true;
			}
		}
		return false;
	}

/***/ }),
/* 32 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports.ui = exports.defaultStyles = undefined;
	
	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };
	
	var _buttons = __webpack_require__(33);
	
	var toolbarSizes = {
		height: 39,
		margin: 7,
		paddingY: 1,
		sectorPadding: 5,
		sectorMargin: 0
	};
	
	// options for 'borders' select
	var _borders = [{ id: "no", value: "no" }, { id: "left", value: "left" }, { id: "top", value: "top" }, { id: "right", value: "right" }, { id: "bottom", value: "bottom" }, { id: "all", value: "all" }, { id: "outer", value: "outer" }, { id: "top-bottom", value: "top_bottom" }];
	
	// default styles for toolbar elements
	var defaultStyles = exports.defaultStyles = {
		"color": "#666666",
		"background": "#ffffff",
		"font-family": "'PT Sans', Tahoma",
		"font-size": "15px",
		"text-align": "left",
		"vertical-align": "top",
		"white-space": "nowrap",
		"borders": ["no", "#434343"]
	};
	
	var ui = exports.ui = {
		"toolbarElements": function toolbarElements(view, structure) {
			var checkButtons = function checkButtons(cells) {
				for (var i = 0; i < cells.length; i++) {
					var name = cells[i].$button;
					if (name) {
						var base = _buttons.buttonsMap[name] ? _buttons.buttonsMap[name](view) : ui.iconButton({ name: name });
						webix.extend(cells[i], base);
					}
	
					if (cells[i].rows) checkButtons(cells[i].rows);
					if (cells[i].cols) checkButtons(cells[i].cols);
				}
			};
			checkButtons(structure);
			return structure;
		},
		"getButtons": function getButtons(view, structure) {
			var config = [];
			for (var block in structure) {
				config.push(ui.elementsBlock(view, block, structure[block]));
				config.push(ui.separator());
			}
			return config;
		},
		"elementsBlock": function elementsBlock(view, name, columns) {
			var block = {
				rows: [{
					padding: 2,
					cols: [{
						height: toolbarSizes.height,
						margin: 2,
						cols: ui.blockColumns(view, columns)
					}]
				}, ui.title({ title: name })]
			};
			return block;
		},
		"blockColumns": function blockColumns(view, buttons) {
			var cols = [];
			for (var i = 0; i < buttons.length; i++) {
				var name = buttons[i];
				if ((typeof name === "undefined" ? "undefined" : _typeof(name)) == "object") {
					cols.push(name);
				} else {
					var base = _buttons.buttonsMap[name] ? _buttons.buttonsMap[name](view) : ui.iconButton({ name: name });
					cols.push(base);
				}
			}
			return cols;
		},
		button: function button(config) {
			return {
				view: "ssheet-toggle", width: config.width || 40, id: config.name, name: config.name, label: config.label,
				css: config.css || "", onValue: config.onValue, offValue: config.offValue,
				tooltip: webix.i18n.spreadsheet.tooltips[config.name] || ""
			};
		},
		colorButton: function colorButton(config) {
			return {
				view: "ssheet-color", css: config.css, name: config.name, width: config.width || 62,
				title: "<span class='webix_ssheet_button_icon webix_ssheet_color_button_icon webix_ssheet_icon_" + config.name.replace(/-/g, "_") + "' ></span>",
				tooltip: webix.i18n.spreadsheet.tooltips[config.name] || ""
			};
		},
		iconButton: function iconButton(config) {
			var btn = webix.copy(config);
			webix.extend(btn, { view: "button", type: "htmlbutton", width: 40, id: config.name,
				label: "<span class='webix_ssheet_button_icon webix_ssheet_icon_" + config.name.replace(/-/g, "_") + "'></span>",
				css: "",
				tooltip: webix.i18n.spreadsheet.tooltips[config.name] || webix.i18n.spreadsheet.menus[config.name] || ""
			});
			if (config.onValue) {
				webix.extend(btn, { view: "ssheet-toggle", onValue: config.onValue, offValue: config.offValue }, true);
			}
	
			return btn;
		},
		segmented: function segmented(config) {
			return {
				view: "segmented", name: config.name, css: config.css || "", width: config.width || 115, options: config.options
			};
		},
		select: function select(config) {
			webix.extend(config, {
				view: "richselect",
				id: config.name,
				value: defaultStyles[config.name],
				suggest: {
					css: "webix_ssheet_suggest",
					//fitMaster: false,
					padding: 0,
					data: config.options
				}
			});
	
			config.tooltip = webix.i18n.spreadsheet.tooltips[config.name] || "";
			if (config.popupWidth) {
				config.suggest.fitMaster = false;
				config.suggest.width = config.popupWidth;
			}
			if (config.popupTemplate) config.suggest.body = {
				template: config.popupTemplate
			};
			return config;
		},
		separator: function separator() {
			return {
				view: "ssheet-separator"
			};
		},
		title: function title(config) {
			var title = config.title;
			if (title.indexOf("$") === 0) title = "";
			title = webix.i18n.spreadsheet.labels[config.title] || title;
	
			return {
				template: title, view: "ssheet-bar-title", height: 24
			};
		},
		borders: function borders(config) {
			return { view: "ssheet-borders", width: config.width || 62, data: _borders, id: config.name, name: config.name,
				tooltip: webix.i18n.spreadsheet.tooltips[config.name] };
		},
		align: function align(config) {
			return { view: "ssheet-align", value: defaultStyles[config.name], width: config.width || 62, data: config.options,
				name: config.name, tooltip: webix.i18n.spreadsheet.tooltips[config.name] };
		},
		condFormat: function condFormat(config) {
			return {
				view: "ssheet-cond-format",
				width: 40,
				id: config.name,
				name: config.name
			};
		}
	};

/***/ }),
/* 33 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports.buttonsMap = exports.fontFamily = undefined;
	
	var _helpers = __webpack_require__(32);
	
	var _formats = __webpack_require__(34);
	
	// options for 'font-family' select
	var _fontFamily = [{ id: "Arial", value: "Arial" }, { id: "'PT Sans', Tahoma", value: "PT Sans" }, { id: "Tahoma", value: "Tahoma" }, { id: "Verdana", value: "Verdana" }, { id: "Calibri, Tahoma", value: "Calibri" }];
	
	// options for 'format' select
	exports.fontFamily = _fontFamily;
	function getCellFormats() {
		var locale = webix.i18n.spreadsheet.labels;
		return [{ id: "-1", value: locale.common }, { id: "price", value: locale.currency, example: "98.20" }, { id: "int", value: locale.number, example: "2120.02" }, { id: "percent", value: locale.percent, example: "0.5" }];
	}
	
	function getColumnOperation() {
		var locale = webix.i18n.spreadsheet.menus;
		return [{ id: "add", group: "column", value: locale["insert-column"] }, { id: "del", group: "column", value: locale["delete-column"] }, { id: "show", group: "column", value: locale["show-column"] }, { id: "hide", group: "column", value: locale["hide-column"] }];
	}
	
	function getRowOperation() {
		var locale = webix.i18n.spreadsheet.menus;
		return [{ id: "add", group: "row", value: locale["insert-row"] }, { id: "del", group: "row", value: locale["delete-row"] }, { id: "show", group: "row", value: locale["show-row"] }, { id: "hide", group: "row", value: locale["hide-row"] }];
	}
	
	// options for 'font-size' select
	function getFontSize() {
		var numbers = ["8", "9", "10", "11", "12", "14", "15", "16", "18", "20", "22", "24", "28", "36"];
		var fontSize = [];
	
		for (var i = 0; i < numbers.length; i++) {
			fontSize.push({ id: numbers[i] + webix.i18n.spreadsheet.labels.px, value: numbers[i] });
		}
		return fontSize;
	}
	
	var buttonsMap = exports.buttonsMap = {
		"font-family": function fontFamily() {
			return _helpers.ui.select({ name: "font-family", tooltip: "Font family", options: _fontFamily, width: 100 });
		},
		"font-size": function fontSize() {
			return _helpers.ui.select({ name: "font-size", tooltip: "Font size", options: getFontSize(), width: 62 });
		},
		"font-weight": function fontWeight() {
			return _helpers.ui.button({ name: "font-weight", label: "B", css: "webix_ssheet_bold",
				tooltip: "Bold", onValue: "bold", offValue: "normal" });
		},
		"font-style": function fontStyle() {
			return _helpers.ui.button({ name: "font-style", label: "I", css: "webix_ssheet_italic",
				tooltip: "Italic", onValue: "italic", offValue: "normal" });
		},
		"text-decoration": function textDecoration() {
			return _helpers.ui.button({ name: "text-decoration", label: "U", css: "webix_ssheet_underline",
				tooltip: "Underline", onValue: "underline", offValue: "normal" });
		},
		"color": function color() {
			return _helpers.ui.colorButton({ name: "color", icon: "font", css: "webix_ssheet_color" });
		},
		"background": function background() {
			return _helpers.ui.colorButton({ name: "background", icon: "paint-brush", css: "webix_ssheet_background" });
		},
		"borders": function borders() {
			return _helpers.ui.borders({ name: "borders" });
		},
		"text-align": function textAlign() {
			var locale = webix.i18n.spreadsheet.tooltips;
			return _helpers.ui.align({ name: "text-align", tooltip: "Horizontal Align", css: "webix_ssheet_align", options: [{ id: "left", css: "webix_ssheet_icon_left", tooltip: locale["align-left"] }, { id: "center", css: "webix_ssheet_icon_center", tooltip: locale["align-center"] }, { id: "right", css: "webix_ssheet_icon_right", tooltip: locale["align-right"] }] });
		},
		"vertical-align": function verticalAlign() {
			var locale = webix.i18n.spreadsheet.tooltips;
			return _helpers.ui.align({ name: "vertical-align", tooltip: "Vertical Align", css: "webix_ssheet_align", options: [{ id: "top", css: "webix_ssheet_icon_top", tooltip: locale["align-top"] }, { id: "middle", css: "webix_ssheet_icon_middle", tooltip: locale["align-middle"] }, { id: "bottom", css: "webix_ssheet_icon_bottom", tooltip: locale["align-bottom"] }] });
		},
		"wrap": function wrap() {
			return _helpers.ui.iconButton({ name: "wrap", onValue: "wrap", offValue: "nowrap" });
		},
		"format": function format() {
			return _helpers.ui.select({
				name: "format", tooltip: "Cell data format", options: getCellFormats(), width: 100,
				popupWidth: 180,
				popupTemplate: function popupTemplate(obj) {
					//make it look like an actual cell
					var format = _formats.formatHelpers[obj.id];
					var cell = { css: "" };
					var example = format ? format(obj.example, cell) : "";
					return obj.value + (format ? "<span class='webix_ssheet_right" + (cell.css ? " " + cell.css : "") + "'>" + example + "</span>" : "");
				}
			});
		},
		"column": function column() {
			return {
				name: "column",
				view: "ssheet-button", icon: "column",
				arrow: true, area: true, width: 55,
				options: getColumnOperation()
			};
		},
		"row": function row() {
			return {
				name: "row", view: "ssheet-button", icon: "row",
				arrow: true, area: true, width: 55,
				options: getRowOperation()
	
			};
		}
	};

/***/ }),
/* 34 */
/***/ (function(module, exports) {

	"use strict";
	
	exports.__esModule = true;
	exports.serialize = serialize;
	exports.load = load;
	exports.getFormat = getFormat;
	exports.getFormatSource = getFormatSource;
	exports.addFormat = addFormat;
	var formats = {};
	var formatsCount = 0;
	
	var formatHelpers = exports.formatHelpers = {
		price: function price(value, extra) {
			extra.css = "webix_ssheet_format_price";
			return webix.i18n.priceFormat(value);
		},
		"int": function int(value, extra) {
			extra.css = "webix_ssheet_format_int";
			return webix.i18n.numberFormat(value);
		},
		percent: function percent(value, extra) {
			extra.css = "webix_ssheet_format_percent";
			return Math.round(value * 100) + "%";
		}
	};
	
	var formatSources = {};
	
	var formatCache = {};
	
	function serialize(view, data) {
		var name,
		    formats = [];
	
		for (name in formatSources) {
			formats.push([name, formatSources[name]]);
		}
		data.formats = formats;
	}
	
	function load(view, data) {
		var i,
		    formats = data.formats;
	
		if (formats) for (i = 0; i < formats.length; i++) {
			addFormat(formats[i][1], formats[i][0]);
		}
	}
	
	function getFormat(name) {
		return formatHelpers[name];
	}
	
	function getFormatSource(name) {
		return formatSources[name] || "";
	}
	
	function addFormat(str, name) {
		if (formatCache[str]) return formatCache[str];
	
		name = name || "fmt" + formatsCount++;
		formats[name] = str;
	
		formatHelpers[name] = format2code(str);
		formatSources[name] = str;
		formatCache[str] = name;
	
		return name;
	}
	
	function splitFormat(str) {
		var conditional = str.match(/.*\[[><=].*/g);
		var parts = str.split(";");
		if (!conditional) {
			if (parts.length > 1) {
				parts[0] = (parts.length > 2 ? "[>=0]" : "[>0]") + parts[0];
				parts[1] = "[<0]" + parts[0];
				if (parts[2]) parts[2] = "[=0]" + parts[0];
			}
		}
		return parts;
	}
	
	function format2code(str) {
		var parts = splitFormat(str);
		var code = [];
		for (var i = 0; i < parts.length; i++) {
			var check = "";
			var color = "";
			var line = parts[i];
			var start = line.indexOf("[");
			if (start !== -1) {
				if (line[1].match(/[><=]/)) {
					var end = line.indexOf("]");
					check = line.substr(start + 1, end - start - 1);
					line = line.substr(end + 1);
				}
			}
	
			start = line.indexOf("[");
			if (start !== -1) {
				var _end = line.indexOf("]");
				color = line.substr(start + 1, _end - start - 1);
				line = line.substr(_end + 1);
			}
	
			code.push(genCode(check, color, line, i));
		}
	
		return new Function("val", "extra", code.join("\n") + " return val;");
	}
	
	function genCode(check, color, line, ind) {
		var str = "";
		if (check) {
			if (!ind) str += "if";else str += "else if";
			if (check[0] === "=") check = "=" + check;
			str += "(val" + check + "){ ";
		}
		if (color) str += "extra.css = \"webix_ssheet_format_" + color + "\";";
		str += genFormatCode(line);
		return str + (check ? "}" : "");
	}
	
	function genFormatCode(line) {
		if (!line) return "return val;";
		var isQuote = 0;
		var str = "return \"\"";
		var fmt = "";
		var comma = false;
		var inserted = false;
	
		for (var i = 0; i < line.length; i++) {
			if (line[i] == "\"") {
				if (isQuote) {
					str += "+\"" + line.substr(isQuote, i - isQuote) + "\"";
					isQuote = false;
					continue;
				} else isQuote = i;
			}
			if (isQuote) continue;else if (line[i] === ".") {
				fmt += ".";
			} else if (line[i] === "0" || line[i] === "#" || line[i] === "?") {
				if (!inserted) {
					str += "+fmt";inserted = true;
				}
				fmt += line[i];
			} else if (line[i] === ",") comma = true;else str += "+\"" + line[i] + "\"";
		}
	
		if (fmt) return numberFormat(fmt, comma) + ";" + str + ";";else return str + ";";
	}
	
	function numberFormat(fmt, comma) {
		var str = "if (isNaN(val)) var fmt = val; else {";
	
		var _fmt$split = fmt.split("."),
		    left = _fmt$split[0],
		    right = _fmt$split[1];
	
		right = (right || "").split("").reverse().join("");
	
		var lzeros = left.indexOf("0");if (lzeros >= 0) lzeros = left.length - lzeros;
		var lfills = left.indexOf("?");if (lfills >= 0) lfills = left.length - lfills;
		var rzeros = right.indexOf("0");if (rzeros >= 0) rzeros = right.length - rzeros;
		var rmax = Math.max(rzeros, right.length);
		var rfills = right.indexOf("?");if (rfills >= 0) rfills = right.length - rfills;
	
		str += "\n\tvar decimal = '.';\n\tvar parts = val.toFixed(" + rmax + ").split(decimal);\n\tvar left = parts[0];\n\tvar lsize = left.length; \n\tvar right = parts[1] || \"\";\n\tif (left.length < " + lzeros + ") left = \"0000000000\".substr(0, " + lzeros + " - left.length)+left;\n\tif (left.length < " + lfills + ") left = \"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\".substr(0, 6*(" + lfills + " - left.length))+left;\n\tif (" + comma + ") { \n\t\tvar buf = [];\n\t\tvar start = 3;\n\t\twhile (lsize > start) { buf.push(left.substr(left.length-start,3)); start+=3; }\n\t\tbuf.push(left.substr(0,left.length-start+3)); \n\t\tleft = buf.reverse().join(\",\");\n\t}\n\tif (right){\n\t\tvar zpoint = right.length; for (var i=right.length-1; i>=0; i--) if (right[i]!=\"0\"){ zpoint = right.length-i; break; }\n\t\n\t\tif (zpoint >  " + rzeros + ")\n\t\t\tright = right.substr(0," + rzeros + ");\n\t\tif (right.length < " + rfills + ") left += \"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\".substr(0, 6*(" + rfills + " - left.length));\n\t}\n\tvar fmt = left+(right?decimal:\"\")+right;";
	
		return str + "}\n";
	}

/***/ }),
/* 35 */
/***/ (function(module, exports) {

	"use strict";
	
	exports.__esModule = true;
	exports.getAllElements = getAllElements;
	function getText(text) {
		var locale = webix.i18n.spreadsheet;
		return locale.menus[text] || locale.labels[text] || locale.tooltips[text] || text;
	}
	function calcButtons() {
		var i,
		    name,
		    width,
		    groups = {
			"undo": ["undo", "redo"],
			"insert": ["add-sparkline", "add-image"]
		},
		    result = {};
	
		for (name in groups) {
			result[name] = 0;
			for (i = 0; i < groups[name].length; i++) {
				width = webix.html.getTextSize(getText(groups[name][i]), "webix_ssheet_button_measure").width + 2;
				result[name] = Math.max(width, result[name]);
			}
		}
		return result;
	}
	function getAllElements() {
		var sizes = calcButtons();
	
		return [{
			padding: 3,
			margin: 0,
			rows: [{
				margin: 2,
				cols: [{
					name: "sheet", view: "ssheet-button-icon-top", label: getText("sheet"), arrow: true,
					options: [{ id: "new-sheet" }, { id: "copy-sheet" }, { id: "remove-sheet" }]
				}, {
					rows: [{ $button: "excel-import" }, { $button: "excel-export" }]
				}, {
					rows: [{ $button: "print" }]
				}]
			}, {}, { template: getText("file"), view: "ssheet-bar-title", height: 24, width: 85 }]
		}, { view: "ssheet-separator" }, {
			padding: 3,
			rows: [{ $button: "undo", view: "ssheet-button", label: getText("undo"), width: sizes.undo }, { $button: "redo", view: "ssheet-button", label: getText("redo"), width: sizes.undo }, { template: getText("undo-redo"), view: "ssheet-bar-title", height: 24 }]
		}, { view: "ssheet-separator" }, {
			padding: 3,
			rows: [{
				margin: 2,
				cols: [{
					margin: 2,
					cols: [{ $button: "font-family", width: 124 }, { $button: "font-size" }]
				}, { $button: "borders" }]
			}, {
				margin: 2,
				cols: [{
					margin: 2,
					cols: [{ $button: "font-weight" }, { $button: "font-style" }, { $button: "text-decoration" }]
				}, { $button: "background" }, { $button: "color" }]
	
			}, { template: getText("font"), view: "ssheet-bar-title", height: 24 }]
		}, { view: "ssheet-separator" }, {
			padding: 3,
			rows: [{
				margin: 2,
				cols: [{ $button: "text-align" }, { $button: "span" }]
			}, {
				margin: 2,
				cols: [{ $button: "vertical-align" }, { $button: "wrap" }]
			}, { template: getText("align"), view: "ssheet-bar-title", height: 24 }]
		}, { view: "ssheet-separator" }, {
			padding: 3,
			rows: [{ $button: "format" }, { $button: "custom-format" }, { template: getText("number"), view: "ssheet-bar-title", height: 24 }]
		}, { view: "ssheet-separator" }, {
			padding: 3,
			rows: [{ cols: [{ $button: "sort-asc" }, { $button: "create-filter" }, { $button: "conditional-format" }, { $button: "add-link" }, { $button: "clear-styles" }] }, { cols: [{ $button: "sort-desc" }, { $button: "add-range" }, { $button: "lock-cell" }, { $button: "add-dropdown" }] }, { template: getText("edit"), view: "ssheet-bar-title", height: 24 }]
		}, { view: "ssheet-separator" }, {
			padding: 3,
			rows: [{ $button: "add-image", view: "ssheet-button", label: getText("image"), width: sizes.insert }, { $button: "add-sparkline", view: "ssheet-button", label: getText("graph"), width: sizes.insert }, { template: getText("insert"), view: "ssheet-bar-title", height: 24 }]
		}, { view: "ssheet-separator" }, {
			padding: 3,
			rows: [{
				cols: [{ rows: [{ $button: "row" }, { $button: "column" }] }, {
					rows: [{ $button: "hide-gridlines" }, { $button: "hide-headers" }]
				}, {
					rows: [{ $button: "freeze-rows" }, { $button: "freeze-columns" }]
				}]
			}, { template: getText("view"), view: "ssheet-bar-title", height: 24 }]
		}, { view: "ssheet-separator" }, {}];
	}

/***/ }),
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports.init = init;
	exports.reset = reset;
	
	var _column_operations = __webpack_require__(37);
	
	var cop = _interopRequireWildcard(_column_operations);
	
	var _column_names = __webpack_require__(41);
	
	var nms = _interopRequireWildcard(_column_names);
	
	var _formats = __webpack_require__(34);
	
	var fmt = _interopRequireWildcard(_formats);
	
	function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj["default"] = obj; return newObj; } }
	
	function init(view, extra) {
		view.attachEvent("onComponentInit", function () {
			return ready(view);
		});
	
		var datatable = {
			view: "datatable", id: "cells", css: "webix_ssheet_table wss_" + view.$index,
			headerRowHeight: 20,
			//select:"cell",
			spans: true,
			leftSplit: 1,
			areaselect: true,
			editable: true,
			editaction: extra.liveEditor ? "custom" : "dblclick",
			navigation: true
		};
	
		//enable clipboard by default
		if (typeof extra.clipboard === "undefined") extra.clipboard = true;
		if (extra) datatable = webix.extend(datatable, extra, true);
	
		return datatable;
	}
	
	function ready(view) {
		var grid = view._table;
	
		//in grid math
		if (view.config.math) {
			grid.config.editMath = true;
		}
	
		//saving value after edit
		grid.attachEvent("onBeforeEditStop", function (st, ed) {
			//ignore empty cells
			if (st.old === webix.undefined && st.value === "") return;
	
			if (st.value != st.old) {
				view.setCellValue(ed.row, ed.column, st.value);
				grid.refresh();
			}
	
			st.value = st.old;
		});
	
		//column drag event checking
		var dragStartColumn = void 0;
		grid.$view.firstChild.addEventListener("mousedown", function (e) {
			//column ids are starting from 0, thats why we adding 1
			dragStartColumn = e.target.parentNode.cellIndex + 1;
		});
		grid.$view.firstChild.addEventListener("mouseup", function (e) {
			var dragEndColumn = e.target.parentNode.cellIndex + 1;
			if (dragStartColumn && dragStartColumn !== dragEndColumn) {
				cop.selectColumn(dragStartColumn, view, dragEndColumn);
				dragStartColumn = undefined;
			}
		});
	
		grid.attachEvent("onEnter", function () {
			if (grid.getEditor()) {
				webix.delay(function () {
					grid.moveSelection("down");
				});
			}
		});
	
		//prevent editing of locked cells
		view.attachEvent("onBeforeEditStart", function (row, column) {
			return !view.isCellLocked(row, column);
		});
		grid.attachEvent("onBeforeEditStart", function (editor) {
			return view.callEvent("onBeforeEditStart", [editor.row, editor.column]);
		});
	
		//column and row selection
		grid.attachEvent("onBeforeSelect", function (id) {
			return id.column != "rowId";
		});
		grid.attachEvent("onBeforeBlockSelect", function (start, end, finalStep) {
			if (finalStep && (start.column !== "rowId" || end.column !== "rowId")) {
				if (start.column === "rowId") start.column = 1;
				if (end.column === "rowId") end.column = 1;
			}
			if (start.column !== "rowId" || end.column !== "rowId") {
				cop.highlightColRow(start, end, view);
				return true;
			}
			return false;
		});
		grid.attachEvent("onSelectChange", function () {
			var ids = grid.getSelectedId(true);
			if (ids.length) {
				var start = ids[0];
				var end = ids[ids.length - 1] ? ids[ids.length - 1] : ids[0];
				cop.highlightColRow(start, end, view);
			} else {
				var data = { row: 0, column: 0 };
				cop.highlightColRow(data, data, view);
			}
		});
	
		grid.attachEvent("onItemDblClick", function (id) {
			if (id.column === "rowId") cop.adjustRow(id.row, view);
		});
	
		var lastHeaderClick = 0;
		grid.attachEvent("onHeaderClick", function (id, e) {
			var headerClick = new Date();
			var dblClick = headerClick - lastHeaderClick <= 300;
	
			if (dblClick) {
				cop.adjustColumn(id.column, view);
			} else {
				lastHeaderClick = headerClick;
				if (e.shiftKey) {
					cop.selectColumns(id.column, view);
				} else {
					cop.selectColumn(id.column, view);
				}
			}
		});
	
		//select rows by shift click
		var lastClickedRow = null;
		grid.attachEvent("onItemClick", function (cell, e) {
			if (cell.column === "rowId") {
				if (!e.shiftKey) cop.selectRow(cell.row, view);else {
					if (!lastClickedRow) cop.selectRow(cell.row, view);else cop.selectRow(lastClickedRow, view, cell.row);
				}
				lastClickedRow = cell.row;
			}
		});
	
		//reset API
		view.attachEvent("onReset", function () {
			return reset(view);
		});
	
		grid.attachEvent("onBlur", function () {
			//after focus moved out, check and if it is somewhere
			//on the spreadsheet controls them move focus back to datatable
			webix.delay(function () {
				var target = document.activeElement;
				if (target && target.tagName == "INPUT") return;
	
				var focus = webix.UIManager.getFocus();
				var need_focus = focus && focus != grid && focus.getTopParentView && focus.getTopParentView() === view;
	
				if (need_focus) webix.UIManager.setFocus(grid);
			}, this, [], 100);
		});
	
		setDefaultCss(view);
	}
	
	function cell_template(view, obj, common, value, column) {
		var format = obj.$cellFormat ? obj.$cellFormat[column.id] : null;
	
		if (format) {
			var cell = { css: "" };
			var helper = fmt.getFormat(format);
			if (helper) {
				var parsed = parseFloat(value);
				if (!isNaN(parsed)) {
					var newvalue = helper(parsed, cell);
					if (cell.css) return "<div class='" + cell.css + "'>" + newvalue + "</div>";
					return newvalue;
				}
			}
		}
		return value;
	}
	
	function reset(view) {
		var grid = view.$$("cells");
		grid.clearAll();
	
		var columns = view.config.columnCount;
		var rows = view.config.rowCount;
	
		var cols = [{
			id: "rowId", header: "", width: 40,
			css: "sheet_column_0",
			template: function template(el) {
				return el.id;
			}
		}];
	
		for (var i = 1; i <= columns; i++) {
			cols.push({
				id: i,
				editor: "text",
				header: {
					text: nms.encode[i],
					css: view._hideColumn && view._hideColumn.indexOf(i + 1) >= 0 ? "webix_ssheet_hide_column" : ""
				},
				template: function template(obj, common, value, column) {
					return cell_template(view, obj, common, value, column);
				}
			});
			view.callEvent("onColumnInit", [cols[i]]);
		}
	
		grid.refreshColumns(cols);
	
		if (view._hideColumn && view._hideColumn.length) view._hideColumn.map(function (id) {
			return grid.hideColumn(id);
		});
	
		var data = [];
		for (var _i = 1; _i <= rows; _i++) {
			data.push({ id: _i });
		}grid.parse(data);
	
		if (view._hideRow && view._hideRow.length) {
			grid.filter(function (obj) {
				if (view._hideRow.indexOf(obj.id) === -1) {
					return true;
				} else {
					if (obj.id - 1 === 0) {
						var cell = view.$$("cells").getColumnConfig("rowId").header[0];
						cell.css = (cell.css || "") + " webix_ssheet_hide_row";
						view.$$("cells").refreshColumns();
					} else {
						view.$$("cells").addCellCss(obj.id - 1, "rowId", "webix_ssheet_hide_row");
					}
					return false;
				}
			});
		}
	}
	
	// nowrap by default
	function setDefaultCss(view) {
		webix.html.addStyle("#" + view._table.$view.id + ".webix_dtable .webix_cell { white-space:nowrap;}");
	}

/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports.selectRow = selectRow;
	exports.selectColumn = selectColumn;
	exports.isColSelected = isColSelected;
	exports.isCellSelected = isCellSelected;
	exports.isRowSelected = isRowSelected;
	exports.selectColumns = selectColumns;
	exports.adjustColumn = adjustColumn;
	exports.adjustRow = adjustRow;
	exports.highlightColRow = highlightColRow;
	
	var _styles = __webpack_require__(38);
	
	function selectRow(row, view, endRow) {
		var start = { row: row, column: 1 };
		var last = { row: endRow || row, column: view.config.columnCount };
		_selectRange(start, last, view);
	}
	
	function selectColumn(column, view, endColumn) {
		var startRow = view.$$("cells").data.order[0];
		var lastRow = view.$$("cells").data.order.length;
	
		var start = { row: startRow, column: column };
		var last = { row: lastRow, column: endColumn || column };
	
		_selectRange(start, last, view);
	}
	
	function isColSelected(column, view) {
		var selectedArea = view.$$("cells").getSelectArea();
		if (selectedArea) {
			if (column >= selectedArea.start.column && column <= selectedArea.end.column) return true;
		}
		return false;
	}
	
	function isCellSelected(row, column, view) {
		return isRowSelected(row, view) && isColSelected(column, view);
	}
	
	function isRowSelected(row, view) {
		var selectedArea = view.$$("cells").getSelectArea();
		if (selectedArea) {
			if (row >= selectedArea.start.row && row <= selectedArea.end.row) return true;
		}
		return false;
	}
	
	function selectColumns(column, view) {
		var selectedArea = view.$$("cells").getSelectArea();
	
		if (!selectedArea) selectColumn(column, view);else {
			var col1 = Math.min(selectedArea.start.column, selectedArea.end.column, column);
			var col2 = Math.max(selectedArea.start.column, selectedArea.end.column, column);
			selectColumn(col1, view, col2);
		}
	}
	
	function _selectRange(a, b, view) {
		view.$$("cells").addSelectArea(a, b);
	}
	
	function adjustColumn(column, view) {
		var width = 25;
	
		view._table.eachRow(function (row) {
			var rowobj = this.getItem(row);
			var text = this.getText(row, column);
			var style = view.getStyle(row, column);
			var css = style ? style.id : "";
	
			if (text) {
				var size = (0, _styles.getTextSize)(view, text, css, 0, rowobj.$height);
				if (size.width > width) width = size.width;
			}
		});
		view._table.setColumnWidth(column, width);
	}
	
	function adjustRow(row, view) {
		var height = 25;
	
		view._table.eachColumn(function (column) {
			var text = this.getText(row, column);
			var style = view.getStyle(row, column);
			var css = style ? style.id : "";
	
			if (text) {
				var size = (0, _styles.getTextSize)(view, text, css, this.getColumnConfig(column).width, 0);
				if (size.height > height) height = size.height;
			}
		});
		view._table.setRowHeight(row, height);
	}
	
	function highlightColRow(start, end, view) {
		if (!view._table.config.header) return;
	
		var sr = start.row,
		    er = end.row,
		    sc = start.column,
		    ec = end.column;
		if (er < sr) {
			;
			var _ref = [er, sr];
			sr = _ref[0];
			er = _ref[1];
		}if (ec < sc) {
			;
	
			var _ref2 = [ec, sc];
			sc = _ref2[0];
			ec = _ref2[1];
		}view._table.eachRow(function (row) {
			if (row >= sr && row <= er) view._table.addCellCss(row, "rowId", "webix_highlight");else view._table.removeCellCss(row, "rowId", "webix_highlight");
		});
	
		view._table.eachColumn(function (col) {
			var node = view._table.getHeaderNode(col);
			if (!node) return;
	
			if (col >= sc && col <= ec) webix.html.addCss(node, "webix_highlight");else webix.html.removeCss(node, "webix_highlight");
		});
	}

/***/ }),
/* 38 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports.style_names = undefined;
	exports.init = init;
	exports.getStyle = getStyle;
	exports.addStyle = addStyle;
	exports.setStyle = setStyle;
	exports.setRangeStyle = setRangeStyle;
	exports.getTextSize = getTextSize;
	exports.clearRangeStyle = clearRangeStyle;
	exports.compactStyles = compactStyles;
	
	var _undo = __webpack_require__(39);
	
	var _column_names = __webpack_require__(41);
	
	//do not change order, it will break server side compatibility
	//add new options to the end of this list
	var style_names = exports.style_names = ["color", "background", "text-align", "font-family", "font-size", "font-style", "text-decoration", "font-weight", "vertical-align", "wrap", "borders", "format", "border-right", "border-bottom", "border-left", "border-top"];
	
	var _style_map = {
		"vertical-align": {
			"top": function top() {
				return "display: block;";
			},
			"middle": function middle(obj) {
				var align = "flex-start";
				if (obj["text-align"] == "center") align = "center";else if (obj["text-align"] == "right") align = "flex-end";
				var result = "display: flex; align-items: center;justify-content:" + align + ";";
				return result;
			},
			"bottom": function bottom(obj) {
				var align = "flex-start";
				if (obj["text-align"] == "center") align = "center";else if (obj["text-align"] == "right") align = "flex-end";
				var result = "display: flex; align-items:flex-end;justify-content:" + align + ";";
				return result;
			}
		},
		"wrap": {
			"wrap": function wrap() {
				return "white-space: normal !important; line-height:normal !important; padding-top:7px;padding-bottom:7px;";
			},
			"nowrap": function nowrap() {
				return "";
			}
		},
		"format": "",
		"borders": "",
		"border-left": function borderLeft(obj) {
			if (obj["border-left"]) return "border-left: 1px solid " + obj["border-left"] + " !important;";
			return "";
		},
		"border-top": function borderTop(obj) {
			if (obj["border-top"]) return "border-top: 1px solid " + obj["border-top"] + " !important;";
			return "";
		},
		"border-right": function borderRight(obj) {
			if (obj["border-right"]) return "border-right: 1px solid " + obj["border-right"] + " !important;";
			return "";
		},
		"border-bottom": function borderBottom(obj) {
			if (obj["border-bottom"]) return "border-bottom: 1px solid " + obj["border-bottom"] + " !important;";
			return "";
		}
	};
	
	var border_checks = {
		"border-left": function borderLeft(cell, area, mode) {
			return cell.column == area.start.column || mode == "no";
		},
		"border-right": function borderRight(cell, area, mode) {
			return cell.column == area.end.column || mode == "all" || mode == "no";
		},
		"border-top": function borderTop(cell, area, mode) {
			return cell.row == area.start.row || mode == "no";
		},
		"border-bottom": function borderBottom(cell, area, mode) {
			return cell.row == area.end.row || mode == "all" || mode == "no";
		}
	};
	
	var _style_handlers = {
		"borders": function borders(view, style, value, cell) {
			var area = view.$$("cells").getSelectArea();
			value = value.split(",");
			var type = value[0];
			var color = value[1];
	
			var modes = ["border-left", "border-right", "border-bottom", "border-top"];
	
			if (type == "no") color = "";else if (type == "top-bottom") {
				modes = ["border-top", "border-bottom"];
			} else if (type != "all" && type != "outer") modes = ["border-" + type];
	
			for (var i = 0; i < modes.length; i++) {
				var mode = modes[i];
				var result = view.callEvent("onAction", ["check-borders", { row: cell.row, column: cell.column, area: area, type: type, mode: mode }]);
				if (result === true || border_checks[mode](cell, area, type)) style = _updateStyle(view, style, mode, color, cell);
			}
	
			return style;
		}
	};
	
	var sizeEl;
	function init(view) {
		view.attachEvent("onStyleSet", function (name, value) {
			return _applyStyles(view, name, value);
		});
		view.attachEvent("onDataParse", function (data) {
			return _parse(view, data);
		});
		view.attachEvent("onDataSerialize", function (data) {
			return _serialize(view, data);
		});
		view.attachEvent("onReset", function () {
			return reset(view);
		});
		view.attachEvent("onUndo", function (type, row, column, style) {
			if (type == "style") _undoStyle(view, row, column, style);
		});
	
		view.attachEvent("onCommand", function (command) {
			if (command.id == "clear-styles") view.clearRange(null, { styles: true });
		});
	
		reset(view);
	
		if (!sizeEl) sizeEl = webix.html.create("DIV", { style: "visibility:hidden; position:absolute; top:0px; left:0px; overflow:hidden;" }, "");
	}
	
	function reset(view) {
		view._styles = {};
		view._styles_cache = {};
		view._styles_max = 1;
	
		var prefix = ".wss_" + view.$index;
		webix.html.removeStyle(prefix);
	}
	
	function getStyle(view, cell) {
		var styles = view.getRow(cell.row).$cellCss;
		if (styles) {
			var styleid = styles[cell.column];
			if (styleid) return view._styles[styleid];
		}
		return null;
	}
	
	// undo
	function _undoStyle(view, row, column, style) {
		var cell = { row: row, column: column };
		setStyle(view, cell, style);
	}
	
	function _serialize(view, obj) {
		var styles = [];
	
		for (var key in view._styles_cache) {
			styles.push([view._styles_cache[key].id, key]);
		}obj.styles = styles;
	}
	
	function addStyle(view, props, origin) {
		var style = {
			props: _styleFromText(";;;;;;;;;;;;;;;")
		};
	
		if (origin) for (var key in origin.props) {
			style.props[key] = origin.props[key];
		}for (var _key in props) {
			style.props[_key] = props[_key];
		}style.text = _styleToText(style);
		var cache = view._styles_cache[style.text];
		if (cache) return cache;
	
		_addStyle(view, style);
	
		return style;
	}
	
	function _parse(view, obj) {
		if (obj.styles) for (var i = 0; i < obj.styles.length; i++) {
			var styleObj = obj.styles[i];
			var style = {
				id: styleObj[0],
				text: styleObj[1],
				props: _styleFromText(styleObj[1])
			};
	
			_addStyle(view, style, true);
		}
	
		for (var _i = 0; _i < obj.data.length; _i++) {
			var _obj$data$_i = obj.data[_i],
			    row = _obj$data$_i[0],
			    column = _obj$data$_i[1],
			    css = _obj$data$_i[3];
	
	
			if (css) update_style_data(view.getRow(row), column, view._styles[css]);
		}
	}
	
	function _applyStyles(view, name, value) {
		//this - spreadsheet
		_undo.group.set(function () {
			view.eachSelectedCell(function (cell) {
				_applyCellStyles(view, cell, name, value);
			});
		});
		view.refresh();
	}
	
	function _applyCellStyles(view, cell, name, value) {
		var ostyle = getStyle(view, cell);
		var nstyle = _updateStyle(view, ostyle, name, value, cell);
	
		if (nstyle && nstyle != ostyle) _setStyle(view, cell, nstyle, ostyle);
	}
	
	function _updateStyle(view, style, name, value, cell) {
	
		if (_style_handlers[name]) {
			return _style_handlers[name](view, style, value, cell);
		}
	
		if (style && style.props[name] == value) return style;
	
		var nstyle = { text: "", id: 0, props: style ? webix.copy(style.props) : {} };
		nstyle.props[name] = value;
		nstyle.text = _styleToText(nstyle);
	
		var cache = view._styles_cache[nstyle.text];
		if (cache) return cache;
	
		_addStyle(view, nstyle);
	
		return nstyle;
	}
	
	function update_style_data(item, column, style) {
		item.$cellCss = item.$cellCss || {};
		item.$cellFormat = item.$cellFormat || {};
	
		if (style) {
			item.$cellCss[column] = style.id;
			item.$cellFormat[column] = style.props.format || null;
		} else {
			delete item.$cellCss[column];
			delete item.$cellFormat[column];
		}
	}
	
	function setStyle(view, cell, style) {
		var old = getStyle(view, cell);
		return _setStyle(view, cell, style, old);
	}
	
	function setRangeStyle(view, range, style) {
		(0, _column_names.eachRange)(range, view, _setStyle, style);
	}
	
	function getTextSize(view, text, css, x, y) {
		var width = 0;
		var height = 0;
	
		sizeEl.innerHTML = text;
		sizeEl.style.width = (x || 1) + "px";
		sizeEl.style.height = (y || 1) + "px";
		sizeEl.className = "webix_table_cell webix_cell " + css;
		view._table.$view.appendChild(sizeEl);
	
		width = Math.max(width, sizeEl.scrollWidth);
		height = Math.max(height, sizeEl.scrollHeight);
	
		view._table.$view.removeChild(sizeEl);
		sizeEl.innerHTML = "";
	
		return { width: width, height: height };
	}
	
	function _setStyle(view, cell, style, old) {
		if (view.callEvent("onBeforeStyleChange", [cell.row, cell.column, style, old])) {
			update_style_data(view.getRow(cell.row), cell.column, style);
			view.saveCell(cell.row, cell.column);
			view.callEvent("onStyleChange", [cell.row, cell.column, style, old]);
		}
	}
	
	function _buildCssString(style) {
		var css = "";
	
		for (var key in style) {
			if (style[key]) {
				if (_style_map[key]) {
					if (_style_map[key][style[key]]) css += _style_map[key][style[key]](style);else if (typeof _style_map[key] == "function") css += _style_map[key](style);
				} else css += key + ":" + style[key] + ";";
			}
		}
	
		return css;
	}
	
	function _addStyle(view, style, silent) {
		view._styles_cache[style.text] = style;
	
		while (!style.id || view._styles[style.id]) {
			style.id = "wss" + view._styles_max++;
		}view._styles[style.id] = style;
	
		var css = _buildCssString(style.props);
		var prefix = ".wss_" + view.$index;
		webix.html.addStyle(prefix + " ." + style.id + "{" + css + "}", prefix);
	
		if (!silent) view._save("styles", { name: style.id, text: style.text });
	}
	
	function _styleToText(style) {
		var id = [];
		for (var i = 0; i < style_names.length; i++) {
			id.push(style.props[style_names[i]]);
		}return id.join(";");
	}
	
	function _styleFromText(text) {
		var parts = text.split(";");
		var props = {};
		for (var i = 0; i < style_names.length; i++) {
			props[style_names[i]] = parts[i];
		}return props;
	}
	
	function clearRangeStyle(view, range) {
		_undo.group.set(function () {
			(0, _column_names.eachRange)(range, view, function (view, cell) {
				var style = getStyle(view, cell);
				if (style) setStyle(view, cell, null);
				view.callEvent("onClearStyle", [cell.row, cell.column]);
			});
		});
	}
	
	function compactStyles(view) {
		var params = view.serialize();
	
		var data = params.data;
		var styles = params.styles;
	
		var used = {};
		for (var i = 0; i < data.length; i++) {
			var name = data[i][3];
			if (name) used[name] = 1;
		}
	
		var newStyles = [];
		for (var _i2 = 0; _i2 < styles.length; _i2++) {
			var _name = styles[_i2][0];
			if (used[_name]) newStyles.push(styles[_i2]);
		}
	
		params.styles = newStyles;
		reset(view);
		_parse(view, params);
	}

/***/ }),
/* 39 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports.group = undefined;
	exports.init = init;
	exports.undo = undo;
	exports.redo = redo;
	exports.reset = reset;
	exports.ignoreUndo = ignoreUndo;
	exports.ignoreReset = ignoreReset;
	
	var _lock_cell = __webpack_require__(40);
	
	var group = exports.group = {
		value: null,
		set: function set(method, master) {
			if (!this.value) {
				this.start();
				method.call(master);
				this.end();
			} else method.call(master);
		},
		start: function start() {
			this.value = webix.uid();
		},
		end: function end() {
			this.value = null;
		}
	};
	
	var ignoreUndoReset = false;
	
	function init(view) {
		reset(view);
		undoHandlers(view);
		view.attachEvent("onHardReset", function () {
			return reset(view);
		});
		view.attachEvent("onAfterSheetShow", function () {
			return reset(view);
		});
		// styles
		view.attachEvent("onBeforeStyleChange", function (row, column, nstyle, ostyle) {
			if ((0, _lock_cell.isCellLocked)(view, row, column)) return false;
			addToHistory(view, { action: "style", row: row, column: column, value: ostyle, newValue: nstyle, group: group.value });
		});
		// editing
		view.attachEvent("onBeforeValueChange", function (row, column, nvalue, ovalue) {
			if ((0, _lock_cell.isCellLocked)(view, row, column)) return false;
			addToHistory(view, { action: "value", row: row, column: column, value: ovalue, newValue: nvalue, group: group.value });
		});
		// merge cells
		view.attachEvent("onBeforeSpan", function (row, column, value) {
			if ((0, _lock_cell.isCellLocked)(view, row, column)) return false;
			addToHistory(view, { action: "span", row: row, column: column, value: value, newValue: value, group: group.value });
		});
		// split cells
		view.attachEvent("onBeforeSplit", function (row, column, value) {
			if ((0, _lock_cell.isCellLocked)(view, row, column)) return false;
			addToHistory(view, { action: "split", row: row, column: column, value: value, newValue: value, group: group.value });
		});
		// Any abstract undo actions
		view.attachEvent("onAction", function (action, obj) {
			return addToHistory(view, {
				action: action,
				row: obj.row || null,
				column: obj.column || null,
				value: obj.value || null,
				newValue: obj.newValue || null,
				group: group.value
			});
		});
	
		// column resize
		view.$$("cells").attachEvent("onColumnResize", function (column, nvalue, ovalue) {
			return addToHistory(view, { action: "c-resize", row: 0, column: column, value: ovalue, newValue: nvalue });
		});
		// column hide
		view.attachEvent("onColumnOperation", function (action, start, end) {
			if (action.id !== "add" && action.id !== "del") addToHistory(view, { action: action, row: 0, column: { start: start, end: end } });
		});
		// row hide
		view.attachEvent("onRowOperation", function (action, start, end) {
			if (action.id !== "add" && action.id !== "del") addToHistory(view, { action: action, row: { start: start, end: end }, column: 0 });
		});
		// row resize
		view.$$("cells").attachEvent("onRowResize", function (row, nvalue, ovalue) {
			return addToHistory(view, { action: "r-resize", row: row, column: 0, value: ovalue, newValue: nvalue });
		});
	
		// condition formats
		view.attachEvent("onBeforeConditionSet", function (row, column, value, nvalue) {
			if ((0, _lock_cell.isCellLocked)(view, row, column)) return false;
	
			value = value ? webix.copy(value) : null;
			addToHistory(view, { action: "condition", row: row, column: column, value: value, newValue: nvalue, group: group.value });
		});
	}
	
	function undo(view) {
		restoreHistory(view, -1);
	}
	
	function redo(view) {
		restoreHistory(view, 1);
	}
	
	function reset(view) {
		if (!ignoreUndoReset) {
			view._ssUndoHistory = [];
			view._ssUndoCursor = -1;
		}
	}
	
	function addToHistory(view, data) {
		if (!view.$skipHistory) {
			// remove futher history
			view._ssUndoHistory.splice(view._ssUndoCursor + 1);
			// add to an array and increase cursor
			view._ssUndoHistory.push(data);
			view._ssUndoCursor++;
		}
	}
	
	function ignoreUndo(func, view) {
		view.$skipHistory = true;
		func();
		view.$skipHistory = false;
	}
	
	function ignoreReset(func) {
		ignoreUndoReset = true;
		func();
		ignoreUndoReset = false;
	}
	
	function restoreHistory(view, step) {
		var data = view._ssUndoHistory[step > 0 ? view._ssUndoCursor + step : view._ssUndoCursor];
		if (data) {
			var value = step > 0 ? data.newValue : data.value;
			var direction = step > 0;
			var params = [data.action, data.row, data.column, value, direction];
	
			view._ssUndoCursor += step;
	
			// group support
			var group = data.group;
			var prevData = view._ssUndoHistory[step > 0 ? view._ssUndoCursor + step : view._ssUndoCursor];
	
			ignoreUndo(function () {
				view.callEvent("onUndo", params);
				if (!group || !(prevData && group == prevData.group)) view.refresh();
			}, view);
	
			if (prevData && group && group == prevData.group) {
				restoreHistory(view, step);
			}
		}
	}
	
	function undoHandlers(view) {
		view.attachEvent("onUndo", function (type, row, column, value) {
			switch (type) {
				case "freeze-row":
					view.freezeRows(value);
					break;
				case "freeze-column":
					view.freezeColumns(value);
					break;
			}
		});
	}

/***/ }),
/* 40 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports.lockCss = undefined;
	
	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };
	
	exports.init = init;
	exports.lockCell = lockCell;
	exports.isCellLocked = isCellLocked;
	exports.serialize = serialize;
	exports.load = load;
	
	var _undo = __webpack_require__(39);
	
	var lockCss = exports.lockCss = "webix_lock";
	
	function init(view) {
		view._locks = {};
		view.attachEvent("onReset", function () {
			return reset(view);
		});
		view.attachEvent("onUndo", function (type, row, column, mode) {
			if (type === "lock") lockCell(view, row, column, !!mode, true);
		});
	
		view.attachEvent("onCommand", function (cm) {
			if (cm.id === "lock-cell") lock(view);
		});
	
		view.attachEvent("onAction", function (action, p) {
			if (action == "before-grid-change") updatePosition(p.name, p.inc, p.data, p.start);
		});
	}
	
	function reset(view) {
		view._locks = {};
	}
	
	function lockCell(view, row, column, mode, silent) {
		var table = view.$$("cells");
		if ((typeof row === "undefined" ? "undefined" : _typeof(row)) === "object" && (typeof column === "undefined" ? "undefined" : _typeof(column)) === "object") {
			_undo.group.set(function () {
				_mapCellsCalls(row, column, function (row, column) {
					lockCell(view, row, column, mode, true);
				});
			});
		} else {
			mode = mode === false ? false : true;
			var objrow = view._locks[row] = view._locks[row] || {};
			objrow[column] = mode;
			if (mode) table.addCellCss(row, column, lockCss, true);else table.removeCellCss(row, column, lockCss, true);
	
			view.callEvent("onAction", ["lock", { row: row, column: column, value: !mode, newValue: mode }]);
		}
	
		if (!silent) table.refresh();
	}
	
	function _mapCellsCalls(start, end, handler, master) {
		for (var x = start.row; x <= end.row; x++) {
			for (var y = start.column; y <= end.column; y++) {
				handler.call(master || this, x, y);
			}
		}
	}
	
	function isCellLocked(view, row, column) {
		return view._locks[row] ? view._locks[row][column] : false;
	}
	
	function serialize(view, data) {
		var row,
		    column,
		    locked = [];
		for (row in view._locks) {
			for (column in view._locks[row]) {
				if (view._locks[row][column]) locked.push([row, column]);
			}
		}
		data.locked = locked;
	}
	
	function load(view, data) {
		var i,
		    locked = data.locked;
	
		if (locked) for (i = 0; i < locked.length; i++) {
			lockCell(view, locked[i][0], locked[i][1], true, true);
		}
	}
	
	function lock(view) {
		var block = view.$$("cells").getSelectArea();
		if (block) {
			var mode = view.isCellLocked(block.start.row, block.start.column);
			view.lockCell(block.start, block.end, !mode);
		}
	}
	
	function updatePosition(name, inc, data, start) {
		var locked = data.locked,
		    i = locked.length;
	
		if (inc) {
			while (i--) {
				var _locked$i = locked[i],
				    row = _locked$i[0],
				    column = _locked$i[1];
	
				if (row && name == "row" && row >= start.row || column && name == "column" && column >= start.column) {
					if (name == "row") {
						if (row < start.row - inc) //delete lock mark if row was deleted
							locked.splice(i, 1);else //update mark position if upper row was deleted
							locked[i][0] = row * 1 + inc;
					} else if (name == "column") {
						if (column < start.column - inc) {
							locked.splice(i, 1);
						} else locked[i][1] = column * 1 + inc;
					}
				}
			}
		}
	}

/***/ }),
/* 41 */
/***/ (function(module, exports) {

	"use strict";
	
	exports.__esModule = true;
	
	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };
	
	exports.adress = adress;
	exports.range = range;
	exports.toRange = toRange;
	exports.toSheetRange = toSheetRange;
	exports.rangeObj = rangeObj;
	exports.eachRange = eachRange;
	exports.isRange = isRange;
	exports.changeRange = changeRange;
	var decode = exports.decode = {};
	var encode = exports.encode = {};
	
	var alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	for (var i = 1; i < 1000; i++) {
		var prefixIndex = parseInt((i - 1) / alpha.length);
		var str = (prefixIndex ? alpha[prefixIndex - 1] : "") + alpha[(i - 1) % alpha.length];
	
		decode[str] = i;
		encode[i] = str;
	}
	
	encode[0] = encode[1];
	
	function adress(text) {
		var size = text.charCodeAt(1) < 59 ? 1 : 2;
		var col = text.substr(0, size);
		var row = text.substr(size);
	
		return [row * 1, decode[col]];
	}
	
	function range(text, view) {
		var nind = text.indexOf("!");
		var sheet = "";
	
		//parse sheet names
		if (nind !== -1) {
			sheet = text.substr(0, nind);
			if (sheet[0] === "'") sheet = sheet.substr(1, sheet.length - 2);
			text = text.substr(nind + 1);
		}
	
		var parts = text.split(":");
		if (parts.length != 2 && view) {
			text = view.ranges.getCode(text, sheet);
			if (!text) return null;
			parts = text.split(":");
		}
	
		var d1 = adress(parts[0]);
		var d2 = adress(parts[1]);
		return [d1[0], d1[1], d2[0], d2[1], sheet];
	}
	
	var notAlphaNum = /[^A-Za-z0-9]/;
	function escapeSheet(text) {
		if (notAlphaNum.test(text)) return "'" + text + "'";
		return text;
	}
	
	function toRange(x, y, x2, y2, sheet) {
		return (sheet ? escapeSheet(sheet) + "!" : "") + encode[y] + x + ":" + encode[y2] + x2;
	}
	function toSheetRange(x, y, sheet) {
		return (sheet ? escapeSheet(sheet) + "!" : "") + x + ":" + y;
	}
	
	function rangeObj(text, view) {
		if ((typeof text === "undefined" ? "undefined" : _typeof(text)) === "object") return text;
		var a = range(text, view);
		return { start: { row: a[0], column: a[1] }, end: { row: a[2], column: a[3] }, sheet: a[4] };
	}
	
	function eachRange(text, view, callback, data) {
		var range = rangeObj(text);
		for (var row = range.start.row; row <= range.end.row; row++) {
			for (var column = range.start.column; column <= range.end.column; column++) {
				callback(view, { row: row, column: column }, data);
			}
		}
	}
	
	var rangeTest = /(([A-Za-z0-9]+|\'[^']+\'')\!|)[A-Z]+[0-9]+\:[A-Z]+[0-9]+/;
	function isRange(text) {
		return rangeTest.test(text);
	}
	
	function changeRange(text, name, inc, start) {
		var updated = false;
	
		if (isRange(text)) {
			var _range = range(text),
			    r1 = _range[0],
			    c1 = _range[1],
			    r2 = _range[2],
			    c2 = _range[3],
			    sheet = _range[4];
	
			if (name === "row" && start.row <= r2) {
				if (start.row <= r1) r1 += inc;
				r2 += inc;
				updated = true;
			} else if (name === "column" && start.column <= c2) {
				if (start.column <= c1) c1 += inc;
				c2 += inc;
				updated = true;
			}
			if (updated) text = toRange(r1, c1, r2, c2, sheet);
		}
	
		return text;
	}

/***/ }),
/* 42 */
/***/ (function(module, exports) {

	"use strict";
	
	exports.__esModule = true;
	exports.init = init;
	function init(view) {
		var grid = view.$$("cells");
	
		webix.UIManager.addHotKey("delete", function () {
			view.eachSelectedCell(function (cell) {
				return view.setCellValue(cell.row, cell.column, "");
			});
			view.refresh();
		}, grid);
	
		webix.UIManager.addHotKey("any", function (view, ev) {
			//ignore shift key
			if ((ev.which || ev.keyCode) == 16) return;
	
			var sel = view.getSelectedId(true);
			if (sel.length && grid.config.editable) {
				grid.$anyKey = true;
				grid.edit(sel[0]);
			}
		}, grid);
	
		webix.UIManager.addHotKey("enter", function (view) {
			var sel = view.getSelectedId(true);
			if (sel.length && grid.config.editable) {
				if (view.config.liveEditor) grid.callEvent("onBeforeLiveEditor", [sel[0]]);else grid.edit(sel[0]);
			}
		}, grid);
	
		webix.UIManager.addHotKey("ctrl-z", function () {
			return view.undo();
		}, grid);
		webix.UIManager.addHotKey("ctrl-y", function () {
			return view.redo();
		}, grid);
	
		webix.UIManager.addHotKey("ctrl-b", function () {
			return styleHotKey(view, "font-weight");
		}, grid);
		webix.UIManager.addHotKey("command-b", function () {
			return styleHotKey(view, "font-weight");
		}, grid);
		webix.UIManager.addHotKey("ctrl-i", function () {
			return styleHotKey(view, "font-style");
		}, grid);
		webix.UIManager.addHotKey("command-i", function () {
			return styleHotKey(view, "font-style");
		}, grid);
		webix.UIManager.addHotKey("ctrl-u", function (v, e) {
			return ctrlU(view, e);
		}, grid);
		webix.UIManager.addHotKey("command-u", function (v, e) {
			return ctrlU(view, e);
		}, grid);
		webix.UIManager.addHotKey("ctrl-p", function (v, e) {
			return ctrlP(view, e);
		}, grid);
		webix.UIManager.addHotKey("command-p", function (v, e) {
			return ctrlP(view, e);
		}, grid);
	}
	
	function ctrlU(view, e) {
		styleHotKey(view, "text-decoration");
		return webix.html.preventEvent(e);
	}
	function ctrlP(view, e) {
		view.callEvent("onCommand", [{ id: "print" }]);
		return webix.html.preventEvent(e);
	}
	
	function styleHotKey(view, attr) {
		var cell = view.getSelectedId();
		if (cell) {
			var style = view.getStyle(cell.row, cell.column);
	
			var element = view.$$("bar").elements[attr].data;
			var elementOn = element.onValue;
			var elementOff = element.offValue;
	
			var state = style && style.props[attr] === elementOn ? elementOff : elementOn;
			view.callEvent("onStyleSet", [attr, state]);
		}
	}

/***/ }),
/* 43 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports.init = init;
	
	var _methods = __webpack_require__(44);
	
	var methods = _interopRequireWildcard(_methods);
	
	function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj["default"] = obj; return newObj; } }
	
	var methodsName = [];
	for (var i in methods.methods) {
		if (i != "__esModule") methodsName.push(i);
	}function init(view) {
		view.attachEvent("onComponentInit", function () {
			return ready(view);
		});
	
		return {
			view: "toolbar",
			css: "webix_ssheet_toolbar",
			elements: [{ view: "label", template: "Edit: ", width: 60 }, {
				view: "live-editor",
				disabled: true,
				id: "liveEditor",
				suggestData: methodsName
			}]
		};
	}
	
	function ready(view) {
		var editor = view.$$("liveEditor");
	
		//block native editor, and move focus to the custom input
		//edit by key press
		view._table.attachEvent("onBeforeEditStart", function (cell) {
			var mode = view._table.$anyKey;
			view._table.$anyKey = false;
	
			return startEdit(this, cell, mode);
		});
		//edit by enter key
		view._table.attachEvent("onBeforeLiveEditor", function (cell) {
			startEdit(this, cell, false);
		});
		//edit by dbl-click
		view._table.attachEvent("onItemDblClick", function (cell) {
			startEdit(this, cell, false);
		});
	
		view.attachEvent("onCellChange", function (r, c, v) {
			var cell = editor.config.activeCell;
			if (cell && cell.row == r && cell.column == c) editor.setValue(v);
		});
	
		view.attachEvent("onAfterSelect", function (data) {
			if (!view.$handleSelection) {
				var cell = data[0];
				fillEditor(view._table, cell);
			}
		});
	
		view.attachEvent("onReset", function () {
			return view.$handleSelection = null;
		});
	
		view.attachEvent("onAction", function (name, options) {
			if (name == "lock" || name == "filter" || name == "dropdown") disableEditor(options.row, options.column);
		});
	
		function startEdit(table, cell, clear) {
			var editor = table.getColumnConfig(cell.column).editor;
			//do not interfere with custom editors
			if (editor == "text") {
				fillEditor(table, cell, clear);
				return false;
			}
			return true;
		}
	
		function disableEditor(row, column) {
			if (view.getCellEditor(row, column) || view.isCellLocked(row, column)) {
				editor.setValue("");
				editor.disable();
				return true;
			}
			return false;
		}
	
		function fillEditor(table, cell, clear) {
			if (disableEditor(cell.row, cell.column)) return;
			editor.enable();
			editor.config.activeCell = cell;
			editor.setValue(clear ? "" : view.getCellValue(cell.row, cell.column));
			editor.focus();
	
			view.$handleSelection = function (a, b, st, en) {
				return pasteRange(table, st, en, a);
			};
		}
	
		function pasteRange(view, st, en, cell) {
			if (st == en) {
				if (!editor.expectOperator()) return endEdit(cell);
				editor.setRange(st);
				webix.delay(function () {
					return editor.focus();
				});
			} else {
				if (!editor.expectRange()) return endEdit(cell);
				editor.setRange(st + ":" + en);
				webix.delay(function () {
					return editor.focus();
				});
			}
	
			return false;
		}
	
		function endEdit(st) {
			if (editor.isEnabled()) {
				editor.updateCellValue();
				editor.setValue(st ? view.getCellValue(st.row, st.column) : "");
			}
			return true;
		}
	}

/***/ }),
/* 44 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports.methods = undefined;
	exports.addMethod = addMethod;
	
	var _numbers = __webpack_require__(45);
	
	var numbers = _interopRequireWildcard(_numbers);
	
	var _strings = __webpack_require__(46);
	
	var strings = _interopRequireWildcard(_strings);
	
	var _other = __webpack_require__(47);
	
	var other = _interopRequireWildcard(_other);
	
	function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj["default"] = obj; return newObj; } }
	
	var methods = exports.methods = {};
	
	webix.extend(methods, numbers);
	webix.extend(methods, strings);
	webix.extend(methods, other);
	
	function addMethod(name, handler) {
		methods[name] = handler;
	}

/***/ }),
/* 45 */
/***/ (function(module, exports) {

	"use strict";
	
	exports.__esModule = true;
	exports.SUM = SUM;
	exports.AVERAGE = AVERAGE;
	exports.COUNT = COUNT;
	exports.COUNTA = COUNTA;
	exports.COUNTBLANK = COUNTBLANK;
	exports.MAX = MAX;
	exports.MIN = MIN;
	exports.PRODUCT = PRODUCT;
	exports.SUMPRODUCT = SUMPRODUCT;
	exports.SUMSQ = SUMSQ;
	exports.VARP = VARP;
	exports.STDEVP = STDEVP;
	exports.POWER = POWER;
	exports.QUOTIENT = QUOTIENT;
	exports.SQRT = SQRT;
	exports.ABS = ABS;
	exports.RAND = RAND;
	exports.PI = PI;
	exports.INT = INT;
	exports.ROUND = ROUND;
	exports.ROUNDDOWN = ROUNDDOWN;
	exports.ROUNDUP = ROUNDUP;
	exports.TRUNC = TRUNC;
	exports.EVEN = EVEN;
	exports.ODD = ODD;
	function _to_number(v) {
		if (v || v === 0) {
			v = v * 1;
			if (!isNaN(v)) return v;
		}
		return false;
	}
	
	/*Empty cells, logical values like TRUE, or text are ignored.*/
	function SUM(set) {
		var sum = 0;
		for (var i = 0; i < set.length; i++) {
			var v = _to_number(set[i]);
			if (v !== false) sum += v;
		}
		return sum;
	}
	
	/*If a range or cell reference argument contains text, logical values, or empty cells, those values are ignored; 
	however, cells with the value zero are included.*/
	function AVERAGE(set) {
		var sum = 0,
		    count = 0;
		for (var i = 0; i < set.length; i++) {
			var v = _to_number(set[i]);
			if (v !== false) {
				sum += v;count++;
			}
		}
		return sum / count;
	}
	
	/*Empty cells, logical values, text, or error values in the array or reference are not counted. */
	function COUNT(set) {
		var count = 0;
		for (var i = 0; i < set.length; i++) {
			var v = _to_number(set[i]);
			if (v !== false) count++;
		}
		return count;
	}
	
	/*counts the number of cells that are not empty in a range, zero is excluded.*/
	function COUNTA(set) {
		var count = 0;
		for (var i = 0; i < set.length; i++) {
			if (set[i] && set[i] * 1 !== 0) count++;
		}return count;
	}
	
	/*Counts empty cells in a specified range of cells. Cells with zero values are not counted.*/
	function COUNTBLANK(set) {
		var count = 0;
		for (var i = 0; i < set.length; i++) {
			if (!set[i] * 1) count++;
		}return count;
	}
	
	/*Empty cells, logical values, or text in the array or reference are ignored.
	If the arguments contain no numbers, MAX returns 0 (zero).*/
	function MAX(set) {
		var max = "";
		for (var i = 0; i < set.length; i++) {
			var v = _to_number(set[i]);
			if (v !== false && (v > max || max === "")) max = v;
		}
		return max || 0;
	}
	
	/*Empty cells, logical values, or text in the array or reference are ignored. 
	If the arguments contain no numbers, MIN returns 0.*/
	function MIN(set) {
		var min = "";
		for (var i = 0; i < set.length; i++) {
			var v = _to_number(set[i]);
			if (v !== false && (v < min || min === "")) min = v;
		}
		return min || 0;
	}
	
	/* Only numbers in the array or reference are multiplied. 
	Empty cells, logical values, and text in the array or reference are ignored.*/
	function PRODUCT(set) {
		var product = "";
		for (var i = 0; i < set.length; i++) {
			var v = _to_number(set[i]);
			if (v !== false) product = product === "" ? v : product * v;
		}
		return product;
	}
	
	/* For valid products only numbers are multiplied. Empty cells, logical values, and text are ignored.
	Treats array entries that are not numeric as if they were zeros.*/
	function SUMPRODUCT(sets) {
		var length = sets[0].length;
		for (var i in sets) {
			if (sets[i].length !== length) return;
		}var sp = 0;
		for (var _i = 0; _i < sets[0].length; _i++) {
			var product = "";
			for (var s in sets) {
				var v = _to_number(sets[s][_i]);
				if (v !== false) product = product === "" ? v : product * v;else {
					product = 0;break;
				}
			}
			if (!webix.isUndefined(product)) sp += product;
		}
		return sp;
	}
	
	/*Empty cells, logical values, text, or error values in the array or reference are ignored. */
	function SUMSQ(set) {
		var sq = 0;
		for (var i = 0; i < set.length; i++) {
			var v = _to_number(set[i]);
			if (typeof v === "number") sq += Math.pow(v, 2);
		}
		return sq;
	}
	
	/*Empty cells, logical values, text, or error values in the array or reference are ignored. */
	function VARP(set) {
		var count = this.COUNT(set);
		var avg = this.AVERAGE(set);
	
		var sum = 0;
		for (var i = 0; i < set.length; i++) {
			var v = _to_number(set[i]);
			if (v !== false) sum += Math.pow(v - avg, 2);
		}
		return sum / count;
	}
	
	/* Empty cells, logical values, text, or error values in the array or reference are ignored. */
	function STDEVP(set) {
		return Math.sqrt(this.VARP(set));
	}
	
	/*real numbers*/
	function POWER(num, pow) {
		var n = _to_number(num),
		    p = _to_number(pow);
		if (typeof n == "number" && typeof p == "number") return Math.pow(n, p);
	}
	
	/*real numbers*/
	function QUOTIENT(num, div) {
		var n = _to_number(num),
		    d = _to_number(div);
		if (typeof n == "number" && typeof d == "number") return n / d;
	}
	
	/*Returns a positive square root.*/
	function SQRT(num) {
		var v = _to_number(num);
		if (v !== false && v >= 0) return Math.sqrt(v);
	}
	
	function ABS(num) {
		var v = _to_number(num);
		if (v !== false) return Math.abs(v);
	}
	
	function RAND() {
		return Math.random();
	}
	
	function PI() {
		return Math.PI;
	}
	
	/*Rounds a number down to the nearest integer*/
	function INT(num) {
		var v = _to_number(num);
		if (v !== false) return Math.round(v);
	}
	
	/*rounds a number to a specified number of digits*/
	function ROUND(num, digits) {
		var v = _to_number(num);
		var d = _to_number(digits) || 0;
		if (v !== false) return parseFloat(v.toFixed(d));
	}
	
	/*rounds a number down to a specified number of digits*/
	function ROUNDDOWN(num, digits) {
		var v = _to_number(num);
		var d = _to_number(digits) || 0;
		if (v !== false) return Math.floor(v * Math.pow(10, d)) / Math.pow(10, d);
	}
	
	/*rounds a number up to a specified number of digits*/
	function ROUNDUP(num, digits) {
		var v = _to_number(num);
		var d = _to_number(digits) || 0;
		if (v !== false) return Math.ceil(v * Math.pow(10, d)) / Math.pow(10, d);
	}
	
	/*Truncates a number to an integer by removing the fractional part of the number.*/
	function TRUNC(num) {
		var v = _to_number(num);
		if (v !== false) return parseInt(v);
	}
	
	/*Returns number rounded up to the nearest even integer*/
	function EVEN(num) {
		var v = _to_number(num);
		if (v !== false) {
			var r = Math.round(v);
			return r % 2 ? r + 1 : r;
		}
	}
	
	/*Returns number rounded up to the nearest odd integer*/
	function ODD(num) {
		var v = _to_number(num);
		if (v !== false) {
			var r = Math.round(v);
			return r % 2 ? r : r + 1;
		}
	}

/***/ }),
/* 46 */
/***/ (function(module, exports) {

	"use strict";
	
	exports.__esModule = true;
	
	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };
	
	exports.CONCATENATE = CONCATENATE;
	exports.LEFT = LEFT;
	exports.MID = MID;
	exports.RIGHT = RIGHT;
	exports.LOWER = LOWER;
	exports.UPPER = UPPER;
	exports.PROPER = PROPER;
	exports.TRIM = TRIM;
	exports.LEN = LEN;
	function CONCATENATE() {
		var data = Array.prototype.slice.call(arguments);
		data = data.map(function (obj) {
			if ((typeof obj === "undefined" ? "undefined" : _typeof(obj)) === "object") return obj.join("");
			return obj;
		});
		return data.join("");
	}
	
	function LEFT(text, count) {
		if (!text) {
			return "";
		}
		return text.substring(0, count);
	}
	
	function MID(text, start, count) {
		if (!text) {
			return "";
		}
		return text.substring(start, start + count);
	}
	
	function RIGHT(text, count) {
		if (!text) {
			return "";
		}
		return text.substring(text.length - count);
	}
	
	function LOWER(text) {
		if (!text) {
			return "";
		}
		return text.toLowerCase();
	}
	
	function UPPER(text) {
		if (!text) {
			return "";
		}
		return text.toUpperCase();
	}
	
	function PROPER(text) {
		if (!text) {
			return "";
		}
		var temp = text.toLowerCase().split(" ");
		text = "";
		for (var idx in temp) {
			text += (text ? " " : "") + temp[idx].substring(0, 1).toUpperCase() + temp[idx].substring(1);
		}
		return text;
	}
	
	function TRIM(text) {
		if (!text) {
			return "";
		}
		return text.trim();
	}
	
	function LEN(text) {
		if (!text && text !== 0) {
			return 0;
		}
		return text.toString().length;
	}

/***/ }),
/* 47 */
/***/ (function(module, exports) {

	"use strict";
	
	exports.__esModule = true;
	exports.IMAGE = IMAGE;
	exports.SPARKLINE = SPARKLINE;
	exports.LINK = LINK;
	exports.IF = IF;
	function IMAGE(url) {
		return "<img class=\"webix_ssheet_cimage\" src=\"" + url + "\">";
	}
	
	function SPARKLINE(arr, type, color1, color2) {
		var i,
		    config = { type: type, color: color1, negativeColor: color2 },
		    width = type == "pie" ? 60 : 150;
		for (i = 0; i < arr.length; i++) {
			arr[i] = arr[i] || 0;
		}return webix.Sparklines.getTemplate(config)(arr, { width: width, height: 35 });
	}
	
	function LINK(url, text) {
		text = text || url;
		return "<a target=\"blank\" href=\"" + url + "\">" + text + "</a>";
	}
	
	function IF(check, pos, neg) {
		if (check) return pos;else return neg;
	}

/***/ }),
/* 48 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports.init = init;
	exports.reset = reset;
	exports.add = add;
	exports.edit = edit;
	exports.remove = remove;
	exports.rename = rename;
	exports.serialize = serialize;
	exports.load = load;
	exports.newSheet = newSheet;
	exports.show = show;
	exports.getSheet = getSheet;
	exports.eachSheet = eachSheet;
	exports.getActive = getActive;
	
	var _array = __webpack_require__(49);
	
	var arr = _interopRequireWildcard(_array);
	
	var _array2 = __webpack_require__(49);
	
	function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj["default"] = obj; return newObj; } }
	
	var options = ["rename-sheet", "remove-sheet"];
	
	var actionMap = {
		"remove-sheet": function removeSheet(view) {
			view.confirm({
				text: webix.i18n.spreadsheet.labels["sheet-remove-confirm"],
				callback: function callback(res) {
					if (res) remove(view, view._activeSheet);
				}
			});
		},
		"rename-sheet": function renameSheet(view) {
			edit(view, view._activeSheet);
		},
		"new-sheet": function newSheet(view) {
			add(view);
		},
		"copy-sheet": function copySheet(view) {
			copy(view);
		}
	};
	
	function init(view) {
		view.attachEvent("onComponentInit", function () {
			return ready(view);
		});
	
		var readonly = view.config.readonly;
		var toolbar = {
			view: "toolbar",
			id: "bottom-toolbar",
			css: "webix_ssheet_bottom_toolbar webix_layout_toolbar",
			paddingY: 0,
			height: 34,
			elements: [{
				view: "button", type: "htmlbutton", width: 34, id: "add-sheet",
				label: "<span class='webix_ssheet_icon_add_sheet'></span>",
				tooltip: webix.i18n.spreadsheet.tooltips["add-sheet"] || "", hidden: readonly
			}, {
				id: "sheets",
				view: "sheets",
				xCount: 5,
				editable: !readonly,
				editaction: "dblclick",
				editor: "text",
				editValue: "value",
				type: {
					width: view.config.sheetTabWidth,
					height: 32,
					template: function template(obj) {
						return "<div>" + obj.value + "</div>" + (!readonly ? "<div class='webix_input_icon fa-angle-down webix_ssheet_sheet_arrow'></div>" : "");
					}
				},
				on: {
					onItemClick: function onItemClick(id) {
						if (id != view._activeSheet) view.showSheet(id);
					}
				}
			}, {
				id: "menu-pager",
				view: "pager",
				autosize: true,
				template: function template() {
					var html = "<button type='button' webix_p_id='prev' class='webix_pager_item'><span class='webix_icon fa-caret-left'></span></button>";
					html += "<button type='button' webix_p_id='next' class='webix_pager_item'><span class='webix_icon fa-caret-right'></span></button>";
					return html;
				}
			}]
		};
	
		view.callEvent("onViewInit", ["bottom-toolbar", toolbar]);
	
		return toolbar;
	}
	
	function menuInit(view) {
		var data = [];
		for (var i = 0; i < options.length; i++) {
			data.push({ id: options[i], value: webix.i18n.spreadsheet.menus[options[i]] || options[i] });
		}var menu = {
			view: "ssheet-suggest",
			data: data
		};
		view.callEvent("onViewInit", ["sheet-menu", menu]);
		return menu;
	}
	
	function reset(view, obj) {
		var tabs = view.$$("sheets");
		if (tabs) {
			tabs.clearAll();
			var set = [];
			obj.sheets.forEach(function (sheet) {
				set.push({ id: sheet.name, value: sheet.name });
			});
			tabs.parse(set);
		}
	}
	
	function ready(view) {
		if (view.$$("add-sheet")) view.$$("add-sheet").attachEvent("onItemClick", function () {
			add(view);
		});
	
		initPager(view);
	
		view.attachEvent("onAfterSheetShow", function (name) {
			return selectSheet(view, name);
		});
	
		view.attachEvent("onCommand", function (command) {
			if (actionMap[command.id]) actionMap[command.id](this);
		});
	
		if (view.$$("sheets")) {
	
			view.$$("sheets").data.attachEvent("onStoreUpdated", function () {
				return setPagerState(view);
			});
	
			view.$$("sheets").attachEvent("onAfterEditStop", function (state) {
				return rename(view, state.old, state.value);
			});
	
			view.$$("sheets").on_click.webix_ssheet_sheet_arrow = function (e, id) {
				if (!view.$sheetMenu) {
					view.$sheetMenu = webix.ui(menuInit(view));
					view.$sheetMenu.attachEvent("onItemClick", function (id) {
						return callAction(view, id);
					});
				}
	
				if (view.$sheetMenu.getItem("remove-sheet")) if (view.$$("sheets").count() == 1) view.$sheetMenu.disableItem("remove-sheet");else view.$sheetMenu.enableItem("remove-sheet");
				if (view.callEvent("onBeforeSheetMenu", [id])) {
					view.$sheetMenu.show(e);
				}
			};
		}
	}
	
	function initPager(view) {
		if (view.$$("menu-pager")) {
	
			view.$$("sheets").define("pager", view.$$("menu-pager"));
	
			view.$$("menu-pager").attachEvent("onAfterRender", function () {
	
				if (!this.data.page) webix.html.addCss(this.$view.firstChild, "ssheet_pager_inactive", true);else webix.html.removeCss(this.$view.firstChild, "ssheet_pager_inactive", true);
	
				if (this.data.page == this.config.limit - 1) webix.html.addCss(this.$view.lastChild, "ssheet_pager_inactive", true);else webix.html.removeCss(this.$view.lastChild, "ssheet_pager_inactive", true);
			});
		}
	}
	
	function add(view, content) {
		var index, name, newIndex;
	
		index = arr.findIndex(view._sheets, function (x) {
			return x.name == view._activeSheet;
		});
		newIndex = index > -1 ? index + 1 : view._sheets.length;
		name = getNewTitle(view);
		content = content || { data: [] };
		view._sheets.splice(newIndex, 0, {
			name: name,
			content: content
		});
		view.$$("sheets").add({ id: name, value: name }, newIndex);
		view.showSheet(name);
		view.callEvent("onSheetAdd", [name]);
		return name;
	}
	
	function getNewTitle(view) {
		var index = view._sheets.length + 1;
		while (getSheet(view, webix.i18n.spreadsheet.labels.sheet + index)) {
			index++;
		}var name = webix.i18n.spreadsheet.labels.sheet + index;
		return name;
	}
	
	function selectSheet(view, name) {
		view.$$("sheets").select(name);
		if (view.$$("menu-pager") && view.$$("sheets").config.xCount) {
			var index = view.$$("sheets").getIndexById(name);
			var page = Math.floor(index / view.$$("sheets").config.xCount);
			view.$$("menu-pager").select(page);
		}
	}
	
	function setPagerState(view) {
		if (view.$$("menu-pager")) {
			if (view.$$("sheets").count() <= view.$$("sheets").config.xCount) {
				webix.html.addCss(view.$$("menu-pager").$view, "webix_sheet_hidden", true);
			} else {
				webix.html.removeCss(view.$$("menu-pager").$view, "webix_sheet_hidden");
			}
		}
	}
	
	function edit(view, name) {
		view.$$("sheets").edit(name);
	}
	
	function remove(view, name) {
		var index, newSheet;
		if (view._sheets.length > 1) {
			index = arr.findIndex(view._sheets, function (x) {
				return x.name == name;
			});
			view._sheets.splice(index, 1);
			if (!view._sheets[index]) index = 0;
			newSheet = view._sheets[index];
			view._activeSheet = null;
			if (view.$$("sheets")) {
				view.$$("sheets").remove(name);
				view.$$("sheets").refresh();
			}
			view.showSheet(newSheet.name);
			view.callEvent("onSheetRemove", [name]);
		}
	}
	
	function rename(view, name, newName) {
		if (name == newName) return;
	
		//correct sheet name
		var i = 1;
		while (!newName || getSheet(view, newName)) {
			newName = webix.i18n.spreadsheet.labels.sheet + i;
			i++;
		}
		newName = newName.replace(/[\*\?\:\[\]\\\/]/g, "").substring(0, 31);
	
		var sheet = getSheet(view, name);
		var sheets = view.$$("sheets");
	
		sheet.name = newName;
		if (view._activeSheet == name) view._activeSheet = newName;
	
		if (sheets) {
			var item = sheets.getItem(name);
			item.value = newName;
			sheets.data.changeId(name, newName);
			sheets.refresh(newName);
		}
	
		view.callEvent("onSheetRename", [name, newName]);
	}
	
	function serialize(view, obj) {
		getSheet(view, view._activeSheet).content = obj;
		return view._sheets;
	}
	
	function load(view, obj) {
		var name = "";
		obj = webix.isArray(obj) ? { sheets: obj } : obj;
	
		if ((obj.excel || !obj.sheets) && view._activeSheet) {
			//loading into active sheet
			name = view._activeSheet;
			getSheet(view, name).content = obj;
			refresh(view, obj);
		} else {
			//creating new set of sheets
			if (!obj.sheets) obj = newSheet(obj);
	
			name = obj.sheets[0].name;
	
			view._activeSheet = "";
			view._sheets = obj.sheets;
			reset(view, obj);
			show(view, name);
		}
	}
	
	function newSheet(obj) {
		return {
			sheets: [{
				name: webix.i18n.spreadsheet.labels.sheet + 1,
				content: obj
			}]
		};
	}
	
	function show(view, name) {
		if (name == view._activeSheet) return;
	
		if (view.callEvent("onBeforeSheetShow", [name])) {
			if (view._activeSheet) getSheet(view, view._activeSheet).content = view.serialize();
	
			view._activeSheet = name;
			var obj = getSheet(view, name).content;
			refresh(view, obj);
	
			view.callEvent("onAfterSheetShow", [name]);
		}
	}
	
	function getSheet(view, name) {
		return arr.find(view._sheets, function (x) {
			return x.name == name;
		});
	}
	
	function eachSheet(view, callback) {
		for (var i = 0; i < view._sheets.length; i++) {
			callback(view._sheets[i].name);
		}
	}
	
	function getActive(view) {
		return view._activeSheet;
	}
	
	function refresh(view, obj) {
		var _getDimension = (0, _array2.getDimension)(obj.data, view.config.rowCount, view.config.columnCount),
		    rows = _getDimension[0],
		    cols = _getDimension[1];
	
		view.callEvent("onReset", []);
		if (rows != view.config.rowCount || cols != view.config.columnCount) {
			view.config.rowCount = rows;
			view.config.columnCount = cols;
			view._resetTable();
		}
	
		view.callEvent("onDataParse", [obj]);
		view._table.refresh();
	}
	
	function copy(view) {
		add(view, view.serialize());
	}
	
	function callAction(view, id) {
		if (!view.$sheetMenu.data.getMark(id, "webix_disabled")) actionMap[id](view);
	}

/***/ }),
/* 49 */
/***/ (function(module, exports) {

	"use strict";
	
	exports.__esModule = true;
	exports.find = find;
	exports.findIndex = findIndex;
	exports.getDimension = getDimension;
	function find(arr, func) {
		var result = findIndex(arr, func);
		return arr[result];
	}
	
	function findIndex(arr, func) {
		var result = -1;
		for (var i = 0; result < 0 && i < arr.length; i++) {
			if (func(arr[i])) result = i;
		}
		return result;
	}
	
	function getDimension(data, rows, cols) {
		//var [ row, column, value, style] = obj.data[i];
		for (var i = 0; i < data.length; i++) {
			var line = data[i];
			if (line[0] > rows) rows = line[0];
			if (line[1] > cols) cols = line[1];
		}
		return [rows * 1, cols * 1];
	}

/***/ }),
/* 50 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports.init = init;
	
	var _image = __webpack_require__(51);
	
	var aiw = _interopRequireWildcard(_image);
	
	var _sparkline = __webpack_require__(54);
	
	var asw = _interopRequireWildcard(_sparkline);
	
	var _ranges = __webpack_require__(57);
	
	var rgs = _interopRequireWildcard(_ranges);
	
	var _dropdown = __webpack_require__(58);
	
	var dpd = _interopRequireWildcard(_dropdown);
	
	var _format = __webpack_require__(59);
	
	var fmt = _interopRequireWildcard(_format);
	
	var _conditional = __webpack_require__(60);
	
	var cnd = _interopRequireWildcard(_conditional);
	
	var _export = __webpack_require__(61);
	
	var exp = _interopRequireWildcard(_export);
	
	var _link = __webpack_require__(62);
	
	var lnk = _interopRequireWildcard(_link);
	
	var _print = __webpack_require__(63);
	
	var pnt = _interopRequireWildcard(_print);
	
	function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj["default"] = obj; return newObj; } }
	
	var bases = [aiw, asw, rgs, dpd, fmt, cnd, exp, lnk, pnt];
	var dialogs = {};
	
	function init(view) {
		for (var i = 0; i < bases.length; i++) {
			dialogs[bases[i].action] = new bases[i].DialogBox(view);
		}view.attachEvent("onCommand", function (action) {
			var box = dialogs[action.id];
			if (box) box.open();
		});
	}

/***/ }),
/* 51 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports.DialogBox = exports.action = undefined;
	
	var _common = __webpack_require__(52);
	
	var _writer = __webpack_require__(53);
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var action = exports.action = "add-image";
	
	var DialogBox = exports.DialogBox = function (_Dialog) {
		_inherits(DialogBox, _Dialog);
	
		function DialogBox() {
			_classCallCheck(this, DialogBox);
	
			return _possibleConstructorReturn(this, _Dialog.apply(this, arguments));
		}
	
		DialogBox.prototype.$show = function $show(box, form) {
			this.cell = this.view.getSelectedId();
			if (!this.cell) return false;
	
			form.elements.preview.setHTML("");
			form.elements.url.setValue("");
	
			//sync state of dialog to data with  the selected cell
			var value = this.view.getCellValue(this.cell.row, this.cell.column);
			var data = (0, _writer.parseImage)(value);
			if (data && data.url) form.elements.url.setValue(data.url);
	
			form.elements.url.focus();
		};
	
		DialogBox.prototype.$init = function $init() {
			var _this2 = this;
	
			var save = this.view.config.save;
			var server = save && save.images || null;
	
			return {
				view: "ssheet-dialog",
				head: webix.i18n.spreadsheet.labels["image-title"],
				move: true,
				position: "center",
				body: {
					view: "form",
					elements: [{ view: "text", name: "url", placeholder: webix.i18n.spreadsheet.labels["image-url"], on: {
							"onChange": showPreview
						} }, { view: "label", label: webix.i18n.spreadsheet.labels["image-or"], align: "center" }, { view: "uploader", label: webix.i18n.spreadsheet.labels["image-upload"], upload: server, on: {
							"onBeforeFileAdd": startLoad,
							"onFileUpload": endLoad
						} }, { view: "formlate", name: "preview", borderless: true, css: "webix_ssheet_preview", template: "", height: 50 }]
				},
				on: {
					onSaveClick: function onSaveClick() {
						return okClick(_this2);
					},
					onHideClick: function onHideClick() {
						return _this2.close();
					},
					onCancelClick: function onCancelClick() {
						return _this2.close();
					}
				}
			};
		};
	
		return DialogBox;
	}(_common.Dialog);
	
	function startLoad(file) {
		var _this3 = this;
	
		if (!this.config.upload) {
			var reader = new FileReader();
			reader.onload = function (e) {
				return _this3.getFormView().elements.url.setValue(e.target.result);
			};
			reader.readAsDataURL(file.file);
			return false;
		} else this.getFormView().elements.preview.setHTML("");
	}
	
	function endLoad(item, response) {
		this.getFormView().elements.url.setValue(response.imageURL);
	}
	
	function okClick(dialog) {
		var cell = dialog.cell;
		var image = dialog.$dialog.getBody().elements.url.getValue();
		if (!/^(https?:\/|data:image)\//i.test(image)) image = "http://" + image;
		dialog.view.addImage(cell.row, cell.column, image);
		dialog.close();
	}
	
	function showPreview(item) {
		if (item) this.getFormView().elements.preview.setHTML("<img class='webix_ssheet_cimage' src='" + item + "'></img>");
	}

/***/ }),
/* 52 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports.Dialog = undefined;
	
	__webpack_require__(19);
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var Dialog = exports.Dialog = function () {
		function Dialog(view) {
			_classCallCheck(this, Dialog);
	
			this.view = view;
		}
	
		Dialog.prototype.$init = function $init() {};
	
		Dialog.prototype.$show = function $show() {};
	
		Dialog.prototype.$hide = function $hide() {};
	
		Dialog.prototype.open = function open() {
			var _this = this;
	
			if (!this.$dialog) {
				this.$dialog = webix.ui(this.$init());
				this.$dialog.attachEvent("onHide", function () {
					return _this.$hide();
				});
			}
	
			var form = this.$dialog.getBody();
			this.$dialog.show();
			if (this.$show(this.$dialog, form) === false) this.close();
		};
	
		Dialog.prototype.close = function close() {
			webix.UIManager.setFocus(this.view);
			this.$dialog.hide();
		};
	
		return Dialog;
	}();

/***/ }),
/* 53 */
/***/ (function(module, exports) {

	"use strict";
	
	exports.__esModule = true;
	exports.image = image;
	exports.chart = chart;
	exports.parseImage = parseImage;
	exports.parseChart = parseChart;
	function image(url) {
		return "=IMAGE(\"" + url + "\")";
	}
	
	function chart(config) {
		var result = "=SPARKLINE(" + config.range + ",\"" + config.type + "\"";
		if (config.type === "bar") result += ",\"" + config.color + "\",\"" + config.negativeColor + "\"";else if (config.color) result += ",\"" + config.color + "\"";
		return result + ")";
	}
	
	function parseImage(value) {
		if (value && value.indexOf("=IMAGE(") === 0) return {
			url: unescape(value.substr(7, value.length - 8))
		};
	}
	
	function parseChart(value) {
		if (value && value.indexOf("=SPARKLINE(") === 0) {
			var text = value.substr(11, value.length - 12).split(",");
			return {
				range: text[0],
				type: unescape(text[1]),
				color: unescape(text[2]),
				negativeColor: unescape(text[3])
			};
		}
	}
	
	function unescape(text) {
		if (!text) return "";
	
		text = text.trim();
		if (text[0] === "\"") text = text.substr(1);
		if (text[text.length - 1] === "\"") text = text.substr(0, text.length - 1);
		return text;
	}

/***/ }),
/* 54 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports.DialogBox = exports.action = undefined;
	
	var _common = __webpack_require__(52);
	
	var _ranges = __webpack_require__(55);
	
	var _column_names = __webpack_require__(41);
	
	var _writer = __webpack_require__(53);
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var action = exports.action = "add-sparkline";
	
	var types = [{ id: "line", value: "Line" }, { id: "spline", value: "Spline" }, { id: "splineArea", value: "Spline Area" }, { id: "area", value: "Area" }, { id: "bar", value: "Bar" }, { id: "pie", value: "Pie" }];
	
	var DialogBox = exports.DialogBox = function (_Dialog) {
		_inherits(DialogBox, _Dialog);
	
		function DialogBox() {
			_classCallCheck(this, DialogBox);
	
			return _possibleConstructorReturn(this, _Dialog.apply(this, arguments));
		}
	
		DialogBox.prototype.$show = function $show(box, form) {
			this.cell = this.view.getSelectedId();
			if (!this.cell) return false;
	
			var els = form.elements;
			this.view.$handleSelection = function (st, end, a, b) {
				els.range.setValue(a + ":" + b);
				return false;
			};
	
			els.range.setValue("");
	
			//sync state of dialog to data with  the selected cell
			var value = this.view.getCellValue(this.cell.row, this.cell.column);
			var data = (0, _writer.parseChart)(value);
			if (data) {
				form.blockEvent();
				els.type.setValue(data.type);
				els.range.setValue(data.range);
				if (data.color) {
					els.color_def.setValue(data.color);
					els.color_pos.setValue(data.color);
				}
				if (data.negativeColor) els.color_neg.setValue(data.negativeColor);
				form.unblockEvent();
				renderPreview(this);
			}
	
			els.range.focus();
		};
	
		DialogBox.prototype.$hide = function $hide() {
			this.view.$handleSelection = null;
		};
	
		DialogBox.prototype.$init = function $init() {
			var _this2 = this;
	
			return {
				view: "ssheet-dialog",
				head: webix.i18n.spreadsheet.labels["sparkline-title"],
				move: true,
				position: "center",
				body: {
					view: "form", id: "form", visibleBatch: 1, on: { onChange: function onChange() {
							return renderPreview(_this2);
						} }, elements: [{ view: "richselect", name: "type", label: webix.i18n.spreadsheet.labels["sparkline-type"], value: "line", labelPosition: "left", suggest: {
							view: "ssheet-form-suggest",
							data: types
						}, on: {
							"onChange": typesChangeHandler
						} }, { view: "text", label: webix.i18n.spreadsheet.labels["sparkline-range"], name: "range" }, { view: "ssheet-colorpicker", label: webix.i18n.spreadsheet.labels["sparkline-color"], name: "color_def", id: "add_sparkline_color", value: "#6666FF", batch: "1" }, { view: "ssheet-colorpicker", label: webix.i18n.spreadsheet.labels["sparkline-positive"], name: "color_pos", value: "#6666FF", batch: "2" }, { view: "ssheet-colorpicker", label: webix.i18n.spreadsheet.labels["sparkline-negative"], name: "color_neg", value: "#FF6666", batch: "2" }, { view: "formlate", name: "preview", borderless: true, css: "webix_ssheet_preview", height: 50 }]
	
				},
				on: {
					onSaveClick: function onSaveClick() {
						return okClick(_this2);
					},
					onHideClick: function onHideClick() {
						return _this2.close();
					},
					onCancelClick: function onCancelClick() {
						return _this2.close();
					}
				}
			};
		};
	
		DialogBox.prototype.checkRange = function checkRange(text) {
			if (text && (0, _column_names.range)(text, this.view)) return true;
			this.view.alert({ text: webix.i18n.spreadsheet.labels["error-range"] });
		};
	
		return DialogBox;
	}(_common.Dialog);
	
	function renderPreview(dialog) {
		var form = dialog.$dialog.getBody();
		var data = form.getValues();
	
		if (data.range && dialog.checkRange(data.range)) {
			var i = void 0,
			    values = (0, _ranges.rangeValue)(dialog.view, data.range),
			    config = sparkConfig(form);
			for (i = 0; i < values.length; i++) {
				values[i] = values[i] || 0;
			}form.elements.preview.setValue(webix.Sparklines.getTemplate(config)(values, { width: 200, height: 40 }));
		}
	}
	
	function sparkConfig(form, config) {
		var data = form.getValues();
		config = config || { type: data.type };
		if (data.type === "bar") {
			config.color = data.color_pos;
			config.negativeColor = data.color_neg;
		} else if (form.elements.color_def.isVisible()) config.color = data.color_def;
	
		return config;
	}
	
	function okClick(dialog) {
		var form = dialog.$dialog.getBody();
		var config = sparkConfig(form, form.getValues());
		if (dialog.checkRange(config.range)) {
			dialog.view.addSparkline(dialog.cell.row, dialog.cell.column, config);
			dialog.close();
		}
	}
	
	function typesChangeHandler(type) {
		var form = this.getFormView();
		switch (type) {
			case "pie":
				form.showBatch(3);
				break;
			case "bar":
				form.showBatch(2);
				break;
			default:
				form.showBatch(1);
		}
	}

/***/ }),
/* 55 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports.Ranges = undefined;
	exports.rangeValue = rangeValue;
	
	var _column_names = __webpack_require__(41);
	
	var _sheet = __webpack_require__(56);
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function rangeValue(view, text, extra) {
		var pos = (0, _column_names.range)(text, view);
		var page = (0, _sheet.getAccessor)(view, pos[4]);
	
		var vals = page.getRange.apply(page, pos);
	
		if (extra) {
			if (extra.unique) {
				var t = {};
				for (var i = vals.length - 1; i >= 0; i--) {
					if (t[vals[i]]) vals.splice(i, 1);else t[vals[i]] = true;
				}
			}
			if (extra.order) vals.sort();
			if (extra.empty) vals.unshift({ id: "$empty", $empty: true, value: "" });
		}
	
		return vals;
	}
	
	var Ranges = exports.Ranges = function () {
		function Ranges(view) {
			_classCallCheck(this, Ranges);
	
			this._master = view;
			this._ranges = {};
		}
	
		Ranges.prototype.clear = function clear() {
			this._ranges = {};
		};
	
		Ranges.prototype.add = function add(name, text) {
			var needUpdate = !!this._ranges[name];
	
			this._ranges[name] = text;
	
			if (needUpdate) this._master.callEvent("onMathRefresh", []);
		};
	
		Ranges.prototype.getCode = function getCode(name, sheet) {
			if (sheet) return (0, _sheet.getAccessor)(this._master, sheet).getRangeCode(name);
	
			return this._ranges[name];
		};
	
		Ranges.prototype.remove = function remove(name) {
			delete this._ranges[name];
		};
	
		Ranges.prototype.getRanges = function getRanges() {
			var data = [];
			for (var name in this._ranges) {
				data.push({ name: name, range: this._ranges[name] });
			}return data;
		};
	
		Ranges.prototype.parse = function parse(data) {
			if (!data) return;
	
			var i = data.length;
			while (i--) {
				var c = data[i];
				this._ranges[c[0]] = c[1];
			}
		};
	
		Ranges.prototype.serialize = function serialize() {
			var name,
			    data = [];
			for (name in this._ranges) {
				data.push([name, this._ranges[name]]);
			}return data;
		};
	
		return Ranges;
	}();

/***/ }),
/* 56 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports.init = init;
	exports.getAccessor = getAccessor;
	
	var _sheets = __webpack_require__(48);
	
	var _column_names = __webpack_require__(41);
	
	//TODO
	// ### column deleting | adding
	// recalculate positions
	
	// ### copy paste between sheets
	// force sheet name
	
	// ### triggers
	// a <- p <- a
	// register triggers from passive sheet
	
	// ### sheet renaming
	// adjust math on sheet rename
	// adjust ranges on sheet rename
	
	function init(view, core, helpers) {
		view.attachEvent("onBeforeSheetShow", function () {
			delete view._mathSheetCache[(0, _sheets.getActive)(view)];
		});
		view.attachEvent("onSheetRename", function (name) {
			delete view._mathSheetCache[name];
		});
		view.attachEvent("onSheetRemove", function (name) {
			delete view._mathSheetCache[name];
		});
	
		view._mathSheetCache = {};
		view._mathSheetCore = core;
		view._mathSheetHelpers = helpers;
	}
	
	function getAccessor(view, name) {
		//actuve sheet accessor
		if (!name) return SheetAccessor(activeAccessor(view));
	
		//passive sheet accessor
		if (!view._mathSheetCache[name]) {
			var temp = view._mathSheetCache[name] = name === (0, _sheets.getActive)(view) ? SheetAccessor(activeAccessor(view)) : SheetAccessor(passiveAccessor(view, name));
			temp.init();
		}
	
		return view._mathSheetCache[name];
	}
	
	function SheetAccessor(helpers) {
		return {
			init: function init() {
				helpers.init();
			},
			getRangeCode: function getRangeCode(name) {
				return helpers.getRangeCode(name);
			},
			getValue: function getValue(r, c) {
				var val = helpers.getCell(r, c);
				var test = val * 1;
				if (!isNaN(test)) return test;
				return val ? val : "";
			},
			getRangeValue: function getRangeValue(name) {
				var code = this.getRangeCode(name) || name;
				if (code.indexOf(":") == -1) return [];
				var pos = (0, _column_names.range)(code);
				return this.getRange.apply(this, pos);
			},
			getRange: function getRange(r1, c1, r2, c2) {
				var set = helpers.getRange(r1, c1, r2, c2);
				for (var i = set.length - 1; i >= 0; i--) {
					var val = set[i];
					var test = val * 1;
					set[i] = isNaN(test) ? val ? val : "" : test;
				}
				return set;
			}
		};
	}
	
	function activeAccessor(view) {
		return {
			init: function init() {},
			getRangeCode: function getRangeCode(name) {
				return view.ranges.getCode(name);
			},
	
			getRow: function getRow(r) {
				return view.getRow(r);
			},
			getCell: function getCell(r, c) {
				return view.getRow(r)[c];
			},
			getRange: function getRange(r1, c1, r2, c2) {
				var set = [];
				for (var i = r1; i <= r2; i++) {
					var item = view.getRow(i);
					for (var j = c1; j <= c2; j++) {
						set.push(item[j]);
					}
				}
				return set;
			}
		};
	}
	
	function passiveAccessor(view, name) {
		var page = (0, _sheets.getSheet)(view, name).content;
		var table = [];
		var formulas = [];
	
		var ranges = [];
	
		return {
			init: function init() {
				if (page.ranges) {
					for (var i = 0; i < page.ranges.length; i++) {
						var line = page.ranges[i];
						ranges[line[0]] = line[1];
					}
				}
	
				if (page.data) {
					for (var _i = 0; _i < page.data.length; _i++) {
						//expand data structure
						var _page$data$_i = page.data[_i],
						    row = _page$data$_i[0],
						    column = _page$data$_i[1],
						    value = _page$data$_i[2];
	
						if (!table[row]) table[row] = [];
						table[row][column] = value;
	
						if (value[0] === "=") {
							//for math cells, store the code
							var math = view._mathSheetHelpers.parse(value, view._mathSheetCore, name);
							table[row][column] = math.handler;
	
							//store list of cross-sheet formulas
							for (var j = 0; j < math.triggers.length; j++) {
								if (math.triggers[j][5]) {
									formulas.push(page.data[_i]);
									break;
								}
							}
						}
					}
				}
			},
			getRangeCode: function getRangeCode(name) {
				return ranges[name];
			},
			getRow: function getRow(i) {
				return table[i] || [];
			},
			getCell: function getCell(i, j) {
				var value = this.getRow(i)[j];
				if (typeof value === "function") return view._mathSheetHelpers.execute(value, view._mathSheetCore);
	
				return value;
			},
	
			getRange: function getRange(r1, c1, r2, c2) {
				var set = [];
				for (var i = r1; i <= r2; i++) {
					for (var j = c1; j <= c2; j++) {
						set.push(this.getCell(i, j));
					}
				}return set;
			}
		};
	}

/***/ }),
/* 57 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports.DialogBox = exports.action = undefined;
	
	var _common = __webpack_require__(52);
	
	var _column_names = __webpack_require__(41);
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var action = exports.action = "add-range";
	
	var DialogBox = exports.DialogBox = function (_Dialog) {
		_inherits(DialogBox, _Dialog);
	
		function DialogBox() {
			_classCallCheck(this, DialogBox);
	
			return _possibleConstructorReturn(this, _Dialog.apply(this, arguments));
		}
	
		DialogBox.prototype.$show = function $show(box) {
			var table = box.$$("table");
			var form = box.$$("form");
			var sheet = ""; // this.view.getActiveSheet(); ranges are per-sheet by default
	
			form.clear();
			form.elements.name.focus();
			form.elements.range.setValue(this.view.getSelectedRange());
	
			table.clearAll();
			table.parse(this.view.ranges.getRanges());
	
			this.view.$handleSelection = function (st, end, a, b) {
				form.elements.range.setValue((0, _column_names.toSheetRange)(a, b, sheet));
				return false;
			};
		};
	
		DialogBox.prototype.$hide = function $hide() {
			this.view.$handleSelection = null;
		};
	
		DialogBox.prototype.saveClick = function saveClick() {
			var form = this.$dialog.$$("form");
			var data = form.getValues();
	
			var sheets = data.range.indexOf("!");
			data.range = data.range.substr(0, sheets + 1) + data.range.substr(sheets + 1).toUpperCase();
			data.name = data.name.toUpperCase();
	
			form.setValues(data);
	
			if (form.validate()) {
				var table = this.$dialog.$$("table");
	
				if (data.id && table.exists(data.id)) {
					this.view.ranges.remove(table.getItem(data.id).name);
					table.updateItem(data.id, data);
				} else this.$dialog.$$("table").add(data);
	
				this.view.ranges.add(data.name, data.range);
				form.clear();
			}
		};
	
		DialogBox.prototype.removeRange = function removeRange(id) {
			var _this2 = this;
	
			this.view.confirm({
				text: webix.i18n.spreadsheet.labels["range-remove-confirm"],
				callback: function callback(res) {
					if (res) {
						var table = _this2.$dialog.$$("table");
						_this2.view.ranges.remove(table.getItem(id).name);
						table.remove(id);
					}
				}
			});
		};
	
		DialogBox.prototype.editRange = function editRange(id) {
			var form = this.$dialog.$$("form");
			form.setValues(this.$dialog.$$("table").getItem(id));
		};
	
		DialogBox.prototype.$init = function $init() {
			var _this3 = this;
	
			var theform = {
				type: "clean",
				cols: [{ view: "ssheet-dialog-table", id: "table", borderless: true, columns: [{ id: "name", header: webix.i18n.spreadsheet.labels["range-name"], width: 120 }, { id: "range", header: webix.i18n.spreadsheet.labels["range-cells"], width: 140 }, { template: "<div class='webix_icon webix_ssheet_range_edit'></div>", width: 30 }, { template: "<div class='webix_icon webix_ssheet_range_delete'></div>", width: 30 }],
					autowidth: true, height: 150,
					onClick: {
						"webix_ssheet_range_delete": function webix_ssheet_range_delete(ev, id) {
							return _this3.removeRange(id);
						},
						"webix_ssheet_range_edit": function webix_ssheet_range_edit(ev, id) {
							return _this3.editRange(id);
						}
					}
				}, { width: 250, view: "form", id: "form",
					rules: {
						name: function name(value) {
							var correct = /^[A-Za-z]+$/.test(value);
							var table = _this3.$dialog.$$("table");
							var data = _this3.$dialog.$$("form").getValues();
	
							var unique = true;
							table.eachRow(function (id) {
								var obj = this.getItem(id);
								if (obj.name == value && obj.id != data.id) unique = false;
							});
	
							return correct && unique;
						},
						range: _column_names.isRange
					},
					elementsConfig: {
						labelWidth: 70
					},
					elements: [{ view: "text", name: "name", gravity: 1, label: webix.i18n.spreadsheet.labels["range-name"] }, { view: "text", name: "range", gravity: 1, label: webix.i18n.spreadsheet.labels["range-cells"] }, { cols: [{}, { view: "button", value: "Save", click: function click() {
								return _this3.saveClick();
							} }] }]
				}]
			};
	
			return {
				view: "ssheet-dialog",
				move: true,
				head: webix.i18n.spreadsheet.labels["range-title"],
				buttons: false,
				autoheight: true,
				width: 610,
				position: "center",
				body: theform,
				on: {
					onSaveClick: function onSaveClick() {
						return _this3.close();
					},
					onHideClick: function onHideClick() {
						return _this3.close();
					},
					onCancelClick: function onCancelClick() {
						return _this3.close();
					}
				}
			};
		};
	
		return DialogBox;
	}(_common.Dialog);

/***/ }),
/* 58 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports.DialogBox = exports.action = undefined;
	
	var _common = __webpack_require__(52);
	
	var _column_names = __webpack_require__(41);
	
	var _undo = __webpack_require__(39);
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var action = exports.action = "add-dropdown";
	
	var DialogBox = exports.DialogBox = function (_Dialog) {
		_inherits(DialogBox, _Dialog);
	
		function DialogBox() {
			_classCallCheck(this, DialogBox);
	
			return _possibleConstructorReturn(this, _Dialog.apply(this, arguments));
		}
	
		DialogBox.prototype.$show = function $show() {
			var form = this.$dialog.$$("form");
			this.cell = this.view.getSelectedId(true);
			if (!this.cell.length) return false;
	
			form.clear();
	
			//restore previosly selected range
			var value = this.view.getCellEditor(this.cell[0].row, this.cell[0].column);
			if (value && value.options) form.elements.range.setValue(value.options);
	
			this.view.$handleSelection = function (st, end, a, b) {
				form.elements.range.setValue(a + ":" + b);
				return false;
			};
	
			form.elements.range.focus();
		};
	
		DialogBox.prototype.$hide = function $hide() {
			this.view.$handleSelection = null;
		};
	
		DialogBox.prototype.$init = function $init() {
			var _this2 = this;
	
			return {
				view: "ssheet-dialog",
				position: "center",
				head: webix.i18n.spreadsheet.labels["dropdown-title"],
				move: true,
				body: {
					view: "form", id: "form", rows: [{ view: "text", label: webix.i18n.spreadsheet.labels["dropdown-range"], name: "range" }]
				},
				on: {
					onSaveClick: function onSaveClick() {
						return _this2.okClick();
					},
					onHideClick: function onHideClick() {
						return _this2.close();
					},
					onCancelClick: function onCancelClick() {
						return _this2.close();
					}
				}
			};
		};
	
		DialogBox.prototype.okClick = function okClick() {
			var values = this.$dialog.$$("form").elements.range.getValue();
	
			if (values) {
				if (!(0, _column_names.range)(values, this.view)) this.view.alert({ text: webix.i18n.spreadsheet.labels["error-range"] });else {
					_undo.group.set(function () {
						for (var i = 0; i < this.cell.length; i++) {
							this.view.setCellEditor(this.cell[i].row, this.cell[i].column, { editor: "richselect", options: values });
						}
					}, this);
					this.close();
				}
			} else this.close();
		};
	
		return DialogBox;
	}(_common.Dialog);

/***/ }),
/* 59 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports.DialogBox = exports.action = undefined;
	
	var _common = __webpack_require__(52);
	
	var _formats = __webpack_require__(34);
	
	var fmt = _interopRequireWildcard(_formats);
	
	function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj["default"] = obj; return newObj; } }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var action = exports.action = "custom-format";
	
	var DialogBox = exports.DialogBox = function (_Dialog) {
		_inherits(DialogBox, _Dialog);
	
		function DialogBox() {
			_classCallCheck(this, DialogBox);
	
			return _possibleConstructorReturn(this, _Dialog.apply(this, arguments));
		}
	
		DialogBox.prototype.$show = function $show() {
			var form = this.$dialog.$$("form");
			this.cell = this.view.getSelectedId(true);
			if (!this.cell) return false;
	
			form.clear();
			form.elements.format.focus();
	
			if (this.cell.length > 0) {
				var style = this.view.getStyle(this.cell[0].row, this.cell[0].column);
				if (style) form.elements.format.setValue(fmt.getFormatSource(style.props.format));
			}
		};
	
		DialogBox.prototype.$init = function $init() {
			var _this2 = this;
	
			return {
				view: "ssheet-dialog",
				position: "center",
				head: webix.i18n.spreadsheet.labels["format-title"],
				move: true,
				body: {
					view: "form", id: "form", rows: [{ view: "text", label: webix.i18n.spreadsheet.labels["format-pattern"], name: "format", labelPosition: "top",
						placeholder: "[>100]0,000.00;[>0]None" }]
				},
				on: {
					onSaveClick: function onSaveClick() {
						return _this2.okClick();
					},
					onHideClick: function onHideClick() {
						return _this2.close();
					},
					onCancelClick: function onCancelClick() {
						return _this2.close();
					}
				}
			};
		};
	
		DialogBox.prototype.okClick = function okClick() {
			var value = this.$dialog.$$("form").elements.format.getValue();
			for (var i = 0; i < this.cell.length; i++) {
				this.view.setFormat(this.cell[i].row, this.cell[i].column, value);
			}this.view.refresh();
			this.close();
		};
	
		return DialogBox;
	}(_common.Dialog);

/***/ }),
/* 60 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports.DialogBox = exports.action = undefined;
	
	var _common = __webpack_require__(52);
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var action = exports.action = "conditional-format";
	
	var condition = [{ id: ">", value: ">" }, { id: "<", value: "<" }, { id: "=", value: "=" }, { id: "<>", value: "<>" }];
	
	function getConditionStyle(data) {
		return data.map(function (item) {
			item.id = item.css;
			return item;
		});
	}
	
	var DialogBox = exports.DialogBox = function (_Dialog) {
		_inherits(DialogBox, _Dialog);
	
		function DialogBox() {
			_classCallCheck(this, DialogBox);
	
			return _possibleConstructorReturn(this, _Dialog.apply(this, arguments));
		}
	
		DialogBox.prototype._getCondition = function _getCondition() {
			var _this2 = this;
	
			var view = this.view;
			var forms = this.$dialog.getBody().getChildViews()[0].getChildViews()[0].getChildViews();
			var data = [];
	
			forms.forEach(function (form) {
				if (!form.getValues) return;
				var values = form.getValues();
				var item = [];
				item.push(values.condition);
	
				if (values.condition !== "<>") {
					item.push(_this2._safeInt(values.value, 10));
				} else {
					item.push([_this2._safeInt(values.value, 10), _this2._safeInt(values.value2, 10)]);
				}
				item.push(values.style);
				for (var i = 0; i < item.length; i++) {
					if (item[i] === "") return false;
				}
				data.push(item);
			});
	
			view.callEvent("onConditionSet", [data]);
		};
	
		DialogBox.prototype._safeInt = function _safeInt(a) {
			var num = parseFloat(a);
			if (num == a) return num;
			return a;
		};
	
		DialogBox.prototype._setCondition = function _setCondition() {
			var view = this.view,
			    select = view.getSelectedId(true),
			    cell = select[0];
	
			if (!select.length) return false;
			var collection = view.conditions.get(cell.row, cell.column);
	
			if (collection) {
				var forms = this.$dialog.getBody().getChildViews()[0].getChildViews()[0].getChildViews();
				collection.forEach(function (item, i) {
					var values = {};
					values.condition = item[0];
					if (webix.isArray(item[1])) {
						values.value = item[1][0].toString();
						values.value2 = item[1][1].toString();
					} else values.value = item[1].toString();
					values.style = item[2];
					forms[i + 1].setValues(values);
				});
			}
		};
	
		DialogBox.prototype.apply = function apply() {
			this._getCondition();
			this.close();
		};
	
		DialogBox.prototype._clean = function _clean() {
			var forms = this.$dialog.getBody().getChildViews()[0].getChildViews()[0].getChildViews();
			forms.forEach(function (form) {
				if (form.setValues) form.setValues({ condition: "", value: "", value2: "", style: "" });
			});
		};
	
		DialogBox.prototype.$show = function $show() {
			this.view.disable();
			this._setCondition();
		};
	
		DialogBox.prototype.$hide = function $hide() {
			this.view.enable();
			this._clean();
		};
	
		DialogBox.prototype.getRows = function getRows(data, count) {
			var headers = {
				height: 36,
				cols: [{ view: "label", label: webix.i18n.spreadsheet.labels.display }, { view: "label", label: webix.i18n.spreadsheet.labels.condition }, { view: "label", label: webix.i18n.spreadsheet.labels.value }, { width: 25 }]
			};
			var arr = [headers];
	
			var item = {
				view: "form",
				padding: 0,
				borderless: true,
	
				elements: [{
					margin: 10,
					cols: [{
						view: "richselect",
						name: "style",
						placeholder: webix.i18n.spreadsheet.labels["conditional-style"],
						css: "webix_ssheet_cformat_select",
						suggest: {
							padding: 0,
							borderless: true,
							css: "webix_ssheet_cformat_list",
							body: {
								template: function template(obj) {
									var css = "webix_ssheet_cformat " + obj.css;
									return "<div class=\"" + css + "\">" + obj.name + "</div>";
								},
	
								data: data
							}
						}
					}, {
						view: "richselect",
						width: 100,
						name: "condition",
						placeholder: webix.i18n.spreadsheet.labels["conditional-operator"],
						on: {
							onChange: function onChange(newv) {
								if (newv === "<>") {
									this.getFormView().elements.value2.show();
								} else {
									this.getFormView().elements.value2.hide();
								}
							}
						},
						suggest: {
							view: "ssheet-form-suggest",
							body: {
								data: condition
							}
						}
					}, {
						cols: [{
							view: "text",
							placeholder: webix.i18n.spreadsheet.labels["conditional-number"],
							name: "value"
						}, {
							view: "text",
							name: "value2",
							hidden: true
						}]
					}, { view: "icon", icon: "trash-o", width: 25, click: clearRow }] }] };
	
			while (count--) {
				arr.push(item);
			}
			return arr;
		};
	
		DialogBox.prototype.$init = function $init() {
			var _this3 = this;
	
			//[IMPROVE] can be optimized by using datalayout instead of nested forms
			return {
				view: "ssheet-dialog",
				head: webix.i18n.spreadsheet.labels["conditional-format"],
				position: "center",
				width: 450,
				move: true,
				buttons: false,
				body: {
					view: "form",
					borderless: true,
					rows: [{ rows: this.getRows(getConditionStyle(this.view.config.conditionStyle), 3) }, {
						cols: [{ gravity: 2 }, {
							view: "button",
							value: webix.i18n.spreadsheet.labels.apply,
							click: function click() {
								return _this3.apply();
							}
						}]
					}]
				},
				on: {
					onHideClick: function onHideClick() {
						return _this3.close();
					}
				}
			};
		};
	
		return DialogBox;
	}(_common.Dialog);
	
	function clearRow() {
		//this - clear icon
		var form = this.getFormView();
		form.clear();
	}

/***/ }),
/* 61 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports.DialogBox = exports.action = undefined;
	
	var _common = __webpack_require__(52);
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var action = exports.action = "excel-export";
	
	var DialogBox = exports.DialogBox = function (_Dialog) {
		_inherits(DialogBox, _Dialog);
	
		function DialogBox() {
			_classCallCheck(this, DialogBox);
	
			return _possibleConstructorReturn(this, _Dialog.apply(this, arguments));
		}
	
		DialogBox.prototype.$show = function $show(box, form) {
			this.view.disable();
	
			form.elements.sheets.setValue(this.getSheets());
			form.elements.filename.setValue("Data");
			form.elements.filename.getInputNode().select();
		};
	
		DialogBox.prototype.$hide = function $hide() {
			this.view.enable();
		};
	
		DialogBox.prototype.$init = function $init() {
			var _this2 = this;
	
			return {
				view: "ssheet-dialog",
				head: webix.i18n.spreadsheet.labels["export-title"],
				move: true,
				position: "center",
				body: {
					view: "form",
					elements: [{ view: "text", name: "filename", placeholder: webix.i18n.spreadsheet.labels["export-name"] }, { view: "multicheckbox", name: "sheets" }]
				},
				on: {
					onSaveClick: function onSaveClick() {
						return okClick(_this2);
					},
					onHideClick: function onHideClick() {
						return _this2.close();
					},
					onCancelClick: function onCancelClick() {
						return _this2.close();
					}
				}
			};
		};
	
		DialogBox.prototype.getSheets = function getSheets() {
			var view = this.view;
			var sheets = view._sheets;
			var value = {};
	
			if (sheets && sheets.length > 1) {
				for (var i = 0; i < sheets.length; i++) {
					value[sheets[i].name] = view._activeSheet === sheets[i].name ? 1 : 0;
				}
			}
			return value;
		};
	
		return DialogBox;
	}(_common.Dialog);
	
	function okClick(dialog) {
		var values = dialog.$dialog.getBody().getValues();
		webix.toExcel(dialog.view, values);
		dialog.close();
	}

/***/ }),
/* 62 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports.DialogBox = exports.action = undefined;
	
	var _common = __webpack_require__(52);
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var action = exports.action = "add-link";
	
	var DialogBox = exports.DialogBox = function (_Dialog) {
		_inherits(DialogBox, _Dialog);
	
		function DialogBox() {
			_classCallCheck(this, DialogBox);
	
			return _possibleConstructorReturn(this, _Dialog.apply(this, arguments));
		}
	
		DialogBox.prototype.$show = function $show(box, form) {
			var cell = this.view.getSelectedId();
			if (!cell) return false;
			if (!this.restoreValue(form)) form.clear();
			form.elements.name.focus();
		};
	
		DialogBox.prototype.restoreValue = function restoreValue(form) {
			var cell = this.view.getSelectedId();
			if (cell) {
				var item = this.view.getRow(cell.row);
				var value = item["$" + cell.column] || item[cell.column];
				if (value && value.indexOf("=LINK") === 0) {
					var parts = value.split("\"");
					form.setValues({
						name: parts[3] || "",
						url: parts[1] || ""
					});
					return true;
				}
			}
			return false;
		};
	
		DialogBox.prototype.$init = function $init() {
			var _this2 = this;
	
			return {
				view: "ssheet-dialog",
				head: webix.i18n.spreadsheet.labels["link-title"],
				move: true,
				position: "center",
				body: {
					view: "form",
					elements: [{ view: "text", name: "name", placeholder: webix.i18n.spreadsheet.labels["link-name"] }, { view: "text", name: "url", placeholder: webix.i18n.spreadsheet.labels["link-url"] }]
				},
				on: {
					onSaveClick: function onSaveClick() {
						return okClick(_this2);
					},
					onHideClick: function onHideClick() {
						return _this2.close();
					},
					onCancelClick: function onCancelClick() {
						return _this2.close();
					}
				}
			};
		};
	
		return DialogBox;
	}(_common.Dialog);
	
	function okClick(dialog) {
		var cell = dialog.view.getSelectedId();
		var data = dialog.$dialog.getBody().getValues();
	
		var text = "";
		if (data.url) {
			data.name = data.name || data.url;
			if (!/^https?:\/\//i.test(data.url)) data.url = "http://" + data.url;
			text = "=LINK(\"" + data.url + "\",\"" + data.name + "\")";
		}
	
		dialog.view.setCellValue(cell.row, cell.column, text);
		dialog.view.refresh();
		dialog.close();
	}

/***/ }),
/* 63 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports.DialogBox = exports.action = undefined;
	
	var _common = __webpack_require__(52);
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var action = exports.action = "print";
	
	var DialogBox = exports.DialogBox = function (_Dialog) {
		_inherits(DialogBox, _Dialog);
	
		function DialogBox() {
			_classCallCheck(this, DialogBox);
	
			return _possibleConstructorReturn(this, _Dialog.apply(this, arguments));
		}
	
		DialogBox.prototype.$show = function $show(box, form) {
			form.setValues({
				data: "current",
				paper: "A4",
				fit: "page",
				mode: "landscape",
				sheetnames: 1,
				margin: 0
			});
		};
	
		DialogBox.prototype.$init = function $init() {
			var _this2 = this;
	
			return {
				view: "ssheet-dialog",
				head: webix.i18n.spreadsheet.labels["print-title"],
				move: true,
				modal: true,
				width: 520, height: 520,
				position: "center",
				buttons: false,
				body: {
					view: "form",
					elements: [{ type: "section", template: webix.i18n.spreadsheet.labels["print-settings"] }, { cols: [{ view: "radio", name: "data", vertical: true, options: [{ id: "current", value: webix.i18n.spreadsheet.labels["current-sheet"] }, { id: "all", value: webix.i18n.spreadsheet.labels["all-sheets"] }, { id: "selection", value: webix.i18n.spreadsheet.labels["selection"] }], on: {
								onChange: function onChange(newv) {
									if (newv == "all") {
										var form = this.getFormView();
										form.elements.sheetnames.setValue(1);
									}
								}
							} }, { rows: [{ view: "checkbox", name: "sheetnames", labelRight: webix.i18n.spreadsheet.labels["sheet-names"] }, { view: "checkbox", name: "borderless", labelRight: webix.i18n.spreadsheet.labels["borderless"] }, { view: "checkbox", name: "skiprows", labelRight: webix.i18n.spreadsheet.labels["skip-rows"] }, { view: "checkbox", name: "margin", labelRight: webix.i18n.spreadsheet.labels["margin"] }] }] }, { type: "section", template: webix.i18n.spreadsheet.labels["print-paper"] }, { view: "radio", name: "paper", options: [{ id: "letter", value: webix.i18n.spreadsheet.labels["page-letter"] }, { id: "A4", value: webix.i18n.spreadsheet.labels["page-a4"] }, { id: "A3", value: webix.i18n.spreadsheet.labels["page-a3"] }] }, { type: "section", template: webix.i18n.spreadsheet.labels["print-layout"] }, { cols: [{ view: "radio", name: "fit", options: [{ id: "page", value: webix.i18n.spreadsheet.labels["page-width"] }, { id: "data", value: webix.i18n.spreadsheet.labels["page-actual"] }] }, { width: 35 }, { view: "radio", width: 220, name: "mode", options: [{ id: "portrait", value: webix.i18n.spreadsheet.labels["page-portrait"] }, { id: "landscape", value: webix.i18n.spreadsheet.labels["page-landscape"] }] }] }, {
						cols: [{}, { view: "button", type: "cancel", value: webix.i18n.spreadsheet.labels.cancel, autowidth: true,
							click: function click() {
								return _this2.close();
							}
						}, { view: "button", value: webix.i18n.spreadsheet.labels.print, autowidth: true,
							click: function click() {
								return _this2.apply(_this2);
							}
						}]
					}]
				},
				on: {
					onHideClick: function onHideClick() {
						return _this2.close();
					}
				}
			};
		};
	
		DialogBox.prototype.apply = function apply(dialog) {
			var data = dialog.$dialog.getBody().getValues();
			data.margin = data.margin ? 0 : {};
			this.close();
			webix.print(dialog.view, data);
		};
	
		return DialogBox;
	}(_common.Dialog);

/***/ }),
/* 64 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports.init = init;
	exports.getMenuData = getMenuData;
	
	var _column_operations = __webpack_require__(37);
	
	function init(view) {
		view.attachEvent("onComponentInit", function () {
			return view.$$("context").attachTo(view._table.$view);
		});
	
		var locale = webix.i18n.spreadsheet.menus;
		var lists = {
			data: [{ id: "clear-styles", value: locale["clear-styles"] }, { id: "lock-cell", value: locale["lock-cell"] }, { $template: "Separator" }, { id: "sort", value: locale.sort, submenu: [{ id: "sort-asc", value: locale["sort-asc"] }, { id: "sort-desc", value: locale["sort-desc"] }] }, { id: "create-filter", value: locale["create-filter"] }, { $template: "Separator" }, { id: "add-range", value: locale["add-range"] }, { id: "add-link", value: locale["add-link"] }],
			column: [{ id: "add", group: "column", value: locale["insert-column"] }, { id: "del", group: "column", value: locale["delete-column"] }, { id: "show", group: "column", value: locale["show-column"] }, { id: "hide", group: "column", value: locale["hide-column"] }],
			row: [{ id: "add", group: "row", value: locale["insert-row"] }, { id: "del", group: "row", value: locale["delete-row"] }, { id: "show", group: "row", value: locale["show-row"] }, { id: "hide", group: "row", value: locale["hide-row"] }]
		};
	
		var context = {
			view: "contextmenu",
			id: "context",
			padding: 0,
			submenuConfig: {
				padding: 0
			},
			data: [],
			on: {
				onMenuItemClick: function onMenuItemClick(id) {
	
					view.callEvent("onCommand", [this.getMenuItem(id)]);
				},
				onBeforeShow: function onBeforeShow(pos) {
					var mode, trg;
	
					trg = view._table.locate(pos);
					if (!trg) return false;
	
					mode = "";
					if (trg.header && trg.column !== "rowId") {
						if (!(0, _column_operations.isColSelected)(trg.column, view)) (0, _column_operations.selectColumn)(trg.column, view);
						mode = "column";
					} else if (trg.row) {
						if (trg.column === "rowId") {
							if (!(0, _column_operations.isRowSelected)(trg.row, view)) (0, _column_operations.selectRow)(trg.row, view);
							mode = "row";
						} else {
							if (!(0, _column_operations.isCellSelected)(trg.row, trg.column, view)) view._table.addSelectArea(trg, trg);
							mode = "data";
						}
					}
	
					if (mode) {
						var data = getMenuData(view, mode, lists[mode]);
						if (data) {
							this.clearAll();
							this.parse(data);
							webix.html.preventEvent(pos);
							return true;
						}
					}
					return false;
				}
			}
		};
	
		view.callEvent("onViewInit", ["context", context]);
		view.ui(context);
	}
	
	function getMenuData(view, name, data) {
		var ev = { area: name, data: data };
		if (view.callEvent("onContextMenuConfig", [ev])) return ev && ev.data;
		return null;
	}

/***/ }),
/* 65 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports.operations = operations;
	
	var _add_row_col = __webpack_require__(66);
	
	var m1 = _interopRequireWildcard(_add_row_col);
	
	var _clipboard = __webpack_require__(70);
	
	var m6 = _interopRequireWildcard(_clipboard);
	
	var _column_operations = __webpack_require__(37);
	
	var m12 = _interopRequireWildcard(_column_operations);
	
	var _conditional_formats = __webpack_require__(71);
	
	var m2 = _interopRequireWildcard(_conditional_formats);
	
	var _math = __webpack_require__(72);
	
	var m16a = _interopRequireWildcard(_math);
	
	var _data = __webpack_require__(74);
	
	var m17 = _interopRequireWildcard(_data);
	
	var _dropdown = __webpack_require__(75);
	
	var m18 = _interopRequireWildcard(_dropdown);
	
	var _formats = __webpack_require__(34);
	
	var m7 = _interopRequireWildcard(_formats);
	
	var _hide_row_col = __webpack_require__(76);
	
	var m13 = _interopRequireWildcard(_hide_row_col);
	
	var _lock_cell = __webpack_require__(40);
	
	var m14 = _interopRequireWildcard(_lock_cell);
	
	var _resize = __webpack_require__(77);
	
	var m3 = _interopRequireWildcard(_resize);
	
	var _selection = __webpack_require__(78);
	
	var m9 = _interopRequireWildcard(_selection);
	
	var _sorting = __webpack_require__(79);
	
	var m10 = _interopRequireWildcard(_sorting);
	
	var _spans = __webpack_require__(81);
	
	var m16 = _interopRequireWildcard(_spans);
	
	var _states = __webpack_require__(82);
	
	var m19 = _interopRequireWildcard(_states);
	
	var _styles = __webpack_require__(38);
	
	var m5 = _interopRequireWildcard(_styles);
	
	var _undo = __webpack_require__(39);
	
	var m11 = _interopRequireWildcard(_undo);
	
	var _import = __webpack_require__(86);
	
	var m20 = _interopRequireWildcard(_import);
	
	var _filter = __webpack_require__(85);
	
	var m21 = _interopRequireWildcard(_filter);
	
	var _print = __webpack_require__(87);
	
	var m22 = _interopRequireWildcard(_print);
	
	var _export = __webpack_require__(88);
	
	var m23 = _interopRequireWildcard(_export);
	
	var _load = __webpack_require__(89);
	
	var m24 = _interopRequireWildcard(_load);
	
	function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj["default"] = obj; return newObj; } }
	
	function operations(view) {
		//order in some cases does matter
		//resize must be applied after math, to size data by the result of math calculation
		var all = [m1, m2, m5, m6, m7, m9, m10, m11, m12, m13, m14, m16, m16a, m3, m17, m18, m19, m20, m21, m22, m23, m24];
		for (var i = 0; i < all.length; i++) {
			if (all[i].init) all[i].init(view);
		}
	}

/***/ }),
/* 66 */
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(process) {"use strict";
	
	exports.__esModule = true;
	exports.init = init;
	exports.process = process;
	
	var _updater = __webpack_require__(68);
	
	var _undo = __webpack_require__(39);
	
	function init(view) {
		view.attachEvent("onCommand", function (action, start, end) {
			if (action.id == "add" || action.id == "del") {
				var area = view._table.getSelectArea();
				if (area && !(start && end)) {
					start = area.start;
					end = area.end;
				}
	
				if (start && end) {
					_undo.group.set(function () {
						process(action, start, end, view);
					});
				}
			}
		});
	
		view.attachEvent("onUndo", function (action, row, column, value) {
			if (action == "grid-change") loadChangedData(view, value);
		});
	}
	
	function process(action, start, end, view, e, r) {
		var data,
		    i,
		    newData,
		    oldData,
		    values,
		    count = 0,
		    origin = 0;
	
		var select = view._table.getSelectArea();
		var state = view._table.getScrollState();
	
		newData = view.serialize({
			math: true
		});
	
		oldData = webix.copy(newData);
		values = getUpdateValues(action, start, end);
	
		view.$handleSelection = null;
	
		if (!e) (0, _undo.ignoreUndo)(function () {
			view.callEvent("onAction", ["before-grid-change", {
				name: values.name,
				inc: values.inc,
				data: newData,
				start: start
			}]);
		}, view);
	
		data = newData.data;
	
		if (action.group == "column") {
			count = end.column - start.column + 1;
			origin = start.column;
	
			if (action.id == "add") _addColumn(action, start, end, view, e, r, data);else if (action.id == "del") {
				_delColumn(action, start, end, view, e, r, data);
				count = -count;
			}
		} else if (action.group == "row") {
			count = end.row - start.row + 1;
			origin = start.row;
	
			if (action.id == "add") _addRow(action, start, end, view, e, r, data);else if (action.id == "del") {
				_delRow(action, start, end, view, e, r, data);
				count = -count;
			}
		}
	
		//update formulas
		var update = { id: action.group, start: origin, count: count };
		i = data.length;
		while (i--) {
			if (data[i][2] && typeof data[i][2] === "string" && data[i][2].substr(0, 1) == "=") data[i][2] = (0, _updater.updateMath)(data[i][2], update);
		}
	
		view.callEvent("onAction", ["grid-change", { value: oldData, newValue: newData }]);
	
		(0, _undo.ignoreUndo)(function () {
			loadChangedData(view, newData);
			view._table.scrollTo(state.x, state.y);
		}, view);
	
		if (select) {
			var grid = view.$$("cells");
			select = fixSelectArea(select, grid);
			if (select) grid.addSelectArea(select.start, select.end);
		}
	}
	
	function fixSelectArea(select, grid) {
		var rows = grid.count();
		var cols = grid.config.columns.length;
		if (select.start.row * 1 > rows) return null;
		if (select.end.row * 1 > rows) return null;
		if (select.start.column * 1 > cols) return null;
		if (select.end.column * 1 > cols) return null;
	
		return select;
	}
	
	function loadChangedData(view, data) {
		// data loading resets undo history, and
		// we need to preserve it
		(0, _undo.ignoreReset)(function () {
			view.$handleSelection = null;
			view.parse(data);
		});
	}
	
	function getUpdateValues(action, start, end) {
		var name = action.group,
		    inc = action.id == "add" ? 1 : action.id == "del" ? -1 : 0;
	
		if (inc) {
			// span support
			if (name == "row") inc += inc * (end.row - start.row);
			if (name == "column") inc += inc * (end.column - start.column);
		}
		return { name: name, inc: inc };
	}
	
	function _addColumn(action, start, end, view, silent, value, data) {
		var i = data.length;
		var add_column_count = end.column - start.column + 1;
		view.config.columnCount += add_column_count;
		view.reset();
	
		while (i--) {
			if (data[i][1] >= start.column) data[i][1] += add_column_count;
		}if (!silent) view.callEvent("onColumnOperation", [action, start, end, null]); //add item to history
		else data.push.apply(data, value); //recovering value which has been deleted
	}
	
	function _delColumn(action, start, end, view, silent, value, data) {
		var i = data.length;
		var del_column_count = end.column - start.column + 1;
	
		if (view.config.columnCount === del_column_count) {
			if (start.column == end.column) return; //deleting last column, ignoring
			//prevent deleting all columns
			end.column--;
			del_column_count--;
		}
	
		view.config.columnCount -= del_column_count;
	
		view.reset();
		var deleted = []; // for history
	
		while (i--) {
			if (data[i][1] >= start.column && data[i][1] <= end.column) deleted.push(data.splice(i, 1)[0]);else if (data[i][1] > end.column) data[i][1] -= del_column_count;
		}if (!silent) view.callEvent("onColumnOperation", [action, start, end, deleted]);
	}
	
	function _addRow(action, start, end, view, silent, value, data) {
		var i = data.length;
		var add_row_count = end.row - start.row + 1;
		view.config.rowCount += add_row_count;
	
		view.reset();
	
		while (i--) {
			if (data[i][0] >= start.row) data[i][0] += add_row_count;
		}if (!silent) {
			view.callEvent("onRowOperation", [action, start, end, null]);
		} else {
			data.push.apply(data, value);
		}
	}
	
	function _delRow(action, start, end, view, silent, value, data) {
		var i = data.length;
		var del_row_count = end.row - start.row + 1;
	
		if (view.config.rowCount === del_row_count) {
			if (start.row == end.row) return; //deleting last column, ignoring
			//prevent deleting all columns
			end.row--;
			del_row_count--;
		}
	
		view.config.rowCount -= del_row_count;
	
		view.reset();
	
		var deleted = [];
		while (i--) {
			if (data[i][0] >= start.row && data[i][0] <= end.row) deleted.push(data.splice(i, 1)[0]);else if (data[i][0] > end.row) data[i][0] -= del_row_count;
		}if (!silent) view.callEvent("onRowOperation", [action, start, end, deleted]);
	}
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(67)))

/***/ }),
/* 67 */
/***/ (function(module, exports) {

	// shim for using process in browser
	var process = module.exports = {};
	
	// cached from whatever global is present so that test runners that stub it
	// don't break things.  But we need to wrap it in a try catch in case it is
	// wrapped in strict mode code which doesn't define any globals.  It's inside a
	// function because try/catches deoptimize in certain engines.
	
	var cachedSetTimeout;
	var cachedClearTimeout;
	
	function defaultSetTimout() {
	    throw new Error('setTimeout has not been defined');
	}
	function defaultClearTimeout () {
	    throw new Error('clearTimeout has not been defined');
	}
	(function () {
	    try {
	        if (typeof setTimeout === 'function') {
	            cachedSetTimeout = setTimeout;
	        } else {
	            cachedSetTimeout = defaultSetTimout;
	        }
	    } catch (e) {
	        cachedSetTimeout = defaultSetTimout;
	    }
	    try {
	        if (typeof clearTimeout === 'function') {
	            cachedClearTimeout = clearTimeout;
	        } else {
	            cachedClearTimeout = defaultClearTimeout;
	        }
	    } catch (e) {
	        cachedClearTimeout = defaultClearTimeout;
	    }
	} ())
	function runTimeout(fun) {
	    if (cachedSetTimeout === setTimeout) {
	        //normal enviroments in sane situations
	        return setTimeout(fun, 0);
	    }
	    // if setTimeout wasn't available but was latter defined
	    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
	        cachedSetTimeout = setTimeout;
	        return setTimeout(fun, 0);
	    }
	    try {
	        // when when somebody has screwed with setTimeout but no I.E. maddness
	        return cachedSetTimeout(fun, 0);
	    } catch(e){
	        try {
	            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
	            return cachedSetTimeout.call(null, fun, 0);
	        } catch(e){
	            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
	            return cachedSetTimeout.call(this, fun, 0);
	        }
	    }
	
	
	}
	function runClearTimeout(marker) {
	    if (cachedClearTimeout === clearTimeout) {
	        //normal enviroments in sane situations
	        return clearTimeout(marker);
	    }
	    // if clearTimeout wasn't available but was latter defined
	    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
	        cachedClearTimeout = clearTimeout;
	        return clearTimeout(marker);
	    }
	    try {
	        // when when somebody has screwed with setTimeout but no I.E. maddness
	        return cachedClearTimeout(marker);
	    } catch (e){
	        try {
	            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
	            return cachedClearTimeout.call(null, marker);
	        } catch (e){
	            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
	            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
	            return cachedClearTimeout.call(this, marker);
	        }
	    }
	
	
	
	}
	var queue = [];
	var draining = false;
	var currentQueue;
	var queueIndex = -1;
	
	function cleanUpNextTick() {
	    if (!draining || !currentQueue) {
	        return;
	    }
	    draining = false;
	    if (currentQueue.length) {
	        queue = currentQueue.concat(queue);
	    } else {
	        queueIndex = -1;
	    }
	    if (queue.length) {
	        drainQueue();
	    }
	}
	
	function drainQueue() {
	    if (draining) {
	        return;
	    }
	    var timeout = runTimeout(cleanUpNextTick);
	    draining = true;
	
	    var len = queue.length;
	    while(len) {
	        currentQueue = queue;
	        queue = [];
	        while (++queueIndex < len) {
	            if (currentQueue) {
	                currentQueue[queueIndex].run();
	            }
	        }
	        queueIndex = -1;
	        len = queue.length;
	    }
	    currentQueue = null;
	    draining = false;
	    runClearTimeout(timeout);
	}
	
	process.nextTick = function (fun) {
	    var args = new Array(arguments.length - 1);
	    if (arguments.length > 1) {
	        for (var i = 1; i < arguments.length; i++) {
	            args[i - 1] = arguments[i];
	        }
	    }
	    queue.push(new Item(fun, args));
	    if (queue.length === 1 && !draining) {
	        runTimeout(drainQueue);
	    }
	};
	
	// v8 likes predictible objects
	function Item(fun, array) {
	    this.fun = fun;
	    this.array = array;
	}
	Item.prototype.run = function () {
	    this.fun.apply(null, this.array);
	};
	process.title = 'browser';
	process.browser = true;
	process.env = {};
	process.argv = [];
	process.version = ''; // empty string to avoid regexp issues
	process.versions = {};
	
	function noop() {}
	
	process.on = noop;
	process.addListener = noop;
	process.once = noop;
	process.off = noop;
	process.removeListener = noop;
	process.removeAllListeners = noop;
	process.emit = noop;
	process.prependListener = noop;
	process.prependOnceListener = noop;
	
	process.listeners = function (name) { return [] }
	
	process.binding = function (name) {
	    throw new Error('process.binding is not supported');
	};
	
	process.cwd = function () { return '/' };
	process.chdir = function (dir) {
	    throw new Error('process.chdir is not supported');
	};
	process.umask = function() { return 0; };


/***/ }),
/* 68 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports.updateMath = updateMath;
	
	var _column_names = __webpack_require__(41);
	
	var nms = _interopRequireWildcard(_column_names);
	
	var _parser = __webpack_require__(69);
	
	function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj["default"] = obj; return newObj; } }
	
	function updateMath(formula, action) {
		var stack = (0, _parser.split)(formula);
		var max = stack.length;
		for (var i = 1; i < max; i += 2) {
			var _stack$i = stack[i],
			    row = _stack$i[0],
			    column = _stack$i[1];
	
	
			if (action.id === "move") {
				column += action.column;
				row += action.row;
			} else if (action.id === "row" && action.start <= row) {
				row += action.count;
			} else if (action.id === "column" && action.start <= column) {
				column += action.count;
			}
	
			if (!column || !row) stack[i] = 0;else stack[i] = nms.encode[column] + row;
		}
	
		return stack.join("");
	}

/***/ }),
/* 69 */
/***/ (function(module, exports) {

	"use strict";
	
	exports.__esModule = true;
	exports.position = position;
	exports.split = split;
	exports.parse = parse;
	function isChar(char) {
		var code = char.charCodeAt(0);
		return code >= 65 && code <= 122 || code === 36;
	}
	function isNumber(char) {
		var code = char.charCodeAt(0);
		return code >= 48 && code <= 57;
	}
	
	function getWord(formula, i, passive) {
		var max = formula.length;
		var sheet = passive;
		var quotes = false;
	
		for (var j = i; j < max; j++) {
			var key = formula[j];
			if (key === "'") {
				quotes = !quotes;
				continue;
			}
	
			if (!quotes) {
				if (key === "!") {
					//mulit-sheet math
					sheet = formula.substr(i, j - i);
					if (sheet[0] === "'") sheet = sheet.substr(1, sheet.length - 2);
					i = j + 1;
				} else if (!isChar(key) && !isNumber(key)) {
					return [formula.substr(i, j - i), j, i, sheet];
				}
			}
		}
	
		return [formula.substr(i), j, i, sheet];
	}
	
	var operand = /^[A-Z\$]+[0-9]+$/;
	function isPosition(text) {
		return operand.test(text);
	}
	
	function position(word) {
		var row = 0,
		    sum = 0,
		    x = 1;
		var chars = false;
	
		for (var j = word.length - 1; j >= 0; j--) {
			var key = word[j].charCodeAt(0);
			if (key === 36) continue;
	
			if (key < 58) {
				sum += (key - 48) * x;
				x *= 10;
			} else {
				if (!chars) {
					x = 1;row = sum;sum = 0;chars = true;
				}
				sum += (key - 64) * x;
				x *= 26;
			}
		}
	
		return [row, sum];
	}
	function operandCode(deps, word, sheet) {
		var _position = position(word),
		    r = _position[0],
		    c = _position[1];
	
		if (sheet !== "") {
			deps.push([r, c, r, c, sheet]);
			return "this.vs(\"" + sheet + "\"," + r + "," + c + ")";
		} else {
			deps.push([r, c, r, c, ""]);
			return "this.v(" + r + "," + c + ")";
		}
	}
	
	function methodCode(word) {
		return "this.m." + word;
	}
	
	function namedRangeCode(deps, view, word, sheet) {
		var code = view.ranges.getCode(word, sheet);
		if (!code) return "";
	
		var sheetInd = code.indexOf("!");
		if (sheetInd !== -1) {
			sheet = code.substr(0, sheetInd);
			if (sheet[0] === "'") sheet = sheet.substr(1, sheet.length - 2);
			code = code.substr(sheetInd + 1);
		}
	
		var _code$split = code.split(":"),
		    a = _code$split[0],
		    b = _code$split[1];
	
		return rangeCode(deps, a, b, sheet);
	}
	
	function templateCode(word) {
		return "this.p." + word;
	}
	
	function rangeCode(deps, a, b, sheet) {
		var _position2 = position(a),
		    r1 = _position2[0],
		    c1 = _position2[1];
	
		var _position3 = position(b),
		    r2 = _position3[0],
		    c2 = _position3[1];
	
		if (r1 > r2) {
			var t = r1;r1 = r2;r2 = t;
		}
		if (c1 > c2) {
			var _t = c1;c1 = c2;c2 = _t;
		}
	
		if (sheet === "") {
			deps.push([r1, c1, r2, c2, ""]);
			return "this.r(" + r1 + "," + c1 + "," + r2 + "," + c2 + ")";
		} else {
			deps.push([r1, c1, r2, c2, sheet]);
			return "this.rs(\"" + sheet + "\"," + r1 + "," + c1 + "," + r2 + "," + c2 + ")";
		}
	}
	
	function split(formula) {
		var lines = [];
		var index = 0;
		var quotes = false,
		    ph = false;
	
		for (var i = 1; i < formula.length; i++) {
			var key = formula[i];
			if (key == "\"") {
				quotes = !quotes;
			} else if (!quotes) {
				if (key == "{" && formula[i + 1] == "{") {
					ph = true;
				} else if (key == "}" && formula[i + 1] == "}") {
					ph = false;
				} else if (!ph) {
					if (key === "'" || isChar(key)) {
						var _getWord = getWord(formula, i, ""),
						    word = _getWord[0],
						    end = _getWord[1],
						    sheet = _getWord[3];
	
						var next = end - 1;
						if (sheet === "") {
							if (formula[next + 1] !== "(" && isPosition(word)) {
								if (i !== 0) lines.push(formula.substr(index, i - index));
								lines.push(position(word));
								index = next + 1;
							}
						} else {
							if (formula[next + 1] === ":") {
								//for multi-sheet reference, ignore second parameter of range
								var _getWord2 = getWord(formula, next + 2, ""),
								    _end = _getWord2[1];
	
								next = _end - 1;
							}
						}
						i = next;
					}
				}
			}
		}
	
		if (index != formula.length) lines.push(formula.substr(index));
		return lines;
	}
	
	function parse(formula, view) {
		var passive = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : "";
	
		var code = "return ";
		var deps = [];
	
		var quotes = false;
		var pair = "",
		    pairsheet = "";
	
		if (formula[0] !== "=") return false;
	
		for (var i = 1; i < formula.length; i++) {
			var key = formula[i];
	
			if (key == "\"") quotes = !quotes;else if (key == "{" && formula[i + 1] == "{") {
				var _getWord3 = getWord(formula, i + 2),
				    word = _getWord3[0],
				    end = _getWord3[1];
	
				i = end + 1;
				code += templateCode(word);
				continue;
			} else if (!quotes && (key === "'" || isChar(key))) {
				var _getWord4 = getWord(formula, i, passive),
				    rawword = _getWord4[0],
				    _end2 = _getWord4[1],
				    start = _getWord4[2],
				    sheet = _getWord4[3];
	
				//lower case formulas, fix them
	
	
				var _word = rawword.toUpperCase();
	
				i = _end2 - 1;
	
				if (formula[i + 1] === "(") {
					code += methodCode(_word);
				} else if (isPosition(_word)) {
					if (formula[i + 1] === ":") {
						pair = _word;pairsheet = sheet;
						i++;
					} else {
						if (pair !== "") {
							code += rangeCode(deps, pair, _word, pairsheet);
							pair = "";
						} else code += operandCode(deps, _word, sheet);
					}
				} else {
					var range = namedRangeCode(deps, view, _word, sheet);
					//we have some error word in a formula
					//break from loop to skip formula updating
					if (range == "") continue;
					code += range;
				}
	
				//operator, or range, or parameter
				if (_word !== rawword) {
					formula = formula.substr(0, start) + _word + formula.substr(_end2);
				}
	
				continue;
			}
	
			if (!quotes) {
				//special handling for & operator, string concat
				if (key === "&" && formula[i + 1] !== "&") code += "+";
				//convert <> to !=
				else if (key === "<" && formula[i + 1] === ">") {
						code += "!=";
						i++;
						//convert = to ==
					} else if (key === "=" && formula[i - 1] !== "<" && formula[i - 1] !== ">") code += "==";else code += key;
			} else code += key;
		}
	
		return { code: code + ";", triggers: deps, text: formula };
	}

/***/ }),
/* 70 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	
	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };
	
	exports.init = init;
	
	var _undo = __webpack_require__(39);
	
	var _updater = __webpack_require__(68);
	
	var styledata, clipdata, oldclip, origin, cutted;
	
	function init(view, readonly) {
		var keyC = 67; //keyboard 'c' code
		var keyX = 88; //keyboard 'x' code
		var grid = view.$$("cells");
	
		grid.attachEvent("onKeyPress", function (code, e) {
			if ((code === keyC || code === keyX) && (e.ctrlKey || e.metaKey) && grid.getSelectedId()) {
				styledata = _get_sel_style(view, grid);
				clipdata = document.getElementsByClassName("webix_clipbuffer")[0].value;
				origin = grid.getSelectArea();
				cutted = code === keyX;
			}
		});
	
		if (!readonly) {
			grid.attachEvent("onPaste", function (text) {
				_clip_to_sel(view, grid, text);
			});
		}
	}
	
	function _clip_to_sel(view, grid, text) {
		var leftTop = grid.getSelectArea();
		if (!leftTop) return;
		var start = leftTop.start;
	
		//special handling for ctrl-c on empty cell clipdata
		var fromSheet = text === clipdata || clipdata === "";
		//detect that clipboard has changed while we have empty clipdata
		//probably something was selected outside of the spreadsheet
		if (clipdata === "" && oldclip && oldclip !== text) {
			clipdata = webix.undefined;
			fromSheet = false;
		}
		oldclip = text;
	
		var data = fromSheet ? styledata : webix.csv.parse(text, grid.config.delimiter);
		var translate = { id: "move", column: 0, row: 0 };
	
		if (fromSheet) {
			translate.column = start.column - origin.start.column;
			translate.row = start.row - origin.start.row;
		} else {
			cutted = false;
		}
	
		_undo.group.start();
		if (data.length == 1 && data[0].length == 1) {
			view.eachSelectedCell(function (cell) {
				var subtrans = {
					id: "move",
					column: translate.column + cell.column * 1 - start.column,
					row: translate.row + cell.row * 1 - start.row
				};
	
				_clipboardToTable(view, cell.row, cell.column, data[0][0], fromSheet, subtrans);
			});
		} else {
			grid.mapCells(start.row, start.column, data.length, null, function (value, row, col, row_ind, col_ind) {
				if (data[row_ind] && data[row_ind].length > col_ind) {
					var cdata = data[row_ind][col_ind];
					_clipboardToTable(view, row, col, cdata, fromSheet, translate);
				}
			}, true);
		}
	
		if (cutted) {
			for (var row = origin.start.row; row <= origin.end.row; row++) {
				for (var column = origin.start.column; column <= origin.end.column; column++) {
					view.setCellValue(row, column, null);
					view.setStyle(row, column, null);
				}
			}
			cutted = false;
		}
		_undo.group.end();
	
		view.refresh();
	}
	
	function _clipboardToTable(view, row, col, cdata, fromSheet, translate) {
		var newValue = cdata;
		var style = null;
	
		if ((typeof cdata === "undefined" ? "undefined" : _typeof(cdata)) === "object") {
			if (cdata.math) newValue = (0, _updater.updateMath)(cdata.math, translate);else newValue = cdata.text;
	
			style = cdata.style;
		}
	
		view.setCellValue(row, col, newValue);
		if (fromSheet) view.setStyle(row, col, style);
	}
	
	function _get_sel_style(view, grid) {
		var data = [];
		var row, last;
	
		grid.mapSelection(function (value, id, col) {
			if (id != last) {
				row = [];data.push(row);
				last = id;
			}
	
			var item = grid.getItem(id);
			var math = item["$" + col];
			row.push({ text: value, math: math, style: view.getStyle(id, col) });
			return value;
		});
	
		return data;
	}

/***/ }),
/* 71 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports.init = init;
	
	var _undo = __webpack_require__(39);
	
	function init(view) {
		view.conditions = {
			_empty: true,
			_pull: {},
			handlers: {
				">": function _(a, b) {
					return a > b;
				},
				"<": function _(a, b) {
					return a < b;
				},
				"=": function _(a, b) {
					return a == b;
				},
				"<>": function _(a, b) {
					return webix.isArray(b) && a < b[1] && a > b[0];
				}
			},
			add: add,
			remove: remove,
			update: update,
			get: get,
			parse: parse,
			serialize: serialize,
			clear: clear
		};
	
		view.attachEvent("onConditionSet", function (data) {
			return _setCondition(view, data);
		});
	
		view.attachEvent("onUndo", function (type_action, row, column, value) {
			if (type_action == "condition") _undoCondition(view, row, column, value);
		});
	
		view.attachEvent("onDataSerialize", function (data) {
			return _serialize(view, data);
		});
		view.attachEvent("onDataParse", function (data) {
			return _parse(view, data);
		});
	
		view.attachEvent("onColumnInit", function (column) {
			return _setCssFormat(view, column);
		});
	
		view.attachEvent("onReset", function () {
			return view.conditions.clear();
		});
	
		view.attachEvent("onClearStyle", function (row, column) {
			var condition = view.conditions.get(row, column);
			if (condition) {
				if (view.callEvent("onBeforeConditionSet", [row, column, condition, null])) {
					view.conditions.remove(row, column);
					view.refresh();
				}
			}
		});
	
		view.attachEvent("onAction", function (action, p) {
			if (action == "before-grid-change") updatePosition(p.name, p.inc, p.data, p.start);
		});
	
		reset(view);
	}
	
	function reset(view) {
		view.conditions.clear();
	}
	
	function _setCondition(view, data) {
		_undo.group.set(function () {
			view.eachSelectedCell(function (cell) {
				var collection = view.conditions.get(cell.row, cell.column);
				if (view.callEvent("onBeforeConditionSet", [cell.row, cell.column, collection || null, data, _undo.group.value])) view.conditions.update(cell.row, cell.column, data);
			});
		});
		view.refresh();
	}
	
	function _undoCondition(view, row, column, value) {
		if (view.conditions.get(row, column)) view.conditions.remove(row, column);
		if (value) view.conditions.update(row, column, value);
	}
	
	function _serialize(view, obj) {
		obj.conditions = view.conditions.serialize();
	}
	
	function _parse(view, obj) {
		view.conditions.parse(obj.conditions);
	}
	
	function _setCssFormat(view, column) {
		column.cssFormat = function (value, obj, row, column) {
			if (view.conditions._empty) return "";
	
			var collection, i;
			collection = view.conditions.get(row, column);
			if (!collection) return "";
	
			i = collection.length;
			while (i--) {
				var handler = view.conditions.handlers[collection[i][0]];
				if (handler && handler(obj[column], collection[i][1])) return collection[i][2];
			}
			return "";
		};
	}
	
	function addEmptyCollection(pull, row, column, state) {
		state._empty = false;
		if (!pull[row]) pull[row] = {};
		if (!pull[row][column]) pull[row][column] = [];
		return pull[row][column];
	}
	
	function parse(data) {
		this._empty = true;
		if (!data) return;
	
		var i = data.length;
		while (i--) {
			var c = data[i];
			var collection = addEmptyCollection(this._pull, c[0], c[1], this);
			collection.push([c[2], c[3], c[4]]);
		}
	}
	
	function clear() {
		this._pull = {};
	}
	
	function update(row, column, newData) {
		var collection = this.get(row, column);
		if (!collection) addEmptyCollection(this._pull, row, column, this);
		this._pull[row][column] = newData;
	}
	
	function get(rowId, columnId) {
		if (!rowId) return this._pull;
		if (!columnId) return this._pull[rowId];
		return this._pull[rowId] ? this._pull[rowId][columnId] : null;
	}
	
	function add(rowId, columnId, condition, value, style) {
		var collection = addEmptyCollection(this._pull, rowId, columnId, this);
		collection.push([condition, value, style]);
	}
	
	function remove(rowId, columnId) {
		var collection = this.get(rowId, columnId);
		if (collection) delete this._pull[rowId][columnId];
	}
	
	function serialize() {
		var column,
		    condition,
		    i,
		    row,
		    data = [];
	
		for (row in this._pull) {
			for (column in this._pull[row]) {
				for (i = 0; i < this._pull[row][column].length; i++) {
					condition = this._pull[row][column][i];
					data.push([row, column, condition[0], condition[1], condition[2]]);
				}
			}
		}return data;
	}
	
	function updatePosition(name, inc, data, start) {
		var conditions = data.conditions,
		    i = conditions.length;
	
		if (inc) {
			while (i--) {
				var _conditions$i = conditions[i],
				    row = _conditions$i[0],
				    column = _conditions$i[1];
	
				if (name == "row" && row >= start.row || name == "column" && column >= start.column) {
					if (name == "row") {
						if (row < start.row - inc) //delete lock mark if row was deleted
							conditions.splice(i, 1);else //update mark position if upper row was deleted
							conditions[i][0] = row * 1 + inc;
					} else if (name == "column") {
						if (column < start.column - inc) {
							conditions.splice(i, 1);
						} else conditions[i][1] = column * 1 + inc;
					}
				}
			}
		}
	}

/***/ }),
/* 72 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports.init = init;
	
	var _methods = __webpack_require__(44);
	
	var _ranges = __webpack_require__(55);
	
	var rgs = _interopRequireWildcard(_ranges);
	
	var _placeholder = __webpack_require__(73);
	
	var psh = _interopRequireWildcard(_placeholder);
	
	var _parser = __webpack_require__(69);
	
	var parser = _interopRequireWildcard(_parser);
	
	var _column_names = __webpack_require__(41);
	
	var _sheet = __webpack_require__(56);
	
	var sheets = _interopRequireWildcard(_sheet);
	
	function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj["default"] = obj; return newObj; } }
	
	function init(view) {
		var triggers = [];
		var backtrak = [];
	
		//math operations object
		var core = get_core(view);
	
		//init named ranges
		view.ranges = core.ranges = new rgs.Ranges(view);
		//init placeholders
		core.p = psh.init(view);
	
		view.attachEvent("onReset", function () {
			triggers = [];
			backtrak = [];
			view.ranges.clear();
		});
	
		view.attachEvent("onCellChange", recalckCell);
		view.attachEvent("onMathRefresh", recalckAll);
	
		view.attachEvent("onDataSerialize", function (data) {
			return _serialize(view, data);
		});
		view.attachEvent("onDataParse", function (data) {
			return _dataParse(view, data);
		});
	
		view.attachEvent("onAction", function (action, p) {
			if (action == "before-grid-change") updatePosition(p.name, p.inc, p.data, p.start);
		});
	
		//execute math handler
		function applyMath(r, c, handler) {
			var row = view.getRow(r);
			row[c] = _execute(handler, core);
			//check if we have related cells, process their math as well
			check_trigger(r, c);
		}
	
		var check_trigger_count;
		//check and run triggers
		function check_trigger(row, column) {
			//triggers is a matrix[row][column]
			//store array of coordinates of related cells
	
			var line = triggers[row];
			if (line) {
				var block = line[column];
				if (block) {
					for (var i = 0; i < block.length; i++) {
						var cell = block[i];
						//prevent infinity loops
						check_trigger_count++;
						if (check_trigger_count > 100) return;
	
						applyMath(cell.row, cell.column, cell.handler);
	
						check_trigger_count--;
					}
				}
			}
		}
	
		function recalckAll() {
			var grid = view.$$("cells");
			var state = grid.getState();
			var columns = state.ids.concat(state.hidden);
	
			grid.eachRow(function (obj) {
				var item = this.getItem(obj);
				for (var i = 1; i < columns.length; i++) {
					var key = columns[i];
					var value = item["$" + key];
					if (value) recalckCell(obj, key, value);
				}
			}, true);
	
			grid.refresh();
		}
	
		function recalckCell(r, c, value) {
			//check if changed cell was a math cell, based on some other cells
			//clean triggers in such case
			var line = backtrak[r];
			if (line && line[c]) remove_triggers(triggers, backtrak, c, r);
	
			//if new value is a math, calculate it and store triggers
			if (value && value.toString().indexOf("=") === 0) {
				var formula = _parse(value, core, "");
				var row = view.getRow(r);
				row[c] = _execute(formula.handler, core);
				row["$" + c] = formula.text;
	
				if (formula.triggers.length) {
					add_triggers(triggers, backtrak, formula.triggers, { row: r, column: c, handler: formula.handler }, view.getActiveSheet());
				}
			}
			//check if we have some other cells, related to the changed one
			check_trigger_count = 0;
			check_trigger(r, c);
		}
	
		sheets.init(view, core, { parse: _parse, execute: _execute });
	}
	
	//add new triggers
	function add_triggers(trs, back, adds, cell, active) {
		//trs - matrix of triggers
		//back - matrix of backlinks
		//adds - list of triggers
		var blist = [];
		for (var i = 0; i < adds.length; i++) {
			var line = adds[i];
			//line = [start_row, start_column, end_row, end_column, sheet]
	
			//ignore triggers from passive cells
			if (line[4] !== "" && line[4] !== active) continue;
	
			for (var j = line[0]; j <= line[2]; j++) {
				var step = trs[j];
				if (!step) step = trs[j] = [];
				for (var k = line[1]; k <= line[3]; k++) {
					var block = step[k];
					if (!block) block = step[k] = [];
	
					blist.push([j, k]);
					block.push(cell);
				}
			}
		}
	
		//store back-relations, for easy trigger removing 
		add_backtrack(back, cell.row, cell.column, blist);
	}
	
	//store backtrack relations as a matrix
	function add_backtrack(back, row, column, adds) {
		var line = back[row];
		if (!line) line = back[row] = [];
		line[column] = adds;
	}
	
	//remove unused triggers
	function remove_triggers(trs, back, c, r) {
		//get list of triggers from backtrack structure
		var adds = back[r][c];
		back[r][c] = null;
	
		//delete triggers
		for (var i = adds.length - 1; i >= 0; i--) {
			var cell = adds[i];
			var block = trs[cell[0]][cell[1]];
	
			for (var j = block.length - 1; j >= 0; j--) {
				var bcell = block[j];
				if (bcell.row == r && bcell.column == c) block.splice(j, 1);
			}
		}
	}
	
	//convert math string to the js function
	function _parse(value, core, passive) {
		var struct = parser.parse(value, core, passive);
		struct.handler = _build_function(struct.code);
		return struct;
	}
	
	function error_function() {
		return "ERROR";
	}
	function _build_function(value) {
		try {
			return new Function(value);
		} catch (e) {
			return error_function;
		}
	}
	
	//run math function
	function _execute(formula, core) {
		var value;
		try {
			value = formula.call(core);
		} catch (e) {
			//some error in the math code
			return "ERROR";
		}
	
		//round values to fix math precision issue in JS
		if (typeof value === "number") {
			return Math.round(value * 100000) / 100000;
		} else if (typeof value !== "string") {
			return value ? value.toString() : "ERROR";
		} else return value;
	}
	
	//converts cell references to code
	function get_core(view) {
		var active = sheets.getAccessor(view);
	
		return {
			rs: function rs(sheet, r1, c1, r2, c2) {
				return sheets.getAccessor(view, sheet).getRange(r1, c1, r2, c2);
			},
			r: active.getRange,
			vs: function vs(sheet, r, c) {
				return sheets.getAccessor(view, sheet).getValue(r, c);
			},
			v: active.getValue,
			m: _methods.methods,
			p: {}
		};
	}
	
	function _serialize(view, obj) {
		obj.ranges = view.ranges.serialize();
	}
	
	function _dataParse(view, obj) {
		view.ranges.parse(obj.ranges);
	}
	
	function updatePosition(name, inc, data, start) {
		var ranges = data.ranges;
		for (var i = 0; i < ranges.length; i++) {
			ranges[i][1] = (0, _column_names.changeRange)(ranges[i][1], name, inc, start);
		}
	}

/***/ }),
/* 73 */
/***/ (function(module, exports) {

	"use strict";
	
	exports.__esModule = true;
	
	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };
	
	exports.init = init;
	function init(view) {
		var holders = {};
	
		view.setPlaceholder = function (obj, value) {
			if ((typeof obj === "undefined" ? "undefined" : _typeof(obj)) === "object") for (var key in obj) {
				holders[key.toLowerCase()] = obj[key];
			} else holders[obj.toString().toLowerCase()] = value;
	
			view.callEvent("onMathRefresh", []);
		};
	
		return holders;
	}

/***/ }),
/* 74 */
/***/ (function(module, exports) {

	"use strict";
	
	exports.__esModule = true;
	exports.init = init;
	function init(view) {
		//parsing initial data
		view.attachEvent("onDataParse", function (data) {
			return _parse(view, data);
		});
		view.attachEvent("onDataSerialize", function (data, config) {
			return _serialize(view, data, config);
		});
	
		// undo
		view.attachEvent("onUndo", function (type, row, column, value) {
			if (type == "value") _undoValue(view, row, column, value);
		});
	}
	
	function _serialize(view, obj, config) {
		var math = !config || config.math !== false;
		var data = [];
		var grid = view.$$("cells");
		var state = grid.getState();
		var columns = state.ids.concat(state.hidden);
	
		// remove "rowId" column
		var rowHeader = columns.indexOf("rowId");
		if (rowHeader > -1) columns.splice(rowHeader, 1);
	
		grid.eachRow(function (obj) {
			var item = this.getItem(obj);
			for (var i = 0; i < columns.length; i++) {
				var key = columns[i];
				var value = item[key];
				var css = item.$cellCss ? item.$cellCss[key] || "" : "";
	
				//serialize instead of value if defined
				if (math) value = item["$" + key] || value;
	
				//put not empty values in serialization
				var hasValue = value || value === 0;
				if (hasValue || css) {
					data.push([obj * 1, key * 1, hasValue ? value : "", css]);
				}
			}
		}, true);
	
		obj.table = {
			frozenColumns: grid.config.leftSplit - 1,
			frozenRows: grid.config.topSplit
		};
	
		obj.data = data;
	}
	
	function _parse(view, obj) {
		var grid = view.$$("cells");
	
		var tconfig = obj.table || { frozenColumns: 0, frozenRows: 0 };
		if (tconfig) {
			if (!webix.isUndefined(tconfig.frozenColumns) && tconfig.frozenColumns + 1 != grid.config.leftSplit) view.freezeColumns(tconfig.frozenColumns);
			if (!webix.isUndefined(tconfig.frozenRows) && tconfig.frozenRows != grid.config.topSplit) view.freezeRows(tconfig.frozenRows);
		}
	
		if (obj.sizes) grid.define("fixedRowHeight", false);
	
		for (var i = 0; i < obj.data.length; i++) {
			var _obj$data$i = obj.data[i],
			    row = _obj$data$i[0],
			    column = _obj$data$i[1],
			    value = _obj$data$i[2],
			    style = _obj$data$i[3];
	
	
			var item = grid.getItem(row);
			item[column] = value;
			if (style) {
				item.$cellCss = item.$cellCss || {};
				item.$cellCss[column] = style;
			}
			view.callEvent("onCellChange", [row, column, value]);
		}
	}
	
	function _undoValue(view, row, column, value) {
		view.setCellValue(row, column, value);
	}

/***/ }),
/* 75 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports.init = init;
	exports.serialize = serialize;
	exports.load = load;
	exports.clearEditors = clearEditors;
	exports.addCellFilter = addCellFilter;
	
	var _ranges = __webpack_require__(55);
	
	var _column_names = __webpack_require__(41);
	
	function init(view) {
		var table = view._table;
		view.attachEvent("onReset", function () {
			return reset(view);
		});
	
		reset(view);
		table.attachEvent("onBeforeEditStart", function (id) {
			var col = this.getColumnConfig(id.column);
			if (this._ssEditors[id.row]) {
				var ed = this._ssEditors[id.row][id.column];
				if (ed) {
					webix.extend(col, ed, true);
					if (ed.options && typeof ed.options == "string") {
						col.options = (0, _ranges.rangeValue)(view, ed.options, { unique: true, empty: true, order: true });
					}
				}
			}
		});
		table.attachEvent("onAfterEditStop", function (state, id) {
			this.getColumnConfig(id.column).editor = "text";
	
			if (isFilter(view, id.row, id.column)) return view.filterSpreadSheet();
		});
	
		table.on_click.ss_filter = function (e, id) {
			this.edit(id);
		};
	
		view.attachEvent("onAction", function (action, p) {
			if (action == "before-grid-change") updatePosition(p.name, p.inc, p.data, p.start);
		});
	}
	
	function reset(view) {
		view._table._ssEditors = {};
		view._table._ssFilters = {};
	}
	
	function serialize(view, data) {
		var row,
		    column,
		    tblEditors = view._table._ssEditors,
		    editors = [];
	
		for (row in tblEditors) {
			for (column in tblEditors[row]) {
				if (!isFilter(view, row, column)) editors.push([row, column, tblEditors[row][column]]);
			}
		}
		data.editors = editors;
	}
	
	function load(view, data) {
		var i,
		    editors = data.editors;
	
		if (editors) for (i = 0; i < editors.length; i++) {
			view.setCellEditor.apply(view, editors[i]);
		}
	}
	
	function clearEditors(view, range) {
		var refilter = false;
	
		view._table.editStop();
		(0, _column_names.eachRange)(range, view, function (view, cell) {
			var editor = view._table._ssEditors[cell.row];
			var filter = view._table._ssFilters[cell.row];
	
			if (editor) {
				if (editor[cell.column]) {
					delete editor[cell.column];
					view._table.removeCellCss(cell.row, cell.column, "ss_filter");
				}
			}
	
			if (filter) {
				if (filter[cell.column]) {
					delete filter[cell.column];
					refilter = true;
				}
			}
		});
	
		if (refilter) view.filterSpreadSheet();
	}
	
	function isFilter(view, row, column) {
		var filters = view._table._ssFilters;
		return filters[row] && filters[row][column];
	}
	
	function addCellFilter(view, row, column) {
		var filters = view._table._ssFilters;
		var line = filters[row] = filters[row] || {};
		line[column] = [row, column];
	
		view.callEvent("onAction", ["addFilter", { row: row, column: column }]);
		view.setCellValue(row, column, "");
	}
	
	// called on column/row add, delete, hide,show
	function updatePosition(name, inc, data, start) {
		var i = void 0,
		    editors = data.editors;
	
		if (inc) {
			// create new editors
			for (i = editors.length - 1; i >= 0; i--) {
				var _editors$i = editors[i],
				    row = _editors$i[0],
				    column = _editors$i[1],
				    editor = _editors$i[2];
	
				if (name == "row" && row >= start.row || name == "column" && column >= start.column) {
					//make copy of editor object
					editor = editors[i][2] = webix.copy(editor);
	
					if (name == "row") {
						if (row < start.row - inc) //delete lock mark if row was deleted
							editors.splice(i, 1);else //update mark position if upper row was deleted
							editors[i][0] = row * 1 + inc;
					} else if (name == "column") {
						if (column < start.column - inc) {
							editors.splice(i, 1);
						} else editors[i][1] = column * 1 + inc;
					}
				}
				editor.options = (0, _column_names.changeRange)(editor.options, name, inc, start);
			}
		}
	}

/***/ }),
/* 76 */
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(process) {"use strict";
	
	exports.__esModule = true;
	exports.init = init;
	exports.reset = reset;
	exports.setColumnState = setColumnState;
	exports.isColumnVisible = isColumnVisible;
	exports.setRowState = setRowState;
	exports.isRowVisible = isRowVisible;
	exports.serialize = serialize;
	exports.load = load;
	function init(view) {
		view.attachEvent("onCommand", function (action) {
			if (action.id == "show" || action.id == "hide") {
				var sel = view._table.getSelectArea();
				if (sel) process(action, sel.start, sel.end, view);
			}
		});
		reset(view);
	
		view.attachEvent("onUndo", function (action, row, column, value, direction) {
			if ((action.id == "hide" || action.id == "show") && (action.group == "column" || action.group == "row")) {
				var type = action.id;
				if (!direction) type = type == "hide" ? "show" : "hide";
				var obj = row || column;
				process({ id: type, group: action.group }, obj.start, obj.end, view, true);
			}
		});
	
		view.attachEvent("onHardReset", function () {
			return reset(view);
		});
	
		view.attachEvent("onAction", function (action, p) {
			if (action == "before-grid-change") _updatePosition(view, p.name, p.inc, p.data, p.start);
		});
	}
	
	function reset(view, mode) {
		if (mode) view["_hidden_" + mode + "_hash"] = {};else {
			view._hidden_cols_hash = {};
			view._hidden_rows_hash = {};
		}
	}
	
	function getIds(id) {
		return webix.isArray(id) ? id : [id, id];
	}
	
	function setColumnState(view, column, state) {
		// hide
		if (!column) {
			var sel = view.getSelectedId(true),
			    action = { id: state === false ? "show" : "hide", group: "column" };
			if (sel.length) view.callEvent("onCommand", [action, sel[0], sel[sel.length - 1]]);
		} else {
			var ids = getIds(column);
			if (!view._hidden_cols_hash[ids[0]] && (webix.isUndefined(state) || state)) {
				_hideColumn({ id: "hide", group: "column" }, { column: ids[0] }, { column: ids[1] }, view);
			}
			// show
			else if (view._hidden_cols_hash[ids[0]] && (webix.isUndefined(state) || !state)) {
					var inc = ids[0] > 1 ? 1 : -1;
					_showColumn({ id: "show", group: "column" }, { column: ids[0] + inc }, { column: ids[1] + inc }, view);
				}
		}
	}
	function isColumnVisible(view, column) {
		return !view._hidden_cols_hash[column];
	}
	
	function setRowState(view, row, state) {
		// hide
		if (!row) {
			var sel = view.getSelectedId(true),
			    action = { id: state === false ? "show" : "hide", group: "row" };
			if (sel.length) view.callEvent("onCommand", [action, sel[0], sel[sel.length - 1]]);
		} else {
			var ids = getIds(row);
			if (!view._hidden_rows_hash[ids[0]] && (webix.isUndefined(state) || state)) {
				_hideRow({ id: "hide", group: "row" }, { row: ids[0] }, { row: ids[1] }, view);
			}
			// show
			else if (view._hidden_rows_hash[ids[0]] && (webix.isUndefined(state) || !state)) {
					var inc = ids[0] > 1 ? 1 : -1;
					_showRow({ id: "show", group: "row" }, { row: ids[0] + inc }, { row: ids[1] + inc }, view);
				}
		}
	}
	
	function isRowVisible(view, row) {
		return !view._hidden_rows_hash[row];
	}
	
	function serialize(view, data) {
		var hrows = [];var hcols = [];
		for (var r in view._hidden_rows_hash) {
			hrows.push(r);
		}for (var c in view._hidden_cols_hash) {
			hcols.push(c);
		}if (hrows.length || hcols.length) data.table.hidden = {};
		if (hrows.length) data.table.hidden.row = hrows;
		if (hcols.length) data.table.hidden.column = hcols;
	}
	
	function load(view, data) {
		reset(view);
	
		if (!webix.isUndefined(data.table) && !webix.isUndefined(data.table.hidden)) {
			var hidden = data.table.hidden;
	
			if (hidden.row && hidden.row.length) {
				for (var r = 0; r < hidden.row.length; r++) {
					_hideRow({ id: "hide", group: "row" }, { row: hidden.row[r] }, { row: hidden.row[r] }, view, true);
				}
			}
	
			if (hidden.column && hidden.column.length) {
				for (var c = 0; c < hidden.column.length; c++) {
					_hideColumn({ id: "hide", group: "column" }, { column: hidden.column[c] }, { column: hidden.column[c] }, view, true);
				}
			}
		}
	}
	
	function process(action) {
		if (action.group == "column") {
			if (action.id == "show") _showColumn.apply(this, arguments);else if (action.id == "hide") _hideColumn.apply(this, arguments);
		} else if (action.group == "row") {
			if (action.id == "show") _showRow.apply(this, arguments);else if (action.id == "hide") _hideRow.apply(this, arguments);
		}
	}
	
	function _showColumn(action, start, end, view, silent) {
		var i = end.column;
	
		while (i >= start.column) {
			var id = i;
			if (!silent) id = view._hidden_cols_hash[i * 1 - 1] || view._hidden_cols_hash[i * 1 + 1] || false;
	
			if (id !== false) {
				delete view._hidden_cols_hash[id];
				var cell = view.$$("cells").getColumnConfig(id - 1 || "rowId").header[0];
				cell.css = cell.css.replace("webix_ssheet_hide_column", "");
				view.$$("cells").showColumn(id);
				start.column = start.column < id ? start.column : id;
				end.column = end.column > id ? end.column : id;
	
				if (!silent) view.callEvent("onColumnOperation", [action, { column: id }, { column: id }, null]);
			}
			i--;
		}
	}
	
	function _showRow(action, start, end, view, silent) {
		var i = end.row;
	
		while (i >= start.row) {
			var id = i;
			if (!silent) id = view._hidden_rows_hash[i * 1 - 1] || view._hidden_rows_hash[i * 1 + 1] || false;
	
			if (id !== false) {
				delete view._hidden_rows_hash[id];
				if (id - 1 === 0) {
					var cell = view.$$("cells").getColumnConfig("rowId").header[0];
					cell.css = cell.css.replace("webix_ssheet_hide_row", "");
					view.$$("cells").refreshColumns();
				} else {
					view.$$("cells").removeCellCss(id - 1, "rowId", "webix_ssheet_hide_row");
				}
				if (!silent) view.callEvent("onRowOperation", [action, { row: id }, { row: id }, null]);
			}
			i--;
		}
	
		view.$$("cells").filter(function (obj) {
			return !view._hidden_rows_hash[obj.id];
		});
	}
	
	function _hideColumn(action, start, end, view, silent) {
		var i = end.column;
		view.$handleSelection = null;
		view.$$("cells").unselect();
	
		if (end.column < 1 || start.column < 1) {
			delete view._hidden_cols_hash[start.column];
			return;
		}
		while (i >= start.column) {
			view._hidden_cols_hash[i] = i;
	
			var cell = view.$$("cells").getColumnConfig(i - 1 || "rowId").header[0];
			cell.css = (cell.css || "") + " webix_ssheet_hide_column";
	
			view.$$("cells").hideColumn(i);
			i--;
		}
	
		if (!silent) view.callEvent("onColumnOperation", [action, start, end, null]);
	}
	
	function _hideRow(action, start, end, view, silent) {
		var i = end.row;
		if (end.row < 1 || start.row < 1) {
			delete view._hidden_rows_hash[start.row];
			return;
		}
	
		while (i >= start.row) {
			view._hidden_rows_hash[i] = i;
	
			if (i - 1 === 0) {
				var cell = view.$$("cells").getColumnConfig("rowId").header[0];
				cell.css = (cell.css || "") + " webix_ssheet_hide_row";
				view.$$("cells").refreshColumns();
			} else {
				view.$$("cells").addCellCss(i - 1, "rowId", "webix_ssheet_hide_row");
			}
			i--;
		}
		view.$$("cells").filter(function (obj) {
			return !view._hidden_rows_hash[obj.id];
		});
	
		if (!silent) view.callEvent("onRowOperation", [action, start, end, null]);
	}
	
	function _updatePosition(view, name, inc, data, start) {
	
		if (data.table && data.table.hidden && data.table.hidden[name] && data.table.hidden[name].length) {
			if (name === "column") reset(view, "cols");
			if (name === "row") reset(view, "rows");
	
			var s = start[name],
			    e = s + inc;
			if (s > e) {
				;
	
				var _ref = [e, s];
				s = _ref[0];
				e = _ref[1];
			}for (var i = s; i < e; i++) {
				for (var h = 0; h < data.table.hidden[name].length; h++) {
					if (data.table.hidden[name][h] >= i) data.table.hidden[name][h] = data.table.hidden[name][h] * 1 + (inc > 0 ? 1 : -1);
				}
			}
		}
	}
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(67)))

/***/ }),
/* 77 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports.init = init;
	
	var _styles = __webpack_require__(38);
	
	function init(view) {
	
		if (view.config.resizeCell) {
			var grid = view.$$("cells");
			grid.define("resizeRow", { headerOnly: true, size: 10 });
			grid.define("resizeColumn", { headerOnly: true, size: 10 });
			grid.define("fixedRowHeight", false);
	
			grid.attachEvent("onRowResize", function (id) {
				view.$$("cells").refreshSelectArea();
				view._save("sizes", { row: id, column: 0, size: view.getRow(id).$height });
			});
			grid.attachEvent("onColumnResize", function (id) {
				view.$$("cells").refreshSelectArea();
				view._save("sizes", { row: 0, column: id, size: view.getColumn(id).width });
			});
	
			// undo
			view.attachEvent("onUndo", function (type, row, column, value) {
				if (type == "c-resize" || type == "r-resize") _undoResize(view, row, column, value);
			});
		}
	
		view.attachEvent("onDataParse", function (data) {
			if (data.sizes) {
				for (var i = 0; i < data.sizes.length; i++) {
					var size = data.sizes[i];
					if (size[0] * 1 !== 0) {
						var row = view.getRow(size[0]);
						if (row) row.$height = size[2] * 1;
					} else {
						var col = view.getColumn(size[1]);
						if (col) col.width = size[2] * 1;
					}
				}
				if (data.sizes.length) view.refresh(true);
			}
		});
	
		view.attachEvent("onDataSerialize", function (data) {
			var sizes = [];
			var grid = view.$$("cells");
			var columns = grid.getState().order;
			var order = grid.data.order;
	
			var defWidth = grid.config.columnWidth;
			var defHeight = grid.config.rowHeight;
	
			for (var i = 1; i < columns.length; i++) {
				var width = grid.getColumnConfig(columns[i]).width;
				if (width && width != defWidth) sizes.push([0, i, width]);
			}
	
			for (var _i = 0; _i < order.length; _i++) {
				var height = grid.getItem(order[_i]).$height;
				if (height && height != defHeight) sizes.push([order[_i] * 1, 0, height]);
			}
	
			data.sizes = sizes;
		});
	
		view.attachEvent("onAction", function (action, p) {
			if (action == "before-grid-change") updatePosition(p.name, p.inc, p.data, p.start);
		});
	
		view.attachEvent("onBeforeStyleChange", function (row, column, n, o) {
			if (o && o.props.wrap === "wrap" && n.props.wrap != o.props.wrap) {
				delete view._table.getItem(row).$height;
			}
		});
		view.attachEvent("onStyleChange", _adjustRowHeight);
		view.attachEvent("onCellChange", _adjustRowHeight);
	}
	
	function _undoResize(view, row, column, value) {
		if (row) {
			view.$$("cells").getItem(row).$height = value;
			view._save("sizes", { row: row, column: 0, size: value });
		} else {
			view._table.setColumnWidth(column, value);
		}
		// update area selection
		view._table.refreshSelectArea();
	}
	
	function updatePosition(name, inc, data, start) {
		var i = void 0,
		    sizes = data.sizes,
		    nsizes = [];
	
		if (inc) {
			for (i = sizes.length - 1; i >= 0; i--) {
				var _sizes$i = sizes[i],
				    row = _sizes$i[0],
				    column = _sizes$i[1],
				    size = _sizes$i[2];
	
				if (row && name == "row" && row >= start.row || column && name == "column" && column >= start.column) {
					//moving row|column index
					row = name == "row" ? row * 1 + inc : row;
					column = name == "column" ? column * 1 + inc : column;
					if (name == "row" && row > 0 || name == "column" && column > 0) nsizes.push([row, column, size]);
				} else //row, column not affected by resize
					nsizes.push(sizes[i]);
			}
			data.sizes = nsizes;
		}
	}
	
	function _adjustRowHeight(row, column) {
		var view = this;
		var style = (0, _styles.getStyle)(view, { row: row, column: column });
	
		//changing styles or content of wrapped cell -> resize the related row
		if (style && style.props.wrap) {
			var item = view._table.getItem(row);
			var height = item.$height || view._table.config.rowHeight;
	
			view._table.eachColumn(function (col, config) {
				var style = (0, _styles.getStyle)(view, { row: row, column: col });
				//include only cells with active wrap into calculation 
				if (style && style.props && style.props.wrap === "wrap") {
					var text = this.getText(row, col);
					height = Math.max(height, (0, _styles.getTextSize)(view, text, style.id, config.width).height);
				}
			});
	
			item.$height = height;
		}
	}

/***/ }),
/* 78 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports.init = init;
	
	var _column_names = __webpack_require__(41);
	
	function init(view) {
		var table = view._table;
	
		var bs = 0;
		table.attachEvent("onAreaDrag", function () {
			return bs = new Date();
		});
		table.attachEvent("onBeforeAreaRemove", function () {
			//[FIXME] currently we don't have enough info to know
			//is user selects new area, and removing need to be blocked
			//or is it some API which attempts to remove the area
			if (view.$handleSelection && new Date() - bs < 500) return false;
		});
	
		table.attachEvent("onBeforeAreaAdd", function (area) {
			//[FIXME] 
			//block selection of row-id column
			//temporary fix, we need to fire onBeforeBlockSelect when selection moves
			//so the common onBeforeBlockSelect handler will block rowId selection
			if (area.start.column == "rowId") return false;
	
			//if we have an active editor, we need to close it on block selection
			//ignore, when selecting the editor's cell
			var editor = table.getEditor();
			if (editor && (editor.row != area.start.row || editor.column != area.start.column || editor.row != area.start.row || editor.column != area.start.column)) table.editCancel();
	
			if (!view.$handleSelection) return true;
	
			var blockStart = _column_names.encode[area.start.column] + area.start.row;
			var blockEnd = _column_names.encode[area.end.column] + area.end.row;
	
			var res = view.$handleSelection(area.start, area.end, blockStart, blockEnd);
	
			if (res !== false) {
				view.$handleSelection = null;
				table.removeSelectArea();
			}
	
			return res;
		});
	
		webix.event(table.$view, "mousedown", function (e) {
			if (view.$handleSelection) return webix.html.preventEvent(e);
		});
	}

/***/ }),
/* 79 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports.init = init;
	exports.sortRange = sortRange;
	
	var _expand_selection = __webpack_require__(80);
	
	var _updater = __webpack_require__(68);
	
	var _column_names = __webpack_require__(41);
	
	var _undo = __webpack_require__(39);
	
	function init(view) {
		view.attachEvent("onCommand", function (cm) {
			if (cm.id === "sort-asc" || cm.id === "sort-desc") sortRange(view, null, cm.id.replace("sort-", ""));
		});
	}
	
	var sorters = {
		"int": function int(dir, key) {
			return function (a, b) {
				var c = parseFloat(a[key]) || -Infinity;
				var d = parseFloat(b[key]) || -Infinity;
				return (c > d ? 1 : c == d ? 0 : -1) * dir;
			};
		},
		"str": function str(dir, key) {
			return function (a, b) {
				var c = (a[key] || "").toString().toLowerCase();
				var d = (b[key] || "").toString().toLowerCase();
				return (c > d ? 1 : c == d ? 0 : -1) * dir;
			};
		}
	};
	
	function sortRange(view, range, dir, type) {
		range = range || view._table.getSelectArea();
	
		if (range) {
			range = (0, _column_names.rangeObj)(range, view);
			var fullrange = range;
			if (range.start.row === range.end.row) fullrange = (0, _expand_selection.expandSelection)(range, view);
	
			var key = view.getCellValue(range.start.row, range.start.column);
			type = type || (isNaN(parseFloat(key)) ? "str" : "int");
			dir = !dir || dir === "asc" ? 1 : -1;
			_undo.group.set(function () {
				return sort(fullrange, range.start.column, type, dir, view);
			});
		}
	}
	
	function sort(range, by, type, dir, view) {
		for (var column = range.start.column; column <= range.end.column; column++) {
			//get all selected column values
			var values = [];
			for (var row = range.start.row; row <= range.end.row; row++) {
				var fullRow = view.getRow(row);
				var value = fullRow[column];
				var style = view.getStyle(row, column);
				var math = fullRow["$" + column];
	
				values.push({
					value: value,
					style: style,
					math: math,
					row: row
				});
			}
	
			//sort them
			values.sort(sorters[type](dir, "value"));
	
			//set new values to original column
			for (var _row = range.start.row; _row <= range.end.row; _row++) {
				var element = values.shift();
	
				view.setStyle(_row, column, element.style || null);
	
				if (!element.math) view.setCellValue(_row, column, element.value);else {
					var translate = {
						id: "move",
						row: _row - element.row,
						column: 0
					};
					var newMath = (0, _updater.updateMath)(element.math, translate);
	
					view.setCellValue(_row, column, newMath);
				}
			}
		}
	
		view.refresh();
	}

/***/ }),
/* 80 */
/***/ (function(module, exports) {

	"use strict";
	
	exports.__esModule = true;
	exports.expandSelection = expandSelection;
	function expandSelection(r, view) {
		var maxY = view.config.rowCount;
		var maxX = view.config.columnCount;
		var table = view._table;
	
		var nr1 = r.start.row,
		    nr2 = r.end.row,
		    nc1 = r.start.column,
		    nc2 = r.end.column;
	
		for (var i = nr1 - 1; i > 0; i--) {
			if (getValue(i, r.start.column, table)) nr1 = i;else break;
		}
	
		for (var _i = nr2 + 1; _i < maxY; _i++) {
			if (getValue(_i, r.end.column, table)) nr2 = _i;else break;
		}
	
		for (var _i2 = nc1 - 1; _i2 > 0; _i2--) {
			if (getValue(r.start.row, _i2, table)) nc1 = _i2;else break;
		}
	
		for (var _i3 = nc2 + 1; _i3 < maxX; _i3++) {
			if (getValue(r.end.row, _i3, table)) nc2 = _i3;else break;
		}
	
		var newrange = { start: { row: nr1, column: nc1 }, end: { row: nr2, column: nc2 } };
		if (nr1 != r.start.row || nr2 != r.end.row || nc1 != r.start.column || nc2 != r.end.column) {
			view._table.addSelectArea(newrange.start, newrange.end);
		}
	
		return newrange;
	}
	
	function getValue(r, c, table) {
		var value = table.getItem(r)[c];
		if (value === undefined || value === "") {
			return false;
		} else {
			return true;
		}
	}

/***/ }),
/* 81 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports.init = init;
	exports.addSpan = addSpan;
	exports.removeSpan = removeSpan;
	exports.getRange = getRange;
	
	var _lock_cell = __webpack_require__(40);
	
	function init(view) {
		view.attachEvent("onStyleChange", function (row, column, style) {
			var span = getSpan(view, row, column);
			if (span) span[3] = style ? style.id : "";
		});
	
		view.attachEvent("onDataParse", function (data) {
			if (data.spans) for (var i = 0; i < data.spans.length; i++) {
				var span = data.spans[i];
				addSpan(view, { row: span[0], column: span[1] }, span[2] * 1, span[3] * 1);
			}
		});
	
		view.attachEvent("onDataSerialize", function (data) {
			var spans = [];
			var pull = view.$$("cells").getSpan();
			if (pull) {
				for (var rid in pull) {
					var row = pull[rid];
					for (var cid in row) {
						var span = row[cid];
						spans.push([rid * 1, cid * 1, span[0], span[1]]);
					}
				}
	
				data.spans = spans;
			}
		});
	
		view.attachEvent("onUndo", function (type, row, column, value, isUndo) {
			if (type == "span" || type == "split") {
				if (type == "span") isUndo = !isUndo;
				undoSpan(view, row, column, value, isUndo);
			}
		});
	
		view.attachEvent("onAction", function (name, options) {
			if (name == "lock") setLockCss(view, options.row, options.column, options.newValue);else if (name == "before-grid-change") updatePosition(options.name, options.inc, options.data, options.start);else if (name == "check-borders") return checkSpanBorders(view, options.row, options.column, options.area, options.mode);
		});
	}
	
	function getSpan(view, row, column) {
		var item = view.$$("cells").getSpan()[row];
		if (item) return item[column];
	}
	
	function addSpan(view, cell, x, y) {
		if (x < 2 && y < 2) return;
	
		var row = view.getRow(cell.row);
		var css = row.$cellCss ? row.$cellCss[cell.column] || "" : "";
		setSpanValue(view, cell, x, y);
		view.$$("cells").addSpan(cell.row, cell.column, x, y, null, css);
		view._save("spans", {
			row: cell.row, column: cell.column,
			x: x, y: y
		});
	}
	
	function removeSpan(view, cell) {
		view.$$("cells").removeSpan(cell.row, cell.column);
		view._save("spans", {
			row: cell.row, column: cell.column,
			x: 0, y: 0
		});
	}
	
	function getRange(sel) {
		var lx, ly, rx, ry;
		rx = ry = 0;
		lx = ly = Infinity;
	
		for (var i = 0; i < sel.length; i++) {
			var cx = sel[i].column * 1;
			var cy = sel[i].row * 1;
	
			lx = Math.min(cx, lx);
			rx = Math.max(cx, rx);
			ly = Math.min(cy, ly);
			ry = Math.max(cy, ry);
		}
	
		return {
			cell: { row: ly, column: lx },
			x: rx - lx + 1,
			y: ry - ly + 1
		};
	}
	
	function undoSpan(view, row, column, value, isUndo) {
		if (isUndo) {
			removeSpan(view, { row: row, column: column });
		} else {
			addSpan(view, { row: row, column: column }, value[0], value[1]);
		}
	}
	
	function setLockCss(view, row, column, mode) {
		var span = getSpan(view, row, column);
		if (span) {
			if (mode) {
				if (span[3].indexOf(_lock_cell.lockCss) == -1) span[3] += " " + _lock_cell.lockCss;
			} else span[3] = span[3].replace(" " + _lock_cell.lockCss, "");
			view._table.refresh();
		}
	}
	
	function updatePosition(name, inc, data, start) {
		var i = void 0,
		    spans = data.spans;
	
		if (inc) {
			for (i = spans.length - 1; i >= 0; i--) {
				var _spans$i = spans[i],
				    row = _spans$i[0],
				    column = _spans$i[1];
	
				if (name == "row" && row >= start.row || name == "column" && column >= start.column) {
					spans[i][0] = name == "row" ? row * 1 + inc : row;
					spans[i][1] = name == "column" ? column * 1 + inc : column;
				}
			}
		}
	}
	
	var borderChecks = {
		"border-right": function borderRight(row, column, span, area) {
			return area.end.column == column + span[0] - 1;
		},
		"border-bottom": function borderBottom(row, column, span, area) {
			return area.end.row == row + span[1] - 1;
		}
	};
	
	function checkSpanBorders(view, row, column, area, mode) {
		var span = getSpan(view, row, column);
		return span && borderChecks[mode] ? borderChecks[mode](row, column, span, area) : false;
	}
	
	// clear merged cells except the first one
	function setSpanValue(view, cell, x, y) {
		var i,
		    j,
		    value,
		    row = cell.row * 1,
		    column = cell.column * 1,
		    isEmpty = !view.getCellValue(row, column);
		for (i = row; i < row + y; i++) {
			for (j = column; j < column + x; j++) {
				value = view.getCellValue(i, j);
				if (value !== "" && (j != column || i != row)) {
					if (isEmpty) {
						isEmpty = false;
						view.setCellValue(row, column, value);
					}
					view.setCellValue(i, j, "");
				}
			}
		}
	}

/***/ }),
/* 82 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports.init = init;
	
	var _hide_gridlines = __webpack_require__(83);
	
	var m1 = _interopRequireWildcard(_hide_gridlines);
	
	var _hide_headers = __webpack_require__(84);
	
	var m2 = _interopRequireWildcard(_hide_headers);
	
	var _lock_cell = __webpack_require__(40);
	
	var m3 = _interopRequireWildcard(_lock_cell);
	
	var _dropdown = __webpack_require__(75);
	
	var m4 = _interopRequireWildcard(_dropdown);
	
	var _filter = __webpack_require__(85);
	
	var m5 = _interopRequireWildcard(_filter);
	
	var _formats = __webpack_require__(34);
	
	var m6 = _interopRequireWildcard(_formats);
	
	var _hide_row_col = __webpack_require__(76);
	
	var m7 = _interopRequireWildcard(_hide_row_col);
	
	function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj["default"] = obj; return newObj; } }
	
	function init(view) {
		var modules = [m1, m2, m3, m4, m5, m6, m7];
		view.attachEvent("onDataSerialize", function (data) {
			for (var i = 0; i < modules.length; i++) {
				if (modules[i].serialize) modules[i].serialize(view, data);
			}
		});
		view.attachEvent("onDataParse", function (data) {
			for (var i = 0; i < modules.length; i++) {
				if (modules[i].load) modules[i].load(view, data);
			}
		});
	}

/***/ }),
/* 83 */
/***/ (function(module, exports) {

	"use strict";
	
	exports.__esModule = true;
	exports.hideGridlines = hideGridlines;
	exports.serialize = serialize;
	exports.load = load;
	function hideGridlines(view) {
		var state = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
	
		var grid = view.$$("cells");
		var borderless = grid.$view.className.indexOf("webix_borderless") != -1;
		if (state === "toggle") state = !borderless;
	
		if (!borderless && state) {
			webix.html.addCss(grid.$view, "webix_borderless", true);
		} else if (borderless && !state) {
			webix.html.removeCss(grid.$view, "webix_borderless");
		}
	}
	
	function serialize(view, data) {
		data.table.gridlines = view.$$("cells").$view.className.indexOf("webix_borderless") != -1 ? 0 : 1;
	}
	
	function load(view, data) {
		var mode = false;
		if (!webix.isUndefined(data.table) && !webix.isUndefined(data.table.gridlines)) mode = !data.table.gridlines;
		hideGridlines(view, mode);
	}

/***/ }),
/* 84 */
/***/ (function(module, exports) {

	"use strict";
	
	exports.__esModule = true;
	exports.hideHeaders = hideHeaders;
	exports.serialize = serialize;
	exports.load = load;
	function hideHeaders(view) {
		var state = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
	
		var grid = view.$$("cells");
		if (state === "toggle") state = grid.config.header;
	
		if (grid.config.header && state) {
			grid.config.header = false;
			if (grid.isColumnVisible("rowId")) grid.hideColumn("rowId", {}, true, true);
			grid.refreshColumns();
		} else if (!grid.config.header && !state) {
			grid.config.header = true;
			if (!grid.isColumnVisible("rowId")) grid.showColumn("rowId", {}, true);
			grid.refreshColumns();
		}
	}
	
	function serialize(view, data) {
		data.table.headers = view.$$("cells").config.header ? 1 : 0;
	}
	
	function load(view, data) {
		var mode = false;
		if (data.table && !webix.isUndefined(data.table.headers)) mode = !data.table.headers;
		hideHeaders(view, mode);
	}

/***/ }),
/* 85 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports.init = init;
	exports.getFilters = getFilters;
	exports.serialize = serialize;
	exports.load = load;
	
	var _undo = __webpack_require__(39);
	
	var _column_names = __webpack_require__(41);
	
	function init(view) {
		view.attachEvent("onCommand", function (cm) {
			if (cm.id === "create-filter") filter(view);
		});
		view.attachEvent("onUndo", function (type, row, column, value) {
			if (type === "filter") {
				if (!value) view.removeFilter();else filter(view, value);
			} else if (type == "dropdown") {
				view.setCellEditor(row, column, value);
			}
		});
	
		view.attachEvent("onAction", function (action, p) {
			if (action == "before-grid-change") updatePosition(p.name, p.inc, p.data, p.start);
		});
	}
	
	function getFilters(table) {
		var filters = table._ssFilters;
		var all = [];
		for (var row in filters) {
			for (var column in filters[row]) {
				all.push(filters[row][column]);
			}
		}return all;
	}
	
	function serialize(view, data) {
		var filters = getFilters(view._table);
		var editors = view._table._ssEditors;
	
		var result = data.filters = [];
		for (var i = 0; i < filters.length; i++) {
			var _filters$i = filters[i],
			    row = _filters$i[0],
			    column = _filters$i[1];
	
			var options = editors[row][column].options;
			if (options) result.push([row, column, options]);
		}
	}
	
	function load(view, data) {
		var i,
		    filters = data.filters;
	
		if (filters) for (i = 0; i < filters.length; i++) {
			view.setCellFilter.apply(view, filters[i]);
		}
	}
	
	function filter(view, range) {
		range = range || view._table.getSelectArea();
		view.removeFilter();
		if (!range) return;
	
		_undo.group.start();
		for (var i = range.start.column; i <= range.end.column; i++) {
			var row = range.start.row;
			var area = (0, _column_names.toRange)(row + 1, i, range.end.row, i);
			view.setCellFilter(row, i, area);
			view.callEvent("onAction", ["filter", { row: row, column: i, value: null, newValue: range }]);
		}
		_undo.group.end();
	}
	
	function updatePosition(name, inc, data, start) {
		var i = void 0,
		    filters = data.filters;
	
		if (inc) {
			for (i = filters.length - 1; i >= 0; i--) {
				var _filters$i2 = filters[i],
				    row = _filters$i2[0],
				    column = _filters$i2[1],
				    range = _filters$i2[2];
	
				if (name == "row" && row >= start.row || name == "column" && column >= start.column) {
					filters[i][0] = name == "row" ? row * 1 + inc : row;
					filters[i][1] = name == "column" ? column * 1 + inc : column;
					filters[i][2] = (0, _column_names.changeRange)(range, name, inc, start);
				}
			}
		}
	}

/***/ }),
/* 86 */
/***/ (function(module, exports) {

	"use strict";
	
	exports.__esModule = true;
	exports.init = init;
	function init(view) {
		var uploader;
		if (window.XMLHttpRequest && new XMLHttpRequest().upload) {
			uploader = webix.ui({ view: "uploader", apiOnly: true });
	
			uploader.attachEvent("onBeforeFileAdd", webix.bind(function (upload) {
				view.reset();
				view.parse(upload.file, "excel");
				return false;
			}, this));
		}
	
		view.attachEvent("onCommand", function (obj) {
			if (obj.id === "excel-import") startImport(view, uploader);
		});
	}
	
	function startImport(view, uploader) {
		if (!uploader) webix.alert(webix.i18n.spreadsheet.labels["import-not-support"]);else webix.delay(function () {
			return uploader.fileDialog();
		});
	}

/***/ }),
/* 87 */
/***/ (function(module, exports) {

	"use strict";
	
	exports.__esModule = true;
	exports.print = print;
	exports.init = init;
	function print(view, options) {
		var active = view._activeSheet;
	
		var html = _buildHtml(view, options, active);
		webix.html.insertBefore(html, options.docFooter, document.body);
		window.print();
		_cleanHtml(view, html);
	
		view.showSheet(active);
	}
	
	function init(view) {
		view.$customPrint = function (options) {
			print(view, options);
		};
	}
	
	function _cleanHtml(view, html) {
		webix.html.remove(html);
	
		view._sheets.forEach(function (sheet, i) {
			webix.html.removeStyle(".wss_" + view.$index + "_" + i);
		});
	}
	function _buildHtml(view, options, active) {
		var doc = webix.html.create("div", { "class": "webix_ssheet_print" });
	
		options.xCorrection = 1;
		options.header = options.header || false;
		options.trim = webix.isUndefined(options.trim) ? true : options.trim;
		options.sheetnames = webix.isUndefined(options.sheetnames) ? true : options.sheetnames;
		var baseIndex = view.$index;
	
		view._sheets.forEach(function (sheet, i) {
			view.$index = baseIndex + "_" + i;
			if (options.data === "all" || options.data !== "all" && sheet.name === active) {
				view.showSheet(sheet.name);
				var prefix = "wss_" + view.$index;
	
				if (options.sheetnames) {
					var sheetname = webix.html.create("div", { "class": "webix_view webix_ssheet_sheetname" }, sheet.name + ":");
					doc.appendChild(sheetname);
				}
	
				var table = view._table.$customPrint(options, true);
	
				//for empty sheet we don't have any HTML
				if (table.firstChild) table.firstChild.className += " " + prefix;
				doc.appendChild(table);
	
				if (options.data == "all" && i + 1 < view._sheets.length) {
					var br = webix.html.create("DIV", { "class": "webix_print_pagebreak" });
					doc.appendChild(br);
				}
			}
		});
		view.$index = baseIndex;
	
		return doc;
	}

/***/ }),
/* 88 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports.init = init;
	
	var _styles = __webpack_require__(38);
	
	var _helpers = __webpack_require__(32);
	
	function init(view) {
		view.$exportView = function (options) {
			webix.extend(options, { header: false, rawValues: true, spans: true, styles: true, math: true, xCorrection: 1, ignore: { rowId: true } });
			if (options.export_mode == "excel") return _exportExcel(view, options);else return view._table;
		};
	}
	
	function _exportExcel(view, options) {
		if (options.sheets === true) options.sheets = view._sheets.map(function (s) {
			return s.name;
		});else if (!options.sheets || !options.sheets.length) options.sheets = [view._activeSheet];else if (typeof options.sheets == "string") options.sheets = [options.sheets];
	
		options.dataOnly = true;
	
		var data = [];
		var active = view._activeSheet;
	
		for (var i = 0; i < options.sheets.length; i++) {
			view.showSheet(options.sheets[i]);
			data = data.concat(webix.toExcel(view._table, options));
			data[i].ranges = [];
	
			var ranges = view.ranges.getRanges();
			for (var r = 0; r < ranges.length; r++) {
				data[i].ranges.push({
					Sheet: i,
					Name: ranges[r].name,
					Ref: options.sheets[i] + "!" + ranges[r].range.replace(/(\w)/gi, function (match) {
						return "$" + match;
					})
				});
			}
			if (options.styles) data[i].styles = _getStyles(view, options);
		}
		view.showSheet(active);
	
		delete options.dataOnly;
		return data;
	}
	
	function _safeColor(str) {
		str = str.substring(1);
		if (str.length === 3) str = str + str;
		return str;
	}
	
	function _getDefaults() {
		var d = _helpers.defaultStyles;
		return {
			font: {
				sz: d["font-size"].replace("px", "") * 0.75,
				name: d["font-family"].replace(/,.*$/, "")
			},
			alignment: {
				horizontal: d["text-align"],
				vertical: d["vertical-align"] == "middle" ? "center" : d["vertical-align"],
				wrapText: d["white-space"] != "nowrap"
			}
		};
	}
	
	function _getStyles(view, options) {
	
		view.compactStyles();
	
		var all = view.serialize(),
		    styles = all.styles,
		    data = all.data,
		    result = [],
		    delta = 0;
	
		var defaults = _getDefaults();
	
		if (options.docHeader) result = result.concat([{ 0: _getDocStyle(options.docHeader.css, defaults) }, {}]);
		if (options.header) result.push({});
		delta = result.length;
	
		if (styles.length) {
			for (var i = 0; i < styles.length; i++) {
				styles[styles[i][0]] = _getCellStyle(styles[i][1]);
			}
			for (var _i = 0; _i < data.length; _i++) {
				var rind = data[_i][0] - 1 + delta,
				    cind = data[_i][1] - 1;
				result[rind] = result[rind] || {};
	
				if (data[_i][3] && styles[data[_i][3]]) result[rind][cind] = styles[data[_i][3]];else //default cell style
					result[rind][cind] = defaults;
			}
		}
		if (options.docFooter) result = result.concat([{}, { 0: _getDocStyle(options.docFooter.css, defaults) }]);
	
		return result;
	}
	
	function _getCellStyle(styles) {
		var str = styles.split(";"),
		    stl = { font: {}, alignment: {}, border: {} };
	
		for (var s = 0; s < str.length; s++) {
			if (str[s]) {
				switch (_styles.style_names[s]) {
					case "color":
						stl.font.color = { rgb: _safeColor(str[s]) };
						break;
					case "background":
						{
							var fill = _safeColor(str[s]);
							if (fill && fill.toLowerCase() !== "ffffff") stl.fill = { fgColor: { rgb: fill } };
							break;
						}
					case "text-align":
						stl.alignment.horizontal = str[s];
						break;
					case "font-family":
						stl.font.name = str[s].replace(/,.*$/, ""); // cut off fallback font
						break;
					case "font-size":
						stl.font.sz = str[s].replace("px", "") * 0.75; //px to pt conversion
						break;
					case "font-style":
						stl.font.italic = true;
						break;
					case "text-decoration":
						stl.font.underline = true;
						break;
					case "font-weight":
						stl.font.bold = true;
						break;
					case "vertical-align":
						stl.alignment.vertical = str[s] == "middle" ? "center" : str[s];
						break;
					case "wrap":
						stl.alignment.wrapText = str[s] == "wrap";
						break;
					case "borders":
						break;
					case "format":
						break;
					case "border-right":
						stl.border.right = { color: { rgb: _safeColor(str[s]) }, style: "thin" };
						break;
					case "border-bottom":
						stl.border.bottom = { color: { rgb: _safeColor(str[s]) }, style: "thin" };
						break;
					case "border-left":
						stl.border.left = { color: { rgb: _safeColor(str[s]) }, style: "thin" };
						break;
					case "border-top":
						stl.border.top = { color: { rgb: _safeColor(str[s]) }, style: "thin" };
						break;
				}
			}
		}
		return stl;
	}
	
	function _getDocStyle(css) {
		if (!css) return {};
		var str = [];
		for (var i = 0; i < _styles.style_names.length; i++) {
			str.push(css[_styles.style_names[i]] || "");
		}return _getCellStyle(str.join(";"));
	}

/***/ }),
/* 89 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports.init = init;
	
	var _styles = __webpack_require__(38);
	
	var _buttons = __webpack_require__(33);
	
	var _helpers = __webpack_require__(32);
	
	function init(view) {
		view._parseExcel = function (obj, driver) {
			var options = obj.options || {};
			options.math = view.config.math;
	
			var full = { sheets: [] };
			for (var i = 0; i < obj.names.length; i++) {
				options.name = obj.names[i];
				full.sheets.push({
					name: options.name,
					content: _excel_to_data(driver.getSheet(obj, options), obj.ranges, i)
				});
			}
			return full;
		};
		view._parseCsv = function (obj, driver) {
			obj = driver.getRecords(obj);
	
			var dataObj = webix.copy(obj);
			var data = [];
	
			for (var i = 0; i < dataObj.length; i++) {
				var item = dataObj[i].split(driver.cell);
				for (var c = 0; c < item.length; c++) {
					var row = i + 1,
					    column = c + 1;
					data.push([row, column, item[c]]);
				}
			}
			return { data: data };
		};
	}
	
	function _excel_to_data(obj, ranges, index) {
		var dataObj = webix.copy(obj.data);
		var data = [];
	
		for (var i = 0; i < dataObj.length; i++) {
			for (var c = 0; c < dataObj[0].length; c++) {
				var row = i + 1,
				    column = c + 1;
				data.push([row, column, dataObj[i][c]]);
			}
		}
		obj.data = data;
	
		//merged cells
		if (obj.spans) {
			for (var _i = 0; _i < obj.spans.length; _i++) {
				obj.spans[_i][0]++;
				obj.spans[_i][1]++;
			}
		}
	
		//named ranges
		if (ranges) {
			for (var r = 0; r < ranges.length; r++) {
				//we don't support global named ranges, so import only sheet-based :(
				if (ranges[r].Sheet === index) {
					obj.ranges = obj.ranges || [];
					obj.ranges.push([ranges[r].Name.toUpperCase(), ranges[r].Ref.substring(ranges[r].Ref.indexOf("!") + 1).replace(/\$/g, "")]);
				}
			}
		}
	
		//cell styles
		if (obj.styles) obj = _getStyles(obj);
	
		//column width and row height
		if (obj.sizes) {
			for (var _i2 = 0; _i2 < obj.sizes.length; _i2++) {
				if (obj.sizes[_i2][0] == "column") obj.sizes[_i2] = [0, obj.sizes[_i2][1] + 1, obj.sizes[_i2][2]];else obj.sizes[_i2] = [obj.sizes[_i2][1] + 1, 0, obj.sizes[_i2][2]];
			}
		}
	
		return obj;
	}
	
	//ARGB conversion
	function _safeColor(str) {
		str = str || "000000";
		if (str.length === 8) str = str.substring(2);
		return "#" + str;
	}
	
	//only fonts that we support can be imported
	function _safeFont(str) {
		var safe = "'PT Sans', Tahoma";
		for (var i = 0; i < _buttons.fontFamily.length; i++) {
			if (_buttons.fontFamily[i].value == str) {
				safe = _buttons.fontFamily[i].id;break;
			}
		}
		return safe;
	}
	
	function _getStyles(obj) {
		var styleshash = {},
		    styles = [],
		    stylescount = 1;
		for (var i = 0; i < obj.styles.length; i++) {
			var str = [],
			    stl = obj.styles[i][2],
			    css = void 0;
	
			for (var s = 0; s < _styles.style_names.length; s++) {
				switch (_styles.style_names[s]) {
					case "color":
						str[s] = stl.font && stl.font.color ? _safeColor(stl.font.color.rgb) || "" : "";
						break;
					case "background":
						str[s] = stl.fill && stl.fill.fgColor ? _safeColor(stl.fill.fgColor.rgb) || "" : "";
						break;
					case "text-align":
						str[s] = (stl.alignment ? stl.alignment.horizontal : "") || _helpers.defaultStyles["text-align"];
						break;
					case "font-family":
						str[s] = stl.font && stl.font.name ? _safeFont(stl.font.name) : "";
						break;
					case "font-size":
						str[s] = stl.font && stl.font.sz ? stl.font.sz / 0.75 + "px" || "" : ""; //pt to px conversion
						break;
					case "font-style":
						str[s] = stl.font && stl.font.italic ? "italic" : "";
						break;
					case "text-decoration":
						str[s] = stl.font && stl.font.underline ? "underline" : "";
						break;
					case "font-weight":
						str[s] = stl.font && stl.font.bold ? "bold" : "";
						break;
					case "vertical-align":
						var va = stl.alignment ? stl.alignment.vertical || "" : "";
						str[s] = (va == "center" ? "middle" : va) || _helpers.defaultStyles["vertical-align"];
						break;
					case "wrap":
						str[s] = stl.alignment && stl.alignment.wrapText ? "wrap" : "nowrap";
						break;
					case "borders":
						str[s] = "";
						break;
					case "format":
						str[s] = ""; //stl.numFmt
						break;
					case "border-right":
						str[s] = stl.border && stl.border.right ? _safeColor(stl.border.right.color.rgb) || "" : "";
						break;
					case "border-bottom":
						str[s] = stl.border && stl.border.bottom ? _safeColor(stl.border.bottom.color.rgb) || "" : "";
						break;
					case "border-left":
						str[s] = stl.border && stl.border.left ? _safeColor(stl.border.left.color.rgb) || "" : "";
						break;
					case "border-top":
						str[s] = stl.border && stl.border.top ? _safeColor(stl.border.top.color.rgb) || "" : "";
						break;
				}
			}
			str = str.join(";");
			css = styleshash[str] || "wss" + stylescount;
	
			for (var d = 0; d < obj.data.length; d++) {
				if (obj.data[d][0] === obj.styles[i][0] + 1 && obj.data[d][1] === obj.styles[i][1] + 1) {
					obj.data[d][3] = css;
					break;
				}
			}
			if (!styleshash[str]) {
				styles.push([css, str]);
				styleshash[str] = css;
				stylescount++;
			}
		}
		obj.styles = styles;
		return obj;
	}

/***/ }),
/* 90 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports.api = api;
	
	var _add_row_col = __webpack_require__(91);
	
	var add = _interopRequireWildcard(_add_row_col);
	
	var _lock_cell = __webpack_require__(92);
	
	var lcl = _interopRequireWildcard(_lock_cell);
	
	var _freeze = __webpack_require__(93);
	
	var fcl = _interopRequireWildcard(_freeze);
	
	var _hide_gridlines = __webpack_require__(94);
	
	var hbr = _interopRequireWildcard(_hide_gridlines);
	
	var _hide_headers = __webpack_require__(95);
	
	var hhd = _interopRequireWildcard(_hide_headers);
	
	var _hide_rows = __webpack_require__(96);
	
	var hrc = _interopRequireWildcard(_hide_rows);
	
	var _math = __webpack_require__(97);
	
	var mat = _interopRequireWildcard(_math);
	
	var _operations = __webpack_require__(98);
	
	var ops = _interopRequireWildcard(_operations);
	
	var _core = __webpack_require__(99);
	
	var cor = _interopRequireWildcard(_core);
	
	var _save = __webpack_require__(100);
	
	var sav = _interopRequireWildcard(_save);
	
	var _dropdown = __webpack_require__(101);
	
	var dpd = _interopRequireWildcard(_dropdown);
	
	var _confirm = __webpack_require__(102);
	
	var cfm = _interopRequireWildcard(_confirm);
	
	var _undo = __webpack_require__(103);
	
	var und = _interopRequireWildcard(_undo);
	
	function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj["default"] = obj; return newObj; } }
	
	function api(view) {
		var all = [add, lcl, fcl, hbr, hhd, hrc, mat, ops, cor, sav, dpd, cfm, und];
		for (var i = 0; i < all.length; i++) {
			webix.extend(view, all[i]);
		}
	}

/***/ }),
/* 91 */
/***/ (function(module, exports) {

	"use strict";
	
	exports.__esModule = true;
	exports.insertRow = insertRow;
	exports.deleteRow = deleteRow;
	exports.insertColumn = insertColumn;
	exports.deleteColumn = deleteColumn;
	function insertRow(row) {
		callCommand(this, "add", "row", row);
	}
	
	function deleteRow(row) {
		callCommand(this, "del", "row", row);
	}
	
	function insertColumn(column) {
		callCommand(this, "add", "column", column);
	}
	
	function deleteColumn(column) {
		callCommand(this, "del", "column", column);
	}
	
	function getCells(view, id, group) {
		if (!id) {
			var area = view.$$("cells").getSelectArea();
			if (area) id = [area.start[group], area.end[group]];
		} else if (!webix.isArray(id)) {
			id = [id, id];
		}
		return id;
	}
	
	function callCommand(view, name, group, id) {
		var start = {},
		    end = {};
		id = getCells(view, id, group);
		start[group] = id[0];
		end[group] = id[1];
		view.callEvent("onCommand", [{ id: name, group: group }, start, end]);
	}

/***/ }),
/* 92 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports.lockCell = lockCell;
	exports.isCellLocked = isCellLocked;
	
	var _lock_cell = __webpack_require__(40);
	
	var lock = _interopRequireWildcard(_lock_cell);
	
	function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj["default"] = obj; return newObj; } }
	
	function lockCell(row, column, state) {
		lock.lockCell(this, row, column, state);
	}
	
	function isCellLocked(row, column) {
		return lock.isCellLocked(this, row, column);
	}

/***/ }),
/* 93 */
/***/ (function(module, exports) {

	"use strict";
	
	exports.__esModule = true;
	exports.freezeColumns = freezeColumns;
	exports.freezeRows = freezeRows;
	function freezeColumns(column) {
		var grid = this.$$("cells");
		var oldColumn = grid.config.leftSplit - 1;
	
		if (column === false) column = 0;
	
		if (webix.isUndefined(column)) {
			var sel = this.getSelectedId(true);
			column = sel.length ? sel[0].column : 0;
		}
	
		if (grid.config.leftSplit > 1) {
			var diff = column - oldColumn;
			if (!diff) column = 0;
		}
	
		grid.unselect();
		grid.define("leftSplit", column ? column + 1 : 1);
		grid.refreshColumns();
	
		this.callEvent("onAction", ["freeze-column", { value: oldColumn, newValue: column }]);
	}
	
	function freezeRows(row) {
		var grid = this.$$("cells");
		var oldRow = grid.config.topSplit;
	
		//if no row selected or at split row
		if (oldRow == row) row = 0;
	
		grid.unselect();
		grid.define("topSplit", row || 0);
		grid.refreshColumns();
	
		this.callEvent("onAction", ["freeze-row", { value: oldRow, newValue: row }]);
	}

/***/ }),
/* 94 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports.hideGridlines = hideGridlines;
	
	var _hide_gridlines = __webpack_require__(83);
	
	var gridlines = _interopRequireWildcard(_hide_gridlines);
	
	function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj["default"] = obj; return newObj; } }
	
	function hideGridlines(state) {
		gridlines.hideGridlines(this, state);
	}

/***/ }),
/* 95 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports.hideHeaders = hideHeaders;
	
	var _hide_headers = __webpack_require__(84);
	
	var headers = _interopRequireWildcard(_hide_headers);
	
	function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj["default"] = obj; return newObj; } }
	
	function hideHeaders(state) {
		headers.hideHeaders(this, state);
	}

/***/ }),
/* 96 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports.hideColumn = hideColumn;
	exports.isColumnVisible = isColumnVisible;
	exports.hideRow = hideRow;
	exports.isRowVisible = isRowVisible;
	
	var _hide_row_col = __webpack_require__(76);
	
	var hrows = _interopRequireWildcard(_hide_row_col);
	
	function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj["default"] = obj; return newObj; } }
	
	function hideColumn(column, state) {
		hrows.setColumnState(this, column, state);
	}
	function isColumnVisible(column) {
		return hrows.isColumnVisible(this, column);
	}
	function hideRow(row, state) {
		hrows.setRowState(this, row, state);
	}
	function isRowVisible(row) {
		return hrows.isRowVisible(this, row);
	}

/***/ }),
/* 97 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports.registerMathMethod = undefined;
	exports.addImage = addImage;
	exports.addSparkline = addSparkline;
	exports.getSheetData = getSheetData;
	exports.recalculate = recalculate;
	
	var _methods = __webpack_require__(44);
	
	var _writer = __webpack_require__(53);
	
	var _sheet = __webpack_require__(56);
	
	function addImage(row, column, url) {
		this.setCellValue(row, column, (0, _writer.image)(url));
		this.callEvent("onStyleSet", ["format", "image"]);
	}
	
	function addSparkline(row, column, config) {
		var value = (0, _writer.chart)(config);
		this.setCellValue(row, column, value);
		this.refresh();
	}
	
	function getSheetData(name) {
		return (0, _sheet.getAccessor)(this, name);
	}
	
	function recalculate() {
		this.callEvent("onMathRefresh", []);
	}
	
	var registerMathMethod = exports.registerMathMethod = _methods.addMethod;

/***/ }),
/* 98 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports.combineCells = combineCells;
	exports.splitCell = splitCell;
	exports.addStyle = addStyle;
	exports.getStyle = getStyle;
	exports.setStyle = setStyle;
	exports.setRangeStyle = setRangeStyle;
	exports.clearRange = clearRange;
	exports.setFormat = setFormat;
	exports.compactStyles = compactStyles;
	exports.serialize = serialize;
	exports.showSheet = showSheet;
	exports.getActiveSheet = getActiveSheet;
	exports.addSheet = addSheet;
	exports.clearSheet = clearSheet;
	exports.renameSheet = renameSheet;
	exports.editSheet = editSheet;
	exports.removeSheet = removeSheet;
	exports.undo = undo;
	exports.redo = redo;
	exports.sortRange = sortRange;
	
	var _sheets = __webpack_require__(48);
	
	var sheets = _interopRequireWildcard(_sheets);
	
	var _spans = __webpack_require__(81);
	
	var spn = _interopRequireWildcard(_spans);
	
	var _undo = __webpack_require__(39);
	
	var und = _interopRequireWildcard(_undo);
	
	var _styles = __webpack_require__(38);
	
	var stl = _interopRequireWildcard(_styles);
	
	var _formats = __webpack_require__(34);
	
	var fmt = _interopRequireWildcard(_formats);
	
	var _sorting = __webpack_require__(79);
	
	var srt = _interopRequireWildcard(_sorting);
	
	var _dropdown = __webpack_require__(75);
	
	function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj["default"] = obj; return newObj; } }
	
	//spans
	function combineCells(range) {
		var _this = this;
	
		if (!range) {
			var sel = this.getSelectedId(true);
			if (sel.length > 1) {
				range = spn.getRange(sel);
			}
		}
		und.group.set(function () {
			if (range && _this.callEvent("onBeforeSpan", [range.cell.row, range.cell.column, [range.x, range.y]])) spn.addSpan(_this, range.cell, range.x, range.y);
		});
		this.refresh();
	}
	
	function splitCell(row, column) {
		if (row && column) {
			spn.removeSpan(this, { row: row, column: column });
		} else {
			und.group.set(function () {
				this.eachSelectedCell(function (cell) {
					var span = this._table.getSpan(cell.row, cell.column);
					if (span && this.callEvent("onBeforeSplit", [cell.row, cell.column, [span[2], span[3]], und.group.value])) {
						spn.removeSpan(this, cell);
					}
				});
			}, this);
		}
	
		this.refresh();
	}
	
	//styles
	function addStyle(prop, origin) {
		return stl.addStyle(this, prop, origin);
	}
	
	function getStyle(row, column) {
		return stl.getStyle(this, { row: row, column: column });
	}
	
	function setStyle(row, column, style) {
		return stl.setStyle(this, { row: row, column: column }, style);
	}
	
	function setRangeStyle(rangeStr, style) {
		und.group.set(function () {
			stl.setRangeStyle(this, rangeStr, style);
		}, this);
	}
	
	function clearRange(rangeStr, type) {
		if (!rangeStr) {
			rangeStr = this._table.getSelectArea();
			if (!rangeStr) return;
		}
	
		if (!type) type = { styles: true, values: true, editors: true };
	
		if (type.styles) stl.clearRangeStyle(this, rangeStr);
	
		if (type.values) this.setRangeValue(rangeStr, null);
	
		if (type.editors) (0, _dropdown.clearEditors)(this, rangeStr);
	
		this.refresh();
	}
	
	function setFormat(row, column, format) {
		var old = this.getStyle(row, column);
		var fmtstring = fmt.addFormat(format);
		var nev = stl.addStyle(this, { format: fmtstring }, old);
		this.setStyle(row, column, nev);
	}
	
	function compactStyles() {
		return stl.compactStyles(this);
	}
	
	// serialize
	function serialize(config) {
		var obj = {};
		this.callEvent("onDataSerialize", [obj, config]);
	
		if (config && config.sheets) return sheets.serialize(this, obj);
		return obj;
	}
	
	//sheets
	function showSheet(name) {
		sheets.show(this, name);
	}
	function getActiveSheet() {
		return this._activeSheet;
	}
	function addSheet(content) {
		sheets.add(this, content);
	}
	function clearSheet() {
		this.reset();
	}
	function renameSheet(name, newName) {
		sheets.rename(this, name, newName);
	}
	function editSheet(name) {
		sheets.edit(this, name);
	}
	function removeSheet(name) {
		sheets.remove(this, name);
	}
	
	//undo
	function undo() {
		und.undo(this);
	}
	function redo() {
		und.redo(this);
	}
	
	// sort
	function sortRange(range, dir) {
		srt.sortRange(this, range, dir);
	}

/***/ }),
/* 99 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports.refresh = refresh;
	exports.eachSelectedCell = eachSelectedCell;
	exports.getSelectedRange = getSelectedRange;
	exports.getSelectedId = getSelectedId;
	exports.getCellValue = getCellValue;
	exports.setCellValue = setCellValue;
	exports.setRangeValue = setRangeValue;
	exports.getRow = getRow;
	exports.getColumn = getColumn;
	exports.reset = reset;
	exports._resetTable = _resetTable;
	
	var _column_names = __webpack_require__(41);
	
	var _table = __webpack_require__(36);
	
	var tbl = _interopRequireWildcard(_table);
	
	var _undo = __webpack_require__(39);
	
	var _sheets = __webpack_require__(48);
	
	function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj["default"] = obj; return newObj; } }
	
	function refresh(all) {
		if (all) this._table.refreshColumns();else this._table.refresh();
	}
	function eachSelectedCell(cb) {
		var cells = this.getSelectedId(true);
		for (var i = 0; i < cells.length; i++) {
			cb.call(this, cells[i]);
		}
	}
	function getSelectedRange(sheet) {
		var area = this._table.getSelectArea();
		if (area) {
			if (sheet) sheet = this.getActiveSheet();
			return (0, _column_names.toRange)(area.start.row, area.start.column, area.end.row, area.end.column, sheet);
		}
		return "";
	}
	function getSelectedId(all) {
		var area = this._table.getSelectArea();
		if (!all) return area && area.start.row ? area.start : null;
	
		var selection = [];
		if (area) {
			var c0 = area.start;
			var c1 = area.end;
	
			for (var i = c0.row; i <= c1.row; i++) {
				for (var j = c0.column; j <= c1.column; j++) {
					selection.push({ row: i, column: j });
				}
			}
		}
	
		return selection;
		//return this._table.getSelectedId(all)
	}
	function getCellValue(row, column) {
		var item = this.getRow(row);
		var value = item["$" + column] || item[column];
	
		if (typeof value === "undefined") return "";
		return value;
	}
	function setCellValue(row, column, value) {
		var item = this.getRow(row);
		var old = item["$" + column] || item[column];
	
		if (this.callEvent("onBeforeValueChange", [row, column, value, old])) {
			item[column] = value;
			delete item["$" + column];
	
			this.callEvent("onCellChange", [row, column, value, true]);
			this.saveCell(row, column);
		}
	}
	
	function setRangeValue(range, value) {
		_undo.group.set(function () {
			var pos = (0, _column_names.rangeObj)(range, this);
			for (var row = pos.start.row; row <= pos.end.row; row++) {
				for (var column = pos.start.column; column <= pos.end.column; column++) {
					this.setCellValue(row, column, value);
				}
			}
		}, this);
	}
	
	function getRow(id) {
		return this._table.getItem(id);
	}
	function getColumn(id) {
		return this._table.getColumnConfig(id);
	}
	function reset(mode) {
		var obj = { data: [] };
		if (mode && mode.sheets) {
			obj = (0, _sheets.newSheet)(obj);
		}
	
		this.parse(obj);
	}
	
	function _resetTable() {
		tbl.reset(this);
	}

/***/ }),
/* 100 */
/***/ (function(module, exports) {

	"use strict";
	
	exports.__esModule = true;
	exports._save = _save;
	exports.saveCell = saveCell;
	function _save(name, data) {
		var save = this.config.save;
		var url = "";
		if (typeof save === "string") url = save + "/" + name;else if (save && save[name]) url = save[name];
	
		if (url) {
			if (url.$proxy && url.save) url.save(this, data, null, null);else webix.ajax().post(url, data);
		}
	}
	
	function saveCell(row, column) {
		var style = this.getStyle(row, column);
	
		this._save("data", {
			row: row,
			column: column,
			value: this.getCellValue(row, column),
			style: style ? style.id : ""
		});
	}

/***/ }),
/* 101 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports.getRow = getRow;
	exports.setCellFilter = setCellFilter;
	exports.setCellEditor = setCellEditor;
	exports.getCellEditor = getCellEditor;
	exports.addFilter = addFilter;
	exports.removeFilter = removeFilter;
	exports.filterSpreadSheet = filterSpreadSheet;
	
	var _dropdown = __webpack_require__(75);
	
	var drd = _interopRequireWildcard(_dropdown);
	
	var _filter = __webpack_require__(85);
	
	function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj["default"] = obj; return newObj; } }
	
	function getRow(id) {
		return this._table.getItem(id);
	}
	
	function setCellFilter(row, column, range) {
		this.setCellEditor(row, column, { editor: "richselect", options: range });
		this.addFilter(row, column);
	}
	
	function setCellEditor(row, column, editor) {
		var line = this._table._ssEditors[row] = this._table._ssEditors[row] || {};
		var value = line[column] || this._table.getItem(row)[column];
		line[column] = editor;
	
		if (editor && editor.editor) this._table.addCellCss(row, column, "ss_filter");else {
			delete this._table._ssEditors[row][column];
			this._table.removeCellCss(row, column, "ss_filter");
		}
		this.callEvent("onAction", ["dropdown", { row: row, column: column, value: value, newValue: editor }]);
	}
	
	function getCellEditor(row, column) {
		var line = this._table._ssEditors[row] || {};
		return line[column] || null;
	}
	
	function addFilter(row, column) {
		return drd.addCellFilter(this, row, column);
	}
	
	function removeFilter() {
		var fs = (0, _filter.getFilters)(this._table);
		for (var i = 0; i < fs.length; i++) {
			var _fs$i = fs[i],
			    row = _fs$i[0],
			    column = _fs$i[1];
	
			var value = this.getCellEditor(row, column);
			this.setCellEditor(row, column, null);
			this._table.removeCellCss(row, column, "ss_filter");
			this.callEvent("onAction", ["filter", { row: row, column: column, value: value, newValue: null }]);
		}
	
		this._table._ssFilters = [];
		this._table.data.filter(function () {
			return true;
		});
	}
	
	function filterSpreadSheet() {
		this._table.data.silent(function () {
			var _this = this;
	
			var filters = (0, _filter.getFilters)(this._table);
	
			var _loop = function _loop() {
				var _filters$i = filters[i],
				    row = _filters$i[0],
				    column = _filters$i[1];
				value = _this._table.getItem(row)[column];
				all = false;
	
				_this._table.data.filter(function (obj) {
					if (!value) return true;
					if (obj.id * 1 <= row * 1) return true;
					if (!obj[column] && obj[column] !== "0") all = true;
					return all || obj[column] == value;
				}, 1, i);
			};
	
			for (var i = 0; i < filters.length; i++) {
				var value;
				var all;
	
				_loop();
			}
		}, this);
	
		this._table.refresh();
	}

/***/ }),
/* 102 */
/***/ (function(module, exports) {

	"use strict";
	
	exports.__esModule = true;
	exports.confirm = confirm;
	exports.alert = alert;
	function confirm(config) {
		var _this = this;
	
		webix.modalbox({
			type: "alert webix_ssheet_confirm",
			text: config.text,
			buttons: [webix.i18n.spreadsheet.labels.cancel, webix.i18n.spreadsheet.labels.ok],
			callback: function callback(result) {
				config.callback.call(_this, result != "0");
			}
		});
	}
	
	function alert(config) {
		webix.modalbox({
			type: "alert webix_ssheet_alert",
			text: config.text,
			buttons: [webix.i18n.spreadsheet.labels.ok]
		});
	}

/***/ }),
/* 103 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports.resetUndo = resetUndo;
	
	var _undo = __webpack_require__(39);
	
	function resetUndo() {
		(0, _undo.reset)(this);
	}

/***/ }),
/* 104 */
/***/ (function(module, exports) {

	"use strict";
	
	exports.__esModule = true;
	exports.init = init;
	var defaultMenu = [{ id: "file", submenu: [{ id: "sheet", submenu: [{ id: "new-sheet" }, { id: "copy-sheet" }, { id: "remove-sheet" }] }, { id: "excel-import" }, { id: "excel-export" }] }, { id: "edit", submenu: [{ id: "add-range" }, { id: "add-dropdown" }, { id: "add-link" }, { id: "lock-cell" }, { id: "conditional-format" }, { id: "clear-styles" }] }, { id: "insert", submenu: [{ id: "add-image" }, { id: "add-sparkline" }] }, { id: "data", submenu: [{ id: "sort", submenu: [{ id: "sort-asc" }, { id: "sort-desc" }] }, { id: "create-filter" }] }, {
		id: "view", submenu: [{ id: "columns", submenu: [{ id: "insert-column" }, { id: "delete-column" }, { id: "show-column" }, { id: "hide-column" }] }, { id: "rows", submenu: [{ id: "insert-row" }, { id: "delete-row" }, { id: "show-row" }, { id: "hide-row" }] }, { $template: "Separator" }, { id: "freeze-columns" }, { id: "freeze-rows" }, { id: "hide-gridlines" }, { id: "hide-headers" }]
	}];
	
	var ui = {
		getMenuData: function getMenuData(menu) {
			var i;
			for (i = 0; i < menu.length; i++) {
				if (typeof menu[i] == "string") menu[i] = { id: menu[i] };
				if (!menu[i].value) menu[i].value = webix.i18n.spreadsheet.menus[menu[i].id];
				if (menu[i].submenu) menu[i].submenu = this.getMenuData(menu[i].submenu);
			}
	
			return menu;
		}
	};
	
	function init(view) {
		view.attachEvent("onComponentInit", function () {
			return ready(view);
		});
	
		var config = view.config,
		    menu = {
			view: "menu",
			borderless: false,
			css: "webix_ssheet_menu",
			id: "menu",
			autowidth: true,
			type: {
				height: 40
			},
			data: ui.getMenuData(webix.isArray(config.menu) ? config.menu : defaultMenu)
		};
	
		view.callEvent("onViewInit", ["menu", menu]);
		return menu;
	}
	
	function ready(view) {
		if (view.$$("menu")) view.$$("menu").attachEvent("onMenuItemClick", function (id) {
			return callAction(view, id);
		});
	}
	
	var actionMap = {
		"undo": function undo(view) {
			view.undo();
		},
		"redo": function redo(view) {
			view.redo();
		},
		"insert-column": function insertColumn(view) {
			view.insertColumn();
		},
		"delete-column": function deleteColumn(view) {
			view.deleteColumn();
		},
		"show-column": function showColumn(view) {
			view.hideColumn(null, false);
		},
		"hide-column": function hideColumn(view) {
			view.hideColumn(null, true);
		},
		"insert-row": function insertRow(view) {
			view.insertRow();
		},
		"delete-row": function deleteRow(view) {
			view.deleteRow();
		},
		"show-row": function showRow(view) {
			view.hideRow(null, false);
		},
		"hide-row": function hideRow(view) {
			view.hideRow(null, true);
		},
		"freeze-columns": function freezeColumns(view) {
			view.freezeColumns();
		},
		"hide-gridlines": function hideGridlines(view) {
			view.hideGridlines("toggle");
		},
		"hide-headers": function hideHeaders(view) {
			view.hideHeaders("toggle");
		}
	};
	
	function callAction(view, id) {
		if (view.callEvent("onMenuItemClick", [id])) {
			if (actionMap[id]) actionMap[id](view);else view.callEvent("onCommand", [{ id: id }]);
		}
	}

/***/ }),
/* 105 */
/***/ (function(module, exports) {

	"use strict";
	
	exports.__esModule = true;
	exports.getDefaultStyles = getDefaultStyles;
	function getText(text) {
		var locale = webix.i18n.spreadsheet.labels;
		return locale[text] || text;
	}
	function getDefaultStyles() {
		return [{ name: getText("normal"), css: "webix_ssheet_condition_regular" }, { name: getText("neutral"), css: "webix_ssheet_condition_neutral" }, { name: getText("bad"), css: "webix_ssheet_condition_bad" }, { name: getText("good"), css: "webix_ssheet_condition_good" }];
	}

/***/ }),
/* 106 */
/***/ (function(module, exports) {

	// removed by extract-text-webpack-plugin
	"use strict";

/***/ })
/******/ ]);